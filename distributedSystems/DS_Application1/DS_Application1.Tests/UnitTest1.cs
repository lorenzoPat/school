﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Data.Entity;
using DS_Application1.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DS_Application1.Tests
{
  [TestClass]
  public class UnitTest1
  {
    distributedSystemsEntities db = new distributedSystemsEntities();

    [TestInitialize]
    public void Init()
    {
      cleanDB();
      populateDB();
    }

    
    [TestMethod]
    public void TestAddUser()
    {
      var before = db.users_asp.ToList();
      users_asp user = GetTestUser();
      db.users_asp.Add(user);
      db.SaveChanges();
      var after = db.users_asp.ToList();
      Assert.AreEqual(after.Count, before.Count + 1);
    }

    [TestMethod]
    public void TestRemoveUser()
    {
      users_asp user = GetTestUser();
      db.users_asp.Add(user);
      db.SaveChanges();

      var before = db.users_asp.ToList();
      db.users_asp.Remove(user);
      db.SaveChanges();
      var after = db.users_asp.ToList();
      Assert.AreEqual(after.Count, before.Count - 1);
    }

    [TestMethod]
    public void TestUpdateUser()
    {
      users_asp user = GetTestUser();
      db.users_asp.Add(user);
      db.SaveChanges();

      user.username = "blabla_test";
      db.SaveChanges();

      var list = db.users_asp.ToList();
      var u = list.Last();
      Assert.AreEqual(u.username, user.username);
    }

    [TestMethod]
    public void TestReadUser()
    {
      users_asp user = GetTestUser();
      db.users_asp.Add(user);
      db.SaveChanges();

      var list = db.users_asp.ToList();
      var u = list.Last();
      Assert.AreEqual(u.username, user.username);
    }

    private void populateDB()
    {
      users_asp user0 = new users_asp();
      user0.username = "user0";
      user0.password = "user0";
      users_asp user1 = new users_asp();
      user1.username = "user1";
      user1.password = "user1";
      users_asp user2 = new users_asp();
      user2.username = "user2";
      user2.password = "user2";
      users_asp user3 = new users_asp();
      user3.username = "user3";
      user3.password = "user3";

      db.users_asp.Add(user0);
      db.users_asp.Add(user1);
      db.users_asp.Add(user2);
      db.users_asp.Add(user3);
      db.SaveChanges();
    }

    private void cleanDB()
    {
      var x = db.users_asp.ToList();

      foreach (var c in x)
      {
        db.users_asp.Remove(c);
      }
      db.SaveChanges();
    }

    private users_asp GetTestUser()
    {
      users_asp user = new users_asp();
      user.username = "test";
      user.password = "test";
      user.type = 1;
      return user;
    }
  }
}
