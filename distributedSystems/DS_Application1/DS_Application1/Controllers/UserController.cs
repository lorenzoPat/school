﻿using DS_Application1.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DS_Application1.Controllers
{
  public class UserController : Controller
  {
    private distributedSystemsEntities db = new distributedSystemsEntities();

    //
    // GET: /User/

    public ActionResult Index()
    {
      return View();
    }

    [HttpPost]
    public ActionResult Index(string username, string password)
    {
      users_asp loggedUser = db.users_asp.Where(u => u.username == username).FirstOrDefault();
      if (loggedUser == null)
        return RedirectToAction("Error");

      Session["loggedUser"] = loggedUser;

      if (loggedUser.type == 0)
        return RedirectToAction("Admin");

      return RedirectToAction("User");

    }

    public ActionResult Error()
    {
      return View();
    }

    public ActionResult Admin()
    {
      users_asp loggedUser = (users_asp)Session["loggedUser"];
      if (loggedUser.type != 0)
        return RedirectToAction("Error");

      return View(db.users_asp.ToList());
    }

    public ActionResult User()
    {
      users_asp loggedUser = (users_asp)Session["loggedUser"];
      Timezone timezone = new Timezone((double)loggedUser.latitude, (double)loggedUser.longitude);
      string result = timezone.GetTimezone();
      ViewBag.result = result;
      ViewBag.name = loggedUser.name;
      ViewBag.surname = loggedUser.surname;
      return View();
    }

    //
    // GET: /User/Details/5

    public ActionResult Details(int id = 0)
    {
      users_asp users_asp = db.users_asp.Find(id);
      if (users_asp == null)
      {
        return HttpNotFound();
      }
      return View(users_asp);
    }

    //
    // GET: /User/Create

    public ActionResult Create()
    {
      return View();
    }

    //
    // POST: /User/Create

    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Create(users_asp users_asp)
    {
      if (ModelState.IsValid)
      {
        db.users_asp.Add(users_asp);
        db.SaveChanges();
        return RedirectToAction("Admin");
      }

      return View(users_asp);
    }

    //
    // GET: /User/Edit/5

    public ActionResult Edit(int id = 0)
    {
      users_asp users_asp = db.users_asp.Find(id);
      if (users_asp == null)
      {
        return HttpNotFound();
      }
      return View(users_asp);
    }

    //
    // POST: /User/Edit/5

    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Edit(users_asp users_asp)
    {
      if (ModelState.IsValid)
      {
        db.Entry(users_asp).State = EntityState.Modified;
        db.SaveChanges();
        return RedirectToAction("Admin");
      }
      return View(users_asp);
    }

    //
    // GET: /User/Delete/5

    public ActionResult Delete(int id = 0)
    {
      users_asp users_asp = db.users_asp.Find(id);
      if (users_asp == null)
      {
        return HttpNotFound();
      }
      return View(users_asp);
    }

    //
    // POST: /User/Delete/5

    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public ActionResult DeleteConfirmed(int id)
    {
      users_asp users_asp = db.users_asp.Find(id);
      db.users_asp.Remove(users_asp);
      db.SaveChanges();
      return RedirectToAction("Admin");
    }

    protected override void Dispose(bool disposing)
    {
      db.Dispose();
      base.Dispose(disposing);
    }
  }
}