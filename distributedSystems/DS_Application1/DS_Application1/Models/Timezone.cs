﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Xml;

namespace DS_Application1.Models
{
  public class Timezone
  {
    private double latitude;
    private double longitude;

    public Timezone(double latitude, double longitude)
    {
      this.latitude = latitude;
      this.longitude = longitude;
    }

    public string GetTimezone()
    {
      string url = String.Format("http://www.earthtools.org/timezone/{0}/{1}", latitude, longitude);
      string webResult = GetWebRequestResult(url);
      return GetTimezoneFromXml(webResult);
    }

    private string GetWebRequestResult(string url)
    {
      var request = (HttpWebRequest)WebRequest.Create(new Uri(url));
      var response = (HttpWebResponse)request.GetResponse();
      Stream dataStream = response.GetResponseStream();
      StreamReader dataread = new StreamReader(dataStream);
      return dataread.ReadToEnd();
    }

    private string GetTimezoneFromXml(string xml)
    {
      XmlDocument doc = new XmlDocument();
      doc.LoadXml(xml);
      XmlNodeList tags = doc.GetElementsByTagName("localtime");
      return tags[0].InnerText;
    }
  }
}