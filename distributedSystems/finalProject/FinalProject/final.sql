-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema final
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `final` ;

-- -----------------------------------------------------
-- Schema final
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `final` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `final` ;

-- -----------------------------------------------------
-- Table `final`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `final`.`user` ;

CREATE TABLE IF NOT EXISTS `final`.`user` (
  `user_id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `type` INT NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `final`.`user_details`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `final`.`user_details` ;

CREATE TABLE IF NOT EXISTS `final`.`user_details` (
  `user_id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) NULL,
  `last_name` VARCHAR(45) NULL,
  `address` VARCHAR(45) NULL,
  `birthday` DATE NULL,
  `info` VARCHAR(250) NULL,
  PRIMARY KEY (`user_id`),
  INDEX `fk_user_details_user_idx` (`user_id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `final`.`groups`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `final`.`groups` ;

CREATE TABLE IF NOT EXISTS `final`.`groups` (
  `group_id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(10) NULL,
  PRIMARY KEY (`group_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `final`.`user_group`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `final`.`user_group` ;

CREATE TABLE IF NOT EXISTS `final`.`user_group` (
  `user_id` INT NOT NULL,
  `group_id` INT NOT NULL,
  INDEX `fk_user_group_user1_idx` (`user_id` ASC),
  INDEX `fk_user_group_group1_idx` (`group_id` ASC),
  PRIMARY KEY (`user_id`, `group_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `final`.`review`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `final`.`review` ;

CREATE TABLE IF NOT EXISTS `final`.`review` (
  `review_id` INT NOT NULL AUTO_INCREMENT,
  `review_text` VARCHAR(250) NULL,
  `owner_id` INT NOT NULL,
  `author_id` INT NOT NULL,
  PRIMARY KEY (`review_id`),
  INDEX `fk_review_user1_idx` (`owner_id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `final`.`review_comments`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `final`.`review_comments` ;

CREATE TABLE IF NOT EXISTS `final`.`review_comments` (
  `comment_id` INT NOT NULL AUTO_INCREMENT,
  `comment` VARCHAR(250) NULL,
  `review_id` INT NOT NULL,
  `author_id` INT NOT NULL,
  PRIMARY KEY (`comment_id`),
  INDEX `fk_review_comments_review1_idx` (`review_id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `final`.`recommendation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `final`.`recommendation` ;

CREATE TABLE IF NOT EXISTS `final`.`recommendation` (
  `recommendation_id` INT NOT NULL AUTO_INCREMENT,
  `recommendation_text` VARCHAR(1000) NOT NULL,
  `owner_id` INT NOT NULL,
  `author_id` INT NOT NULL,
  PRIMARY KEY (`recommendation_id`),
  INDEX `fk_recommendation_user1_idx` (`owner_id` ASC))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `final`.`user`
-- -----------------------------------------------------
START TRANSACTION;
USE `final`;
INSERT INTO `final`.`user` (`user_id`, `username`, `password`, `type`) VALUES (NULL, 'admin', 'admin', 0);
INSERT INTO `final`.`user` (`user_id`, `username`, `password`, `type`) VALUES (NULL, 'user', 'user', 1);
INSERT INTO `final`.`user` (`user_id`, `username`, `password`, `type`) VALUES (NULL, 'penguin', 'penguin', 1);
INSERT INTO `final`.`user` (`user_id`, `username`, `password`, `type`) VALUES (NULL, 'hedgehog', 'hedgehog', 1);
INSERT INTO `final`.`user` (`user_id`, `username`, `password`, `type`) VALUES (NULL, 'pig', 'pig', 1);
INSERT INTO `final`.`user` (`user_id`, `username`, `password`, `type`) VALUES (NULL, 'panda', 'panda', 1);
INSERT INTO `final`.`user` (`user_id`, `username`, `password`, `type`) VALUES (NULL, 'clion', 'clion', 1);
INSERT INTO `final`.`user` (`user_id`, `username`, `password`, `type`) VALUES (NULL, 'ent', 'ent', 2);
INSERT INTO `final`.`user` (`user_id`, `username`, `password`, `type`) VALUES (NULL, 'cherry', 'cherry', 2);
INSERT INTO `final`.`user` (`user_id`, `username`, `password`, `type`) VALUES (NULL, 'sequoia', 'sequoia', 2);
INSERT INTO `final`.`user` (`user_id`, `username`, `password`, `type`) VALUES (NULL, 'bonsai', 'bonsai', 2);

COMMIT;


-- -----------------------------------------------------
-- Data for table `final`.`user_details`
-- -----------------------------------------------------
START TRANSACTION;
USE `final`;
INSERT INTO `final`.`user_details` (`user_id`, `first_name`, `last_name`, `address`, `birthday`, `info`) VALUES (NULL, 'admin', 'admin', 'admin', '1990-5-15', NULL);
INSERT INTO `final`.`user_details` (`user_id`, `first_name`, `last_name`, `address`, `birthday`, `info`) VALUES (NULL, 'user', 'user', 'user', '1990-4-20', 'no info');
INSERT INTO `final`.`user_details` (`user_id`, `first_name`, `last_name`, `address`, `birthday`, `info`) VALUES (NULL, 'penguin', 'penguin', 'North Pole', '1991-7-8', 'freezing');
INSERT INTO `final`.`user_details` (`user_id`, `first_name`, `last_name`, `address`, `birthday`, `info`) VALUES (NULL, 'hedgehog', 'hedgehog', 'Your garden', '1992-4-6', '');
INSERT INTO `final`.`user_details` (`user_id`, `first_name`, `last_name`, `address`, `birthday`, `info`) VALUES (NULL, 'pig', 'pig', NULL, '1987-4-8', 'I like mud');
INSERT INTO `final`.`user_details` (`user_id`, `first_name`, `last_name`, `address`, `birthday`, `info`) VALUES (NULL, 'panda', 'panda', NULL, '1991-8-12', NULL);
INSERT INTO `final`.`user_details` (`user_id`, `first_name`, `last_name`, `address`, `birthday`, `info`) VALUES (NULL, 'clion', 'cppide', 'assembly (bellow c)', '1993-1-28', 'I will beat VS');
INSERT INTO `final`.`user_details` (`user_id`, `first_name`, `last_name`, `address`, `birthday`, `info`) VALUES (NULL, 'ent', 'fangorn', 'Middle-Earth', '1400-9-20', 'I will fail you all');
INSERT INTO `final`.`user_details` (`user_id`, `first_name`, `last_name`, `address`, `birthday`, `info`) VALUES (NULL, 'cherry', 'pick', 'Sweeden', '1981-7-5', NULL);
INSERT INTO `final`.`user_details` (`user_id`, `first_name`, `last_name`, `address`, `birthday`, `info`) VALUES (NULL, 'General', 'Sherman', 'U.S.', '1709-6-7', NULL);
INSERT INTO `final`.`user_details` (`user_id`, `first_name`, `last_name`, `address`, `birthday`, `info`) VALUES (NULL, 'bonsai', 'chi', 'China', '1965-9-19', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `final`.`groups`
-- -----------------------------------------------------
START TRANSACTION;
USE `final`;
INSERT INTO `final`.`groups` (`group_id`, `name`) VALUES (NULL, 'red');
INSERT INTO `final`.`groups` (`group_id`, `name`) VALUES (NULL, 'green');
INSERT INTO `final`.`groups` (`group_id`, `name`) VALUES (NULL, 'blue');
INSERT INTO `final`.`groups` (`group_id`, `name`) VALUES (NULL, 'yellow');

COMMIT;


-- -----------------------------------------------------
-- Data for table `final`.`user_group`
-- -----------------------------------------------------
START TRANSACTION;
USE `final`;
INSERT INTO `final`.`user_group` (`user_id`, `group_id`) VALUES (2, 1);
INSERT INTO `final`.`user_group` (`user_id`, `group_id`) VALUES (3, 1);
INSERT INTO `final`.`user_group` (`user_id`, `group_id`) VALUES (5, 1);
INSERT INTO `final`.`user_group` (`user_id`, `group_id`) VALUES (2, 2);
INSERT INTO `final`.`user_group` (`user_id`, `group_id`) VALUES (3, 2);
INSERT INTO `final`.`user_group` (`user_id`, `group_id`) VALUES (4, 2);
INSERT INTO `final`.`user_group` (`user_id`, `group_id`) VALUES (4, 3);
INSERT INTO `final`.`user_group` (`user_id`, `group_id`) VALUES (5, 3);
INSERT INTO `final`.`user_group` (`user_id`, `group_id`) VALUES (6, 3);
INSERT INTO `final`.`user_group` (`user_id`, `group_id`) VALUES (8, 1);
INSERT INTO `final`.`user_group` (`user_id`, `group_id`) VALUES (9, 1);
INSERT INTO `final`.`user_group` (`user_id`, `group_id`) VALUES (11, 1);

COMMIT;


-- -----------------------------------------------------
-- Data for table `final`.`review`
-- -----------------------------------------------------
START TRANSACTION;
USE `final`;
INSERT INTO `final`.`review` (`review_id`, `review_text`, `owner_id`, `author_id`) VALUES (NULL, 'review from penguin to user', 2, 3);
INSERT INTO `final`.`review` (`review_id`, `review_text`, `owner_id`, `author_id`) VALUES (NULL, 'review from pig to user', 2, 5);
INSERT INTO `final`.`review` (`review_id`, `review_text`, `owner_id`, `author_id`) VALUES (NULL, 'review from hedgehog to user', 2, 4);
INSERT INTO `final`.`review` (`review_id`, `review_text`, `owner_id`, `author_id`) VALUES (NULL, 'review from user to pig', 5, 2);
INSERT INTO `final`.`review` (`review_id`, `review_text`, `owner_id`, `author_id`) VALUES (NULL, 'review from hedgehog to pig', 5, 4);

COMMIT;


-- -----------------------------------------------------
-- Data for table `final`.`review_comments`
-- -----------------------------------------------------
START TRANSACTION;
USE `final`;
INSERT INTO `final`.`review_comments` (`comment_id`, `comment`, `review_id`, `author_id`) VALUES (NULL, 'comment from pig', 1, 5);
INSERT INTO `final`.`review_comments` (`comment_id`, `comment`, `review_id`, `author_id`) VALUES (NULL, 'from hedgehog', 1, 4);
INSERT INTO `final`.`review_comments` (`comment_id`, `comment`, `review_id`, `author_id`) VALUES (NULL, 'from hedgehog', 2, 4);
INSERT INTO `final`.`review_comments` (`comment_id`, `comment`, `review_id`, `author_id`) VALUES (NULL, 'bla pig', 3, 5);
INSERT INTO `final`.`review_comments` (`comment_id`, `comment`, `review_id`, `author_id`) VALUES (NULL, 'from penguin', 4, 3);
INSERT INTO `final`.`review_comments` (`comment_id`, `comment`, `review_id`, `author_id`) VALUES (NULL, 'from', 5, 2);

COMMIT;


-- -----------------------------------------------------
-- Data for table `final`.`recommendation`
-- -----------------------------------------------------
START TRANSACTION;
USE `final`;
INSERT INTO `final`.`recommendation` (`recommendation_id`, `recommendation_text`, `owner_id`, `author_id`) VALUES (NULL, 'recommendation from ent to user ', 2, 8);
INSERT INTO `final`.`recommendation` (`recommendation_id`, `recommendation_text`, `owner_id`, `author_id`) VALUES (NULL, 'recommendation from ent to pig', 5, 8);
INSERT INTO `final`.`recommendation` (`recommendation_id`, `recommendation_text`, `owner_id`, `author_id`) VALUES (NULL, 'recomm from cherry to user', 2, 9);
INSERT INTO `final`.`recommendation` (`recommendation_id`, `recommendation_text`, `owner_id`, `author_id`) VALUES (NULL, 'rec from bonsai to user', 2, 11);

COMMIT;

