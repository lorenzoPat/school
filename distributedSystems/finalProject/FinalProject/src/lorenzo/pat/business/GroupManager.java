package lorenzo.pat.business;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lorenzo.pat.dao.GroupDao;
import lorenzo.pat.dao.GroupDaoImpl;
import lorenzo.pat.entities.Group;
import lorenzo.pat.entities.User;

public class GroupManager implements Serializable {

  private static final long serialVersionUID = 1L;
  private GroupDao dao;
  
  public GroupManager() {
    dao = new GroupDaoImpl();
  }
  
  public void addGroup(Group group) {
    dao.create(group);
  }
  
  public void deleteGroup(Integer groupId) {
    dao.remove(groupId);
  }
  
  public void updateGroup(Group group) {
    dao.update(group);
  }
  
  public List<Group> getAllGroups() {
    return dao.getAll();
  }
  
  public Integer getGroupsNr() {
    return dao.getAll().size();
  }
  
  public Group getGroup(Integer groupId) {
    return dao.getGroupById(groupId);
  }
  
  public List<User> getAssignedUsersForGroup(Group group) {
    return new ArrayList<User>(group.getAssignedUsers());
  }
  
  
}
