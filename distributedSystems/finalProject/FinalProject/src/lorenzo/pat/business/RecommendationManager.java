package lorenzo.pat.business;

import java.io.Serializable;
import java.util.List;
import lorenzo.pat.dao.RecommendationDao;
import lorenzo.pat.dao.RecommendationDaoImpl;
import lorenzo.pat.entities.Recommendation;
import lorenzo.pat.entities.User;

public class RecommendationManager implements Serializable {

  public RecommendationManager() {
    dao = new RecommendationDaoImpl();
  }

  public void addRecommendation(Recommendation recommendation) {
    dao.create(recommendation);
  }

  public void deleteRecommendation(Integer recommendationId) {
    dao.remove(recommendationId);
  }

  public void updateRecommendation(Recommendation recommendation) {
    dao.update(recommendation);
  }

  public List<Recommendation> getAllRecommendations() {
    return dao.getAll();
  }

  public Integer getRecommendationsNr() {
    return dao.getAll().size();
  }

  public Recommendation getRecommendation(Integer recommendationId) {
    return dao.getRecommendationById(recommendationId);
  }

  public Recommendation getRecommendationForUser(User owner, Integer authorId) {
    for (Recommendation recommendation : owner.getRecommendations()) {
      if (recommendation.getAuthorId() == authorId)
        return recommendation;
    }
    
    return null;
  }

  private RecommendationDao dao;
  private static final long serialVersionUID = 1L;
}
