package lorenzo.pat.business;

import java.io.Serializable;

import lorenzo.pat.dao.CommentDao;
import lorenzo.pat.dao.CommentDaoImpl;
import lorenzo.pat.dao.ReviewDao;
import lorenzo.pat.dao.ReviewDaoImpl;
import lorenzo.pat.entities.Comment;
import lorenzo.pat.entities.Review;

public class ReviewManager implements Serializable {

  public ReviewManager() {
    reviewDao = new ReviewDaoImpl();
    commentDao = new CommentDaoImpl();
  }
  
  public void addReview(Review review) {
    reviewDao.create(review);
  }
  
  public void updateReview(Review review) {
    reviewDao.update(review);
  }
  
  public void removeReview(Integer reviewId) {
    reviewDao.remove(reviewId);
  }
  
  public void addComment(Comment comment) {
    commentDao.create(comment);
  }
  
  public void updateComment(Comment comment) {
    commentDao.update(comment);
  }
  
  public void removeComment(Integer commentId) {
    commentDao.remove(commentId);
  }
  
  ReviewDao reviewDao;
  CommentDao commentDao;
  
  private static final long serialVersionUID = 1L;

}
