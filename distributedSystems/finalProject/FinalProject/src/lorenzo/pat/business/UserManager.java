package lorenzo.pat.business;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import lorenzo.pat.dao.UserDao;
import lorenzo.pat.dao.UserDaoImpl;
import lorenzo.pat.entities.Group;
import lorenzo.pat.entities.User;

public class UserManager implements Serializable {
  private static final long serialVersionUID = 1L;
  private UserDao dao = new UserDaoImpl();
  private User loggedUser = new User();

  public UserManager() {
  }

  public boolean isValidUser(String[] credentials) {
    List<User> allUsers = dao.getAll();

    for (User user : allUsers) {
      if (user.getUsername().equals(credentials[0])
          && user.getPassword().equals(credentials[1])) {
        setLoggedUser(user);
        return true;
      }
    }

    return false;
  }

  public boolean isAdmin(String[] credentials) {
    List<User> allUsers = dao.getAll();

    for (User user : allUsers) {
      if (user.getUsername().equals(credentials[0]) && user.getType() == 0) {
        return true;
      }
    }

    return false;
  }

  public boolean isStudent(String[] credentials) {
    List<User> allUsers = dao.getAll();

    for (User user : allUsers) {
      if (user.getUsername().equals(credentials[0]) && user.getType() == 1) {
        return true;
      }
    }

    return false;
  }

  public boolean isTeacher(String[] credentials) {
    List<User> allUsers = dao.getAll();

    for (User user : allUsers) {
      if (user.getUsername().equals(credentials[0]) && user.getType() == 2) {
        return true;
      }
    }

    return false;
  }
  
  public List<User> getAssignedStudentsForTeacher(User teacher) {
    Set<Group> assignedGroups = teacher.getGroups();
    List<User> assignedStudents = new ArrayList<>();
   
    for (Group group : assignedGroups) {
      Set<User> allUsersInGroup = group.getAssignedUsers();
      for (User user : allUsersInGroup) {
        if (!assignedStudents.contains(user) && user.getType() == 1) {
          assignedStudents.add(user);
        }
      }
    }
    
    return assignedStudents;
  }

  public List<User> getColleaguesForStudent(User student) {
    Set<Group> assignedGroups = student.getGroups();
    List<User> colleagues = new ArrayList<>();
   
    for (Group group : assignedGroups) {
      Set<User> allUsersInGroup = group.getAssignedUsers();
      for (User user : allUsersInGroup) {
        if (!colleagues.contains(user) && user.getType() == 1 && !user.equals(student)) {
          colleagues.add(user);
        }
      }
    }
    
    return colleagues;
  }
  
  public User getLoggedUser() {
    return loggedUser;
  }

  public List<User> getAllUsers() {
    return dao.getAll();
  }
  
  public void addUser(User user) {
    dao.create(user);
  }
  
  public void deleteUser(Integer userId) {
    dao.remove(userId);
  }
  
  public void updateUser(User user) {
    dao.update(user);
  }
  
  public User getUser(Integer userId) {
    return dao.getUserById(userId);
  }

  private void setLoggedUser(User user) {
    loggedUser = user;
  }
  
  
}
