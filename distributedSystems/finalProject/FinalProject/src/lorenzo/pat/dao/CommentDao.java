package lorenzo.pat.dao;

import java.util.List;

import lorenzo.pat.entities.Comment;

public interface CommentDao {
  public void create(Comment comment);

  public void update(Comment comment);

  public List<Comment> getAll();

  public Comment getCommentById(Integer commentId);

  public void remove(Integer commentId);
}
