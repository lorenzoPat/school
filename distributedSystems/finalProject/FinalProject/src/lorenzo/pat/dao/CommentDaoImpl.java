package lorenzo.pat.dao;

import java.io.Serializable;
import java.util.List;

import lorenzo.pat.entities.Comment;

import org.hibernate.Session;

public class CommentDaoImpl extends DaoBase<Comment> implements CommentDao, Serializable {

  private static final long serialVersionUID = 1L;

  @Override
  public List<Comment> getAll() {
    Session session = getSession();
    @SuppressWarnings("unchecked")
    List<Comment> commentList = session.createQuery("from Comment").list();
    commitAndClose(session);
    return commentList;
  }

  @Override
  public Comment getCommentById(Integer commentId) {
    Session session = getSession();
    Comment comment = (Comment) session.load(Comment.class, commentId);
    commitAndClose(session);
    return comment;
  }

  @Override
  public void remove(Integer commentId) {
    Session session = getSession();
    Comment comment = (Comment) session.load(Comment.class, commentId);
    session.delete(comment);
    commitAndClose(session);
  }

}
