package lorenzo.pat.dao;

import java.util.List;

import lorenzo.pat.entities.Group;

public interface GroupDao {

  public void create(Group group);

  public void update(Group group);

  public List<Group> getAll();

  public Group getGroupById(Integer groupId);

  public void remove(Integer groupId);

}
