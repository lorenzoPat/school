package lorenzo.pat.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;

import lorenzo.pat.entities.Group;

public class GroupDaoImpl extends DaoBase<Group> implements GroupDao, Serializable {

  private static final long serialVersionUID = 1L;

  @Override
  public List<Group> getAll() {
    Session session = getSession();
    @SuppressWarnings("unchecked")
    List<Group> groupsList = session.createQuery("from Group").list();
    commitAndClose(session);
    return groupsList;
  }

  @Override
  public Group getGroupById(Integer groupId) {
    Session session = getSession();
    Group group = (Group) session.get(Group.class, groupId);
    commitAndClose(session);
    return group;
  }

  @Override
  public void remove(Integer groupId) {
    Session session = getSession();
    Group group = (Group) session.load(Group.class, groupId);
    session.delete(group);
    commitAndClose(session);
  }

}
