package lorenzo.pat.dao;

import java.util.List;

import lorenzo.pat.entities.Recommendation;

public interface RecommendationDao {
  public void create(Recommendation recommendation);

  public void update(Recommendation recommendation);

  public List<Recommendation> getAll();

  public Recommendation getRecommendationById(Integer recommendationId);

  public void remove(Integer recommendationId);
}
