package lorenzo.pat.dao;

import java.io.Serializable;
import java.util.List;

import lorenzo.pat.entities.Recommendation;

import org.hibernate.Session;

public class RecommendationDaoImpl extends DaoBase<Recommendation> implements RecommendationDao, Serializable {

  @Override
  public List<Recommendation> getAll() {
    Session session = getSession();
    @SuppressWarnings("unchecked")
    List<Recommendation> recommendationsList = session.createQuery("from Recommendation").list();
    commitAndClose(session);
    return recommendationsList;
  }

  @Override
  public Recommendation getRecommendationById(Integer recommendationId) {
    Session session = getSession();
    Recommendation recommendation = (Recommendation) session.get(Recommendation.class, recommendationId);
    commitAndClose(session);
    return recommendation;
  }

  @Override
  public void remove(Integer recommendationId) {
    Session session = getSession();
    Recommendation recommendation = (Recommendation) session.get(Recommendation.class, recommendationId);
    session.delete(recommendation);
    commitAndClose(session);
    
  }

  private static final long serialVersionUID = 1L;
}
