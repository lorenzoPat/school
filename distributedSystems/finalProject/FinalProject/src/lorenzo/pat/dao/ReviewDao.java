package lorenzo.pat.dao;

import java.util.List;

import lorenzo.pat.entities.Review;

public interface ReviewDao {
  public void create(Review review);

  public void update(Review review);

  public List<Review> getAll();

  public Review getReviewById(Integer reviewId);

  public void remove(Integer reviewId);
}
