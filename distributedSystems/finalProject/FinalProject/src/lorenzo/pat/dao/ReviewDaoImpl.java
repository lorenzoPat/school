package lorenzo.pat.dao;

import java.io.Serializable;
import java.util.List;

import lorenzo.pat.entities.Review;

import org.hibernate.Session;

public class ReviewDaoImpl extends DaoBase<Review> implements ReviewDao, Serializable {

  @Override
  public List<Review> getAll() {
    Session session = getSession();
    @SuppressWarnings("unchecked")
    List<Review> reviewsList = session.createQuery("from Review").list();
    commitAndClose(session);
    return reviewsList;
  }

  @Override
  public Review getReviewById(Integer reviewId) {
    Session session = getSession();
    Review review = (Review) session.get(Review.class, reviewId);
    commitAndClose(session);
    return review;
  }

  @Override
  public void remove(Integer reviewId) {
    Session session = getSession();
    Review review = (Review) session.load(Review.class, reviewId);
    session.delete(review);
    commitAndClose(session);
  }
  
  private static final long serialVersionUID = 1L;
}
