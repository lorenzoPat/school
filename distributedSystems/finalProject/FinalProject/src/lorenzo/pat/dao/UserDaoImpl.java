package lorenzo.pat.dao;

import java.io.Serializable;
import java.util.List;

import lorenzo.pat.entities.User;

import org.hibernate.Session;

public class UserDaoImpl extends DaoBase<User> implements UserDao, Serializable {

  private static final long serialVersionUID = 1L;

  @Override
  public List<User> getAll() {
    Session session = getSession();
    @SuppressWarnings("unchecked")
    List<User> usersList = session.createQuery("from User").list();
    commitAndClose(session);
    return usersList;
  }

  @Override
  public User getUserById(Integer userId) {
    Session session = getSession();
    User user = (User) session.get(User.class, userId);
    commitAndClose(session);
    return user;
  }

  @Override
  public void remove(Integer userId) {
    Session session = getSession();
    User user = (User) session.load(User.class, userId);
    session.delete(user);
    commitAndClose(session);
  }

}
