package lorenzo.pat.entities;

import java.io.Serializable;

public class Comment implements Serializable{

  public Comment() {}
  
  public Integer getCommentId() {
    return commentId;
  }

  public void setCommentId(Integer commentId) {
    this.commentId = commentId;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public Review getReview() {
    return review;
  }

  public void setReview(Review review) {
    this.review = review;
  }

  public Integer getAuthorId() {
    return authorId;
  }

  public void setAuthorId(Integer authorId) {
    this.authorId = authorId;
  }

  private Integer commentId;
  private String comment;
  private Review review;
  private Integer authorId;
  
  private static final long serialVersionUID = 1L;

}
