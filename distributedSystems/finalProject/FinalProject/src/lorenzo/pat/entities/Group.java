package lorenzo.pat.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Group implements Serializable {

  public Group() { }

  public Integer getGroupId() {
    return groupId;
  }

  public void setGroupId(Integer groupId) {
    this.groupId = groupId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Set<User> getAssignedUsers() {
    return assignedUsers;
  }

  public void setAssignedUsers(Set<User> assignedUsers) {
    this.assignedUsers = assignedUsers;
  }
  
  @Override
  public int hashCode() {
    return this.groupId;
  }
  
  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }

    if (getClass() != obj.getClass()) {
      return false;
    }

    final Group other = (Group) obj;

    return this.name.equals(other.name);
  }

  @Override
  public String toString() {
    return "Group{" + "groupId=" + groupId + ", name=" + name + '}';
  }

  private Integer groupId;
  private String name;
  private Set<User> assignedUsers = new HashSet<User>(0);

  private static final long serialVersionUID = 1L;  
}
