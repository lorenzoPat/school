package lorenzo.pat.entities;

import java.io.Serializable;

public class Recommendation implements Serializable{

  public Recommendation() {
  }

  public Integer getRecommendationId() {
    return recommendationId;
  }

  public void setRecommendationId(Integer recommendationId) {
    this.recommendationId = recommendationId;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public User getOwner() {
    return owner;
  }

  public void setOwner(User owner) {
    this.owner = owner;
  }

  public Integer getAuthorId() {
    return authorId;
  }

  public void setAuthorId(Integer authorId) {
    this.authorId = authorId;
  }

  @Override
  public String toString() {
    return "Recommendation{" + "recommendationId=" + recommendationId 
            + ", recommendationText=" + text 
            + ", owner=" + owner.getUsername() 
            + ", authorid=" + authorId + '}';
  }

  private Integer recommendationId;
  private String text;
  private Integer authorId;
  private User owner;
  
  private static final long serialVersionUID = 1L;
}
