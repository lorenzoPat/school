package lorenzo.pat.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Review implements Serializable{

  public Review() {
  }

  public Integer getReviewId() {
    return reviewId;
  }

  public void setReviewId(Integer reviewId) {
    this.reviewId = reviewId;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public User getOwner() {
    return owner;
  }

  public void setOwner(User owner) {
    this.owner = owner;
  }

  public Integer getAuthorId() {
    return authorId;
  }

  public void setAuthorId(Integer authorId) {
    this.authorId = authorId;
  }
  
  public Set<Comment> getComments() {
    return comments;
  }

  public void setComments(Set<Comment> comments) {
    this.comments = comments;
  }

  @Override
  public String toString() {
    return "Review {" + "reviewId=" + reviewId 
            + ", recommendationText=" + text 
            + ", owner=" + owner.getUsername() 
            + ", authorid=" + authorId + '}';
  }

  private Integer reviewId;
  private String text;
  private Integer authorId;
  private User owner;
  private Set<Comment> comments = new HashSet<Comment>(0);

  private static final long serialVersionUID = 1L;
}
