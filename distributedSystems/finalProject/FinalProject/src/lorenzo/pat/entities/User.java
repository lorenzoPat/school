package lorenzo.pat.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class User implements Serializable{

  public User() {
  }

  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }

  public UserDetails getUserDetails() {
    return userDetails;
  }

  public void setUserDetails(UserDetails userDetails) {
    this.userDetails = userDetails;
  }

  public Set<Group> getGroups() {
    return groups;
  }

  public void setGroups(Set<Group> groups) {
    this.groups = groups;
  }

  public Set<Recommendation> getRecommendations() {
    return recommendations;
  }

  public void setRecommendations(Set<Recommendation> recommendations) {
    this.recommendations = recommendations;
  }

  public Set<Review> getReviews() {
    return reviews;
  }

  public void setReviews(Set<Review> reviews) {
    this.reviews = reviews;
  }
  
  @Override
  public String toString() {
    return "User{" + "userId=" + userId + ", username=" + username + ", password=" + password + ", type=" + type + '}';
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }

    if (getClass() != obj.getClass()) {
      return false;
    }

    final User other = (User) obj;

    return this.username.equals(other.getUsername()) && this.password.equals(other.getPassword());
  }

  private Integer userId;
  private String username;
  private String password;
  private Integer type;
  private UserDetails userDetails;
  private Set<Group> groups = new HashSet<Group>(0);
  private Set<Recommendation> recommendations = new HashSet<Recommendation>(0);
  private Set<Review> reviews = new HashSet<Review>(0);
    
  private static final long serialVersionUID = 1L;
}
