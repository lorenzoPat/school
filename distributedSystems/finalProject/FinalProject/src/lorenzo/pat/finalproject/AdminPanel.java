package lorenzo.pat.finalproject;

import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

public class AdminPanel extends Panel {

  private VerticalLayout mainPanel;
  private Panel bottomPanel;
  //private CRUDPanel crudPanel;
  //private AdministrationPanel administrationPanel;
  
  private MenuBar menu;
  private MenuItem logOutItem;
  
  private static final long serialVersionUID = 1L;
  
  public AdminPanel() {    
    mainPanel = new VerticalLayout();
    bottomPanel = new Panel();
    menu = new MenuBar();
    
    initializeHeader();
    initializeContent();
    
  }
  
  public void initializeContent() {
    mainPanel.addComponent(bottomPanel);
    setContent(mainPanel);
  }
  
  private void initializeHeader() {
    HorizontalLayout header = new HorizontalLayout();
    header.addComponent(menu);
    
    menu.addItem("CRUD", getCrudCommand());
    menu.addItem("Administration", getAdministrationCommand());
    logOutItem = menu.addItem("Log out", null);
    
    header.setWidth("100%");
    
    mainPanel.addComponent(header);
  }
  
  private MenuBar.Command getCrudCommand() {
    return new MenuBar.Command() {
      
      @Override
      public void menuSelected(MenuItem selectedItem) {
        bottomPanel.setContent(new CRUDPanel());
      }
      
      private static final long serialVersionUID = 1L;
    };
  }
  
  private MenuBar.Command getAdministrationCommand() {
    return new MenuBar.Command() {
      
      @Override
      public void menuSelected(MenuItem selectedItem) {
        bottomPanel.setContent(new AdministrationPanel());
      }
      
      private static final long serialVersionUID = 1L;
    };
  }
    
  public void addMenuCommandForLogout(MenuBar.Command command) {
    logOutItem.setCommand(command);
  }
  
}
