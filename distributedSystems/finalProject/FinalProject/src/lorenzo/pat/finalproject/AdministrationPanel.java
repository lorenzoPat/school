package lorenzo.pat.finalproject;

import java.util.ArrayList;
import java.util.List;
import lorenzo.pat.business.GroupManager;
import lorenzo.pat.business.UserManager;
import lorenzo.pat.entities.Group;
import lorenzo.pat.entities.User;
import com.vaadin.data.Item;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;

// An array of buttons is used for adding users to specific groups.
// For each group a button is created. Using setValue we can set the value of the
// button to be the id of the group it represents.
public class AdministrationPanel extends HorizontalSplitPanel {

  public AdministrationPanel() {
    groupManager = new GroupManager();
    userManager = new UserManager();
    user = new User();

    usersPanel = new UsersTablePanel(userManager.getAllUsers());
    groupButtons = new Button[groupManager.getGroupsNr()];
    groupPanels = new GroupPanel[groupManager.getGroupsNr()];
    int i = 0;
    for (Group group : groupManager.getAllGroups()) {
      groupButtons[i] = new Button();
      groupPanels[i] = new GroupPanel(groupManager, group.getGroupId());
      i++;
    }

    initialize();
  }

  private void initialize() {
    initializeLeftPanel();
    initializeRightPanel();
  }

  private void initializeLeftPanel() {
    VerticalLayout leftPanel = new VerticalLayout();
    HorizontalLayout buttonsPanel = new HorizontalLayout();

    initializeButtons();
    addTableClickListener();
    addGroupButtonsClickListeners();

    for (int i = 0; i < groupManager.getGroupsNr(); i++) {
      buttonsPanel.addComponent(groupButtons[i]);
    }

    leftPanel.addComponent(usersPanel);
    leftPanel.addComponent(buttonsPanel);

    addComponent(leftPanel);
  }

  private void initializeButtons() {
    List<Group> allGroups = groupManager.getAllGroups();
    int i = 0;

    for (Group group : allGroups) {
      groupButtons[i].setCaption("Add to " + group.getName());
      groupButtons[i].setData(group.getGroupId());
      groupButtons[i].setEnabled(false);
      i++;
    }
  }

  private void addTableClickListener() {
    usersPanel.addClickListener(new ItemClickListener() {
      @Override
      public void itemClick(ItemClickEvent event) {
        Object itemId = event.getItemId();
        Item data = usersPanel.getSelectedData(itemId);

        Integer userId = (Integer) data.getItemProperty(UsersTable.USER_ID)
            .getValue();
        user = userManager.getUser(userId);
        updateGroupButtonsStatus(user);
      }

      private static final long serialVersionUID = 1L;
    });
  }

  // Global user is set when an item in the table is selected.
  // When a button is clicked the user is added into the group and the buttons
  // status is updated.
  private void addGroupButtonsClickListeners() {
    for (int i = 0; i < groupManager.getGroupsNr(); i++) {
      groupButtons[i].addClickListener(new Button.ClickListener() {

        @Override
        public void buttonClick(ClickEvent event) {
          Integer groupId = (Integer) event.getButton().getData();
          Group tmpGroup = groupManager.getGroup(groupId);

          tmpGroup.getAssignedUsers().add(user);
          user.getGroups().add(tmpGroup);

          userManager.updateUser(user);
          groupManager.updateGroup(tmpGroup);

          updateGroupButtonsStatus(user);
          updateAllTables();
        }
        
        private static final long serialVersionUID = 1L;
      });
    }
  }

  // Buttons that correspond to groups the user belongs to are set to disabled
  // The rest of the buttons are set to enabled
  private void updateGroupButtonsStatus(User selectedUser) {
    for (int i = 0; i < groupManager.getGroupsNr(); i++) {
      Integer groupId = (Integer) groupButtons[i].getData();
      Group tmpGroup = groupManager.getGroup(groupId);
      groupButtons[i].setEnabled(!selectedUser.getGroups().contains(tmpGroup));
    }
  }

  private void initializeRightPanel() {
    HorizontalLayout rightPanel = new HorizontalLayout();

    for (int i = 0; i < groupManager.getGroupsNr(); i++) {
      rightPanel.addComponent(groupPanels[i]);
    }

    addComponent(rightPanel);
  }

  private void updateAllTables() {
    int i = 0;
    for (Group group : groupManager.getAllGroups()) {
      groupPanels[i].updateUsersTable(new ArrayList<User>(group
          .getAssignedUsers()));
      i++;
    }

  }

  private GroupManager groupManager;
  private UserManager userManager;
  private User user;

  private UsersTablePanel usersPanel;
  private Button groupButtons[];
  private GroupPanel groupPanels[];

  private static final long serialVersionUID = 1L;

}
