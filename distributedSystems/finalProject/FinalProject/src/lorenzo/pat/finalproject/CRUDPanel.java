package lorenzo.pat.finalproject;

import lorenzo.pat.business.UserManager;
import lorenzo.pat.entities.User;

import com.vaadin.data.Item;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.Button.ClickEvent;

public class CRUDPanel extends HorizontalSplitPanel {

  public CRUDPanel() {
    userManager = new UserManager();
    usersPanel = new UsersTablePanel(userManager.getAllUsers());
    userDetailsPanel = new UserDetailsPanel();

    initialize();
  }

  private void initialize() {
    addComponent(usersPanel);
    addComponent(userDetailsPanel);
    
    addTableClickListener();
    addClearBtnClickListener();
    addAddBtnClickListener();
    addDeleteBtnClickListener();
    addUpdateBtnClickListener();
  }

  private void addTableClickListener() {
    usersPanel.addClickListener(new ItemClickListener() {
      @Override
      public void itemClick(ItemClickEvent event) {
        Object itemId = event.getItemId();
        fillFieldsWithData(usersPanel.getSelectedData(itemId));
      }
      
      private static final long serialVersionUID = 1L;
    });
  }

  private void fillFieldsWithData(Item data) {
    userDetailsPanel.fillFieldsWithData(data);
  }

  private void addClearBtnClickListener() {
    userDetailsPanel.addClearBtnClickListener(new ClickListener() {
      @Override
      public void buttonClick(ClickEvent event) {
        userDetailsPanel.emptyFields();
      }

      private static final long serialVersionUID = 1L;
    });
  }

  private void addAddBtnClickListener() {
    userDetailsPanel.addAddBtnClickListener(new ClickListener() {
      @Override
      public void buttonClick(ClickEvent event) {
        User newUser = userDetailsPanel.getNewUser();
        userManager.addUser(newUser);
        usersPanel.updateUsersTable(userManager.getAllUsers());
      }
      
      private static final long serialVersionUID = 1L;
    });
  }
  
  private void addDeleteBtnClickListener() {
    userDetailsPanel.addDeleteBtnClickListener(new ClickListener() {
      @Override
      public void buttonClick(ClickEvent event) {
        Integer userId = userDetailsPanel.getSelectedUserId();
        userManager.deleteUser(userId);
        userDetailsPanel.emptyFields();
        usersPanel.updateUsersTable(userManager.getAllUsers());
      }
      
      private static final long serialVersionUID = 1L;
    });
  }
  
  private void addUpdateBtnClickListener() {
    userDetailsPanel.addUpdateBtnClickListener(new ClickListener() {
      @Override
      public void buttonClick(ClickEvent event) {
        User updateUser = userDetailsPanel.getUpdateUser();
        userDetailsPanel.emptyFields();
        userManager.updateUser(updateUser);
        usersPanel.updateUsersTable(userManager.getAllUsers());
      }
      
      private static final long serialVersionUID = 1L;
    });
  }
  
  private UserManager userManager;
  private UsersTablePanel usersPanel;
  private UserDetailsPanel userDetailsPanel;

  private static final long serialVersionUID = 1L;
}
