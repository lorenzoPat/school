package lorenzo.pat.finalproject;

import java.io.Serializable;

import com.vaadin.ui.Button;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;

import lorenzo.pat.business.UserManager;

public class Controller implements Serializable{
  private static final long serialVersionUID = 1L;
  private UserManager userManager;
  private FinalProjectMainFrame view;

  public Controller(FinalProjectMainFrame view) {
    this.view = view;
    userManager = new UserManager();
    view.setUserManager(userManager);
    
    
  }
  
  public void initialize() {
    addEventHandlers();
  }

  private void addEventHandlers() {
    addLoginHandler();
    addLogoutHandler();
  }

  private void addLoginHandler() {
    
    view.addClickListenerForLoginButton(new Button.ClickListener() {
      private static final long serialVersionUID = 1L;

      @Override
      public void buttonClick(Button.ClickEvent event) {
        String[] credentials = view.getLoginCredentials();
        if (userManager.isValidUser(credentials)) {
          view.setLoggedUser(userManager.getLoggedUser());
          if (userManager.isAdmin(credentials)) {
            System.out.println("redirect to admin");
            view.setPanel(FinalProjectMainFrame.ADMIN);
          }

          if (userManager.isStudent(credentials)) {
            System.out.println("redirect to student");
            view.setPanel(FinalProjectMainFrame.STUDENT);
          }

          if (userManager.isTeacher(credentials)) {
            System.out.println("redirect to teacher");
            view.setPanel(FinalProjectMainFrame.TEACHER);
          }
        } else {
          
        }
      }
    });
  }
  
  private void addLogoutHandler() {
    view.addMenuCommandForLogout(new MenuBar.Command() {
      
      private static final long serialVersionUID = 1L;

      @Override
      public void menuSelected(MenuItem selectedItem) {
        view.setPanel(FinalProjectMainFrame.LOGIN);
      }
    });
  }
}
