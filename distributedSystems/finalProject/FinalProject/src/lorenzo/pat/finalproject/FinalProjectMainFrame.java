package lorenzo.pat.finalproject;

import javax.servlet.annotation.WebServlet;




import lorenzo.pat.business.UserManager;
import lorenzo.pat.entities.User;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Button;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
@Theme("finalproject")
public class FinalProjectMainFrame extends UI {

  public static final String LOGIN = "login";
  public static final String STUDENT = "student";
  public static final String ADMIN = "admin";
  public static final String TEACHER = "teacher";


  private LoginPanel loginPanel;
  private StudentPanel studentPanel;
  private AdminPanel adminPanel;
  private TeacherPanel teacherPanel;
  private UserManager userManager;
  private User loggedUser;
  
  @WebServlet(value = "/*", asyncSupported = true)
  @VaadinServletConfiguration(productionMode = false, ui = FinalProjectMainFrame.class)
  public static class Servlet extends VaadinServlet {
  }
  
  @Override
  protected void init(VaadinRequest request) {
    Controller controller = new Controller(this);
    
    loginPanel = new LoginPanel();
    studentPanel = new StudentPanel(userManager);
    adminPanel = new AdminPanel();
    teacherPanel = new TeacherPanel(userManager);
    loggedUser = new User();
    
    controller.initialize();
   
    
    setPanel(LOGIN);
    
  }
  
  public void setPanel(String panelName) {
    if (panelName.equals(LOGIN)) {
      setContent(loginPanel);
    }
    
    if (panelName.equals(STUDENT)) {
      studentPanel.initializeContent();
      setContent(studentPanel);
    }
    
    if (panelName.equals(TEACHER))  {
//      User loggedUser = 
      teacherPanel.initializeContent();
      setContent(teacherPanel);
    }

    if (panelName.equals(ADMIN)) {
      setContent(adminPanel);
    }
  }
  
  public void setUserManager(UserManager userManager) {
    this.userManager = userManager;
  }

  //login methods
  public void addClickListenerForLoginButton(Button.ClickListener listener) {
    loginPanel.addClickListenerForLoginButton(listener);
  }

  public String[] getLoginCredentials() {
    return loginPanel.getLoginCredentials();
  }
  
  //logout
  public void addMenuCommandForLogout(MenuBar.Command command) {
    adminPanel.addMenuCommandForLogout(command);
    teacherPanel.addMenuCommandForLogout(command);
    studentPanel.addMenuCommandForLogout(command);
  }

  public User getLoggedUser() {
    return loggedUser;
  }

  public void setLoggedUser(User loggedUser) {
    this.loggedUser = loggedUser;
  }
  
  


	

}