package lorenzo.pat.finalproject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import lorenzo.pat.business.GroupManager;
import lorenzo.pat.business.UserManager;
import lorenzo.pat.entities.Group;
import lorenzo.pat.entities.User;

import com.vaadin.data.Item;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.VerticalLayout;

public class GroupPanel extends VerticalLayout {
  
  public GroupPanel(GroupManager groupManager, Integer groupId) {
    this.groupManager = groupManager;
    this.groupId = groupId;
    
    Group group = groupManager.getGroup(groupId);
    usersPanel = new UsersTablePanel(new ArrayList<User>(group.getAssignedUsers()));
    removeBtn = new Button();
    
    initialize();
  }
  
  public void updateUsersTable(List<User> usersList) {
    usersPanel.updateUsersTable(usersList);
  }
  
  
  private void initialize() {
    setCaption("Group " + groupManager.getGroup(groupId).getName());
    removeBtn.setCaption("Remove user");
    
    addBtnCListener();
    
    addComponent(usersPanel);
    addComponent(removeBtn);
  }
  
  private void addBtnCListener() {
    removeBtn.addClickListener(new ClickListener() {
      
      @Override
      public void buttonClick(ClickEvent event) {
        Item data = usersPanel.getSelectedData();
        Integer userId = (Integer) data.getItemProperty(UsersTable.USER_ID).getValue();
        
        Group group = groupManager.getGroup(groupId);
        Set<User> allUsers = group.getAssignedUsers();
        Iterator<User> it = allUsers.iterator();
        
        User user;
        
        while (it.hasNext()) {
          user = it.next();
          if (user.getUserId() == userId) {
            user.getGroups().remove(group);
            UserManager u = new UserManager();
            u.updateUser(user);
            it.remove();
          }
        }
        
        groupManager.updateGroup(group);
        
        updateUsersTable(new ArrayList<User>(group.getAssignedUsers()));
      }
      
      private static final long serialVersionUID = 1L;
    });
  }
  
  private UsersTablePanel usersPanel; 
  private Button removeBtn;
  private GroupManager groupManager;
  private Integer groupId;
    
  private static final long serialVersionUID = 1L;
}
