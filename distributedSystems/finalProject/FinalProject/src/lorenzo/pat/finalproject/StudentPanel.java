package lorenzo.pat.finalproject;

import java.util.ArrayList;

import lorenzo.pat.business.UserManager;
import lorenzo.pat.entities.User;

import com.vaadin.data.Item;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.MenuBar.MenuItem;

public class StudentPanel extends Panel {
  private static final long serialVersionUID = 1L;

  private MenuBar menu;
  private MenuItem logOutItem;
  private VerticalLayout skeletonPanel;
  private HorizontalLayout mainPanel;
  private VerticalLayout rightPanel;
  private VerticalLayout studentPanel;
  
  private UserManager userManager;
  private UsersTablePanel usersTablePanel;
  
  public StudentPanel(UserManager userManager) {
    this.userManager = userManager;
    skeletonPanel = new VerticalLayout();
    mainPanel = new HorizontalLayout();
    menu = new MenuBar();
    rightPanel = new VerticalLayout();  
    usersTablePanel = new UsersTablePanel(new ArrayList<User>());
    studentPanel = new VerticalLayout();
    
    initialize();
  }
  
  private void initialize() {
    HorizontalLayout header = new HorizontalLayout();
    header.addComponent(menu);
    
    menu.addItem("My profile", getProfileCommand());
    menu.addItem("Chage preferences", getPreferencesCommand());
    logOutItem = menu.addItem("Log out", null);
    header.setWidth("100%");
    
    
    skeletonPanel.addComponent(header);
    skeletonPanel.addComponent(mainPanel);
    
    setContent(skeletonPanel);
  }
  
  
  public void initializeContent() {
    initializeUsersTable();
    initializeStudentPanel();
    initializeRightPanel(userManager.getLoggedUser());
    mainPanel.addComponent(usersTablePanel);
    mainPanel.addComponent(rightPanel);
    
    rightPanel.setHeight("100%");
    rightPanel.setWidth("100%");    
  }
  
  public void initializeStudentPanel() {
    
    studentPanel.setWidth("100%");
    studentPanel.setHeight("100%");
  }
  
  private void initializeRightPanel(User user) {
    rightPanel.removeAllComponents();
    VerticalLayout infoPanel = new VerticalLayout();
    Label firstNameLbl = new Label("First Name: " + user.getUserDetails().getFirstName());
    Label lastNameLbl = new Label("Last Name: " + user.getUserDetails().getLastName());
    Label addressLbl = new Label("Address: " + user.getUserDetails().getAddress());
    Label birthdayLbl = new Label("Birthday: " + user.getUserDetails().getBirthday());
    Label infoLbl = new Label("About me: " + user.getUserDetails().getInfo());
    
    infoPanel.addComponent(firstNameLbl);
    infoPanel.addComponent(lastNameLbl);
    infoPanel.addComponent(addressLbl);
    infoPanel.addComponent(birthdayLbl);
    infoPanel.addComponent(infoLbl);
    
    rightPanel.addComponent(infoPanel);
    rightPanel.addComponent(studentPanel);
  }
  
  
  
  private void initializeUsersTable() {

    usersTablePanel.updateUsersTable(userManager.getColleaguesForStudent(userManager.getLoggedUser()));
    usersTablePanel.addClickListener(new ItemClickListener() {
      
      @Override
      public void itemClick(ItemClickEvent event) {
        Object itemId = event.getItemId();
        Item selectedItem = usersTablePanel.getSelectedData(itemId);
        
        Integer userId = (Integer)selectedItem.getItemProperty(UsersTable.USER_ID).getValue(); 
        User selectedUser = userManager.getUser(userId);
        initializeRightPanel(selectedUser);
        studentPanel.removeAllComponents();
        studentPanel.addComponent(new StudentReview(userManager, selectedUser));
      }
      
      private static final long serialVersionUID = 1L;
    });
  }
  
  
  private MenuBar.Command getProfileCommand() {
    return new MenuBar.Command() {
      
      @Override
      public void menuSelected(MenuItem selectedItem) {
        studentPanel.removeAllComponents();
        studentPanel.addComponent(new StudentReview(userManager, userManager.getLoggedUser()));
      }
      
      private static final long serialVersionUID = 1L;
    };
  }

  private MenuBar.Command getPreferencesCommand() {
    return new MenuBar.Command() {
      
      @Override
      public void menuSelected(MenuItem selectedItem) {
        studentPanel.removeAllComponents();
        studentPanel.addComponent(new StudentPreferencesPanel(userManager));
      }
      
      private static final long serialVersionUID = 1L;
    };
  }
  
  public void addMenuCommandForLogout(MenuBar.Command command) {
    logOutItem.setCommand(command);
  }
  
  
  class StudentPreferencesPanel extends VerticalLayout{
    
    StudentPreferencesPanel(UserManager userManager) {
      this.userManager = userManager;
      userIdField = new TextField();
      usernameField = new TextField();
      passwordField = new TextField();
      firstNameField = new TextField();
      lastNameField = new TextField();
      addressField = new TextField();
      birthdayField = new DateField();
      updateDetails = new Button("Update details");
      
      initialize();
    }
    
    private void initialize() {
      userIdField.setCaption(UsersTable.USER_ID);
      userIdField.setValue(String.valueOf(userManager.getLoggedUser().getUserId()));
      
      usernameField.setCaption(UsersTable.USERNAME);
      usernameField.setValue(userManager.getLoggedUser().getUsername());
      usernameField.setWidth("100%");

      passwordField.setCaption(UsersTable.PASSWORD);
      passwordField.setValue(userManager.getLoggedUser().getPassword());
      passwordField.setWidth("100%");

      firstNameField.setCaption(UsersTable.FIRST_NAME);
      firstNameField.setValue(userManager.getLoggedUser().getUserDetails().getFirstName());
      firstNameField.setWidth("100%");

      lastNameField.setCaption(UsersTable.LAST_NAME);
      lastNameField.setValue(userManager.getLoggedUser().getUserDetails().getLastName());
      lastNameField.setWidth("100%");

      addressField.setCaption(UsersTable.ADDRESS);
      addressField.setValue(userManager.getLoggedUser().getUserDetails().getAddress());
      addressField.setWidth("100%");

      birthdayField.setCaption(UsersTable.BIRTHDAY);
      
      birthdayField.setValue(userManager.getLoggedUser().getUserDetails().getBirthday());
      birthdayField.setWidth("100%");

      userIdField.setEnabled(false);
      usernameField.setEnabled(false);
      
      addComponent(userIdField);
      addComponent(usernameField);
      addComponent(passwordField);
      addComponent(firstNameField);
      addComponent(lastNameField);
      addComponent(addressField);
      addComponent(birthdayField);
      addComponent(updateDetails);
      
      addClickListenerToBtn();
      
      setWidth("100%");
    }
    
    private void addClickListenerToBtn() {
      updateDetails.addClickListener(new ClickListener() {

        @Override
        public void buttonClick(ClickEvent event) {
          User loggedUser = userManager.getLoggedUser();
          loggedUser.setPassword(passwordField.getValue());
          loggedUser.getUserDetails().setFirstName(firstNameField.getValue());
          loggedUser.getUserDetails().setLastName(lastNameField.getValue());
          loggedUser.getUserDetails().setAddress(addressField.getValue());
          loggedUser.getUserDetails().setBirthday(birthdayField.getValue());
          
          userManager.updateUser(loggedUser);
        }
        
        private static final long serialVersionUID = 1L;
      });
    }
    

    private TextField userIdField;
    private TextField usernameField;
    private TextField passwordField;
    private TextField firstNameField;
    private TextField lastNameField;
    private TextField addressField;
    private DateField birthdayField;
    private Button updateDetails;
    private UserManager userManager;
    
    private static final long serialVersionUID = 1L;  

  }
}
