package lorenzo.pat.finalproject;

import java.util.Set;

import lorenzo.pat.business.ReviewManager;
import lorenzo.pat.business.UserManager;
import lorenzo.pat.entities.Comment;
import lorenzo.pat.entities.Review;
import lorenzo.pat.entities.User;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;

public class StudentReview extends VerticalLayout {

  public StudentReview(UserManager userManager, User student) {
    this.student = student;
    this.userManager = userManager;
    
    initialize();
  }

  private void initialize() {
    Set<Review> allReviews = student.getReviews();

    for (Review review : allReviews) {
      addComponent(new ReviewPanel(review, userManager));
    }
    
    if (!userManager.getLoggedUser().equals(student))
      addComponent(new ReviewPanel(userManager));
  }

  private UserManager userManager;
  private User student;
  private static final long serialVersionUID = 1L;

  
  
  
  
  
  
  class ReviewPanel extends VerticalLayout {
    
    ReviewPanel(UserManager userManager) {
      this.userManager = userManager;
      reviewManager = new ReviewManager();
      reviewText = new TextArea();
      
      initializeNewReview(userManager);
    }

    ReviewPanel(Review review, UserManager userManager) {
      this.review = review;
      this.userManager = userManager;
      reviewManager = new ReviewManager();
      reviewText = new TextArea();

      initialize();
    }
    
    
    private void initializeNewReview(UserManager userManager) {
      reviewText.setCaption(userManager.getLoggedUser().getUserDetails().getFirstName()
          + " "
          + userManager.getLoggedUser().getUserDetails().getLastName());
      
      reviewText.setWidth("400px");
      reviewText.setHeight("100px");
      
      Button createBtn = new Button("CreateReview");
      createBtn.addClickListener(new ClickListener() {

        @Override
        public void buttonClick(ClickEvent event) {
          Review newReview = new Review();
          newReview.setAuthorId(userManager.getLoggedUser().getUserId());
          newReview.setText(reviewText.getValue());
          newReview.setOwner(student);
          student.getReviews().add(newReview);
          reviewManager.addReview(newReview);
        }
        
        private static final long serialVersionUID = 1L;
      });
      
      addComponent(reviewText);
      addComponent(createBtn);
    }
    
    private void initialize() {
      reviewText.setValue(review.getText());
      reviewText.setCaption(userManager.getUser(review.getAuthorId())
          .getUserDetails().getFirstName()
          + " "
          + userManager.getUser(review.getAuthorId()).getUserDetails()
              .getLastName());
      reviewText.setEnabled(review.getAuthorId() == userManager.getLoggedUser().getUserId() );
    
      reviewText.setWidth("400px");
      reviewText.setHeight("100px");
      
      addComponent(reviewText);
      
      //if the user that currently sees the review is the author, it has the possibility to update the review
      if (review.getAuthorId() == userManager.getLoggedUser().getUserId()) {
        Button updateBtn = new Button("Update review");
        addComponent(updateBtn);
        updateBtn.addClickListener(new ClickListener() {
          
          @Override
          public void buttonClick(ClickEvent event) {
            review.setText(reviewText.getValue());
            System.out.println("will update review " + review);
            reviewManager.updateReview(review);
          }
          
          private static final long serialVersionUID = 1L;
        });
      }

      //add comments for each review
      for (Comment comment : review.getComments()) {
        HorizontalLayout commentLayout = new HorizontalLayout();
        Label commentAuthor = new Label(userManager.getUser(comment.getAuthorId()).getUserDetails().getFirstName());
        TextField commentField = new TextField();
        
        commentField.setValue(comment.getComment());
        commentField.setEnabled(false);
        
        commentAuthor.setWidth("100px");
        commentField.setWidth("200px");
        
        commentLayout.addComponent(commentAuthor);
        commentLayout.addComponent(commentField);
        
        //the user that wrote the comment should have the possibility to update its comment
        if (comment.getAuthorId() == userManager.getLoggedUser().getUserId()) {
          commentField.setEnabled(true);
          Button updateBtn = new Button("Update comment");
          commentLayout.addComponent(updateBtn);
          updateBtn.addClickListener(new ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
              comment.setComment(commentField.getValue());
              System.out.println("will update comment " + comment.getAuthorId() + " " + comment.getComment() + " " + comment.getCommentId() + " " + comment.getReview());
              reviewManager.updateComment(comment);
            }
            
            private static final long serialVersionUID = 1L;
          });      
        }        
        
        addComponent(commentLayout);
      }
      
      
      HorizontalLayout newCommentLayout = new HorizontalLayout();
      Label commentAuthor = new Label(userManager.getLoggedUser().getUserDetails().getFirstName());
      TextField commentField = new TextField();
      Button createBtn = new Button("Add comment");
      
      commentAuthor.setWidth("100px");
      commentField.setWidth("200px");
      
      createBtn.addClickListener(new ClickListener() {
      
        @Override
        public void buttonClick(ClickEvent event) {
          Comment newComment = new Comment();
          newComment.setAuthorId(userManager.getLoggedUser().getUserId());
          newComment.setComment(commentField.getValue());
          newComment.setReview(review);
          review.getComments().add(newComment);
          reviewManager.addComment(newComment);
        }
        
        private static final long serialVersionUID = 1L;
      });
      
      newCommentLayout.addComponent(commentAuthor);
      newCommentLayout.addComponent(commentField);
      newCommentLayout.addComponent(createBtn);

      addComponent(newCommentLayout);
    }

    private Review review;
    private UserManager userManager;
    private ReviewManager reviewManager;
    private TextArea reviewText;
    
    private static final long serialVersionUID = 1L;
  }
}
