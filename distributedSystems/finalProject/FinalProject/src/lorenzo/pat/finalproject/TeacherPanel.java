package lorenzo.pat.finalproject;

import java.util.ArrayList;

import lorenzo.pat.business.RecommendationManager;
import lorenzo.pat.business.UserManager;
import lorenzo.pat.entities.Recommendation;
import lorenzo.pat.entities.User;

import com.vaadin.data.Item;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.VerticalLayout;

public class TeacherPanel extends Panel {
  
  public TeacherPanel(UserManager userManager) {
    this.userManager = userManager;
    skeletonPanel = new VerticalLayout();
    recommendationPanel = new VerticalLayout();
    rightPanel = new VerticalLayout();
    mainPanel = new HorizontalLayout();
    menu = new MenuBar();
    recommendationManager = new RecommendationManager();
    usersTablePanel = new UsersTablePanel(new ArrayList<User>());

    initialize();
  }

  public void initializeContent() {
    initializeUsersTable();
    initializeRightPanel(userManager.getLoggedUser());
    
    mainPanel.addComponent(usersTablePanel);
    mainPanel.addComponent(rightPanel);
    
    rightPanel.setHeight("100%");
    rightPanel.setWidth("100%");
  }
  
  private void initializeRecommendationPanel(User selectedUser) {
    recommendationPanel.removeAllComponents();
    recommendationPanel.setHeight("100%");
    recommendationPanel.setWidth("100%");

    recommendationText = new TextArea();
    addBtn = new Button("Post recommendation");
    updateBtn = new Button("Update recommendation");
    
    recommendationPanel.addComponent(recommendationText);
    recommendationText.setCaption("Recommendation for " + selectedUser.getUserDetails().getFirstName() + " " + selectedUser.getUserDetails().getLastName());
    recommendationText.setWidth("600px");
    recommendationText.setHeight("200px");

    HorizontalLayout footer = new HorizontalLayout();
    footer.addComponent(addBtn);
    footer.addComponent(updateBtn);
    recommendationPanel.addComponent(footer);
    
    if (recommendationManager.getRecommendationForUser(selectedUser, userManager.getLoggedUser().getUserId()) == null) {
      recommendationText.setValue("");
      updateBtn.setEnabled(false);
      addBtn.setEnabled(true);
    } else {
      recommendationText.setValue(recommendationManager.getRecommendationForUser(selectedUser, userManager.getLoggedUser().getUserId()).getText());
      addBtn.setEnabled(false);
      updateBtn.setEnabled(true);
    }
    
    addAddBtnClickListener(selectedUser);
    addUpdateBtnClickListener(selectedUser);
  }

  
  private void initializeRightPanel(User user) {
    rightPanel.removeAllComponents();
    VerticalLayout infoPanel = new VerticalLayout();
    Label firstNameLbl = new Label("First Name: " + user.getUserDetails().getFirstName());
    Label lastNameLbl = new Label("Last Name: " + user.getUserDetails().getLastName());
    Label addressLbl = new Label("Address: " + user.getUserDetails().getAddress());
    Label birthdayLbl = new Label("Birthday: " + user.getUserDetails().getBirthday());
    Label infoLbl = new Label("About me: " + user.getUserDetails().getInfo());
    
    infoPanel.addComponent(firstNameLbl);
    infoPanel.addComponent(lastNameLbl);
    infoPanel.addComponent(addressLbl);
    infoPanel.addComponent(birthdayLbl);
    infoPanel.addComponent(infoLbl);
    
    rightPanel.addComponent(infoPanel);
    rightPanel.addComponent(recommendationPanel);
  }

  private void initialize() {
    HorizontalLayout header = new HorizontalLayout();
    header.addComponent(menu);

    logOutItem = menu.addItem("Log out", null);
    header.setWidth("100%");

    skeletonPanel.addComponent(header);
    skeletonPanel.addComponent(mainPanel);

    setContent(skeletonPanel);
  }

  private void addAddBtnClickListener(User selectedUser) {
    addBtn.addClickListener(new ClickListener() {
      @Override
      public void buttonClick(ClickEvent event) {
        Recommendation recommendation = new Recommendation();
        recommendation.setText(recommendationText.getValue());
        recommendation.setAuthorId(userManager.getLoggedUser().getUserId());
        recommendation.setOwner(selectedUser);
        
        selectedUser.getRecommendations().add(recommendation);
        
        recommendationManager.addRecommendation(recommendation);
        initializeRecommendationPanel(selectedUser);//to set update btn on true
      }
      
      private static final long serialVersionUID = 1L;
    });
  }

  private void addUpdateBtnClickListener(User selectedUser) {
    updateBtn.addClickListener(new ClickListener() {
      @Override
      public void buttonClick(ClickEvent event) {
        Recommendation updRec = recommendationManager.getRecommendationForUser(selectedUser, userManager.getLoggedUser().getUserId());
        updRec.setText(recommendationText.getValue());
        
        recommendationManager.updateRecommendation(updRec);
      }
      
      private static final long serialVersionUID = 1L;
    });
  }
  
  private void initializeUsersTable() {
    usersTablePanel.updateUsersTable(userManager
        .getAssignedStudentsForTeacher(userManager.getLoggedUser()));
    usersTablePanel.addClickListener(new ItemClickListener() {

      @Override
      public void itemClick(ItemClickEvent event) {
        Object itemId = event.getItemId();
        Item selectedItem = usersTablePanel.getSelectedData(itemId);
        
        Integer userId = (Integer)selectedItem.getItemProperty(UsersTable.USER_ID).getValue(); 
        User selectedUser = userManager.getUser(userId);
        
        initializeRecommendationPanel(selectedUser);
        initializeRightPanel(selectedUser);
      }

      private static final long serialVersionUID = 1L;
    });
  }

  public void addMenuCommandForLogout(MenuBar.Command command) {
    logOutItem.setCommand(command);
  }

  private MenuBar menu;
  private MenuItem logOutItem;
  private VerticalLayout skeletonPanel;
  private HorizontalLayout mainPanel;
  private VerticalLayout recommendationPanel;
  private VerticalLayout rightPanel;
  private UserManager userManager;
  private UsersTablePanel usersTablePanel;
  private TextArea recommendationText;
  private Button addBtn;
  private Button updateBtn;
  private RecommendationManager recommendationManager;

  private static final long serialVersionUID = 1L;  
}
