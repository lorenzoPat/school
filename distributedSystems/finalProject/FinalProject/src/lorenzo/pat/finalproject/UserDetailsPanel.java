package lorenzo.pat.finalproject;

import java.util.Date;

import lorenzo.pat.entities.User;
import lorenzo.pat.entities.UserDetails;

import com.vaadin.ui.Button;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.data.Item;

public class UserDetailsPanel extends VerticalLayout {

  public UserDetailsPanel() {
    clearFieldsBtn = new Button("Clear");
    addUserBtn = new Button("Add user");
    deleteUserBtn = new Button("Delete user");
    updateUserBtn = new Button("Update user");

    userIdField = new TextField();
    usernameField = new TextField();
    passwordField = new TextField();
    typeField = new TextField();
    firstNameField = new TextField();
    lastNameField = new TextField();
    addressField = new TextField();
    birthdayField = new DateField();

    initialize();
  }

  public void fillFieldsWithData(Item data) {
    Integer userId = (Integer) data.getItemProperty(UsersTable.USER_ID).getValue();
    userIdField.setValue(Integer.toString(userId));

    usernameField.setValue((String) data.getItemProperty(UsersTable.USERNAME).getValue());
    passwordField.setValue((String) data.getItemProperty(UsersTable.PASSWORD).getValue());

    Integer type = (Integer) data.getItemProperty(UsersTable.TYPE).getValue();
    typeField.setValue(Integer.toString(type));

    firstNameField.setValue((String) data.getItemProperty(UsersTable.FIRST_NAME).getValue());
    lastNameField.setValue((String) data.getItemProperty(UsersTable.LAST_NAME).getValue());
    addressField.setValue((String) data.getItemProperty(UsersTable.ADDRESS).getValue());

    Date birthday = (Date) data.getItemProperty(UsersTable.BIRTHDAY).getValue();
    birthdayField.setValue(birthday);
  }
  
  public void addClearBtnClickListener(ClickListener listener) {
    clearFieldsBtn.addClickListener(listener);
  }
  
  public void addAddBtnClickListener(ClickListener listener) {
    addUserBtn.addClickListener(listener);
  }
  
  public void addDeleteBtnClickListener(ClickListener listener) {
    deleteUserBtn.addClickListener(listener);
  }
  
  public void addUpdateBtnClickListener(ClickListener listener) {
    updateUserBtn.addClickListener(listener);
  }
  
  public void emptyFields() {
    userIdField.setValue("");
    usernameField.setValue("");
    passwordField.setValue("");
    typeField.setValue("");
    firstNameField.setValue("");
    lastNameField.setValue("");
    addressField.setValue("");
    birthdayField.setValue(null);
  }
  
  public User getNewUser() {
    User newUser = new User();
    
    newUser.setUsername(usernameField.getValue());
    newUser.setPassword(passwordField.getValue());
    newUser.setType(Integer.parseInt(typeField.getValue()));

    UserDetails newUserDetails = new UserDetails();
    newUserDetails.setFirstName(firstNameField.getValue());
    newUserDetails.setLastName(lastNameField.getValue());
    newUserDetails.setAddress(addressField.getValue());
    newUserDetails.setBirthday(birthdayField.getValue());

    newUser.setUserDetails(newUserDetails);
    newUserDetails.setUser(newUser);

    return newUser;
  }
  
  public Integer getSelectedUserId() {
    return Integer.parseInt(userIdField.getValue());
  }
  
  public User getUpdateUser() {
    User updateUser = new User();
    
    updateUser.setUserId(Integer.parseInt(userIdField.getValue()));
    updateUser.setUsername(usernameField.getValue());
    updateUser.setPassword(passwordField.getValue());
    updateUser.setType(Integer.parseInt(typeField.getValue()));
   
    UserDetails userDetails = new UserDetails();
    userDetails.setFirstName(firstNameField.getValue());
    userDetails.setLastName(lastNameField.getValue());
    userDetails.setAddress(addressField.getValue());
    userDetails.setBirthday(birthdayField.getValue());
    
    userDetails.setUser(updateUser);
    //updateUser.setUserDetails(userDetails);///*-* for some reason fails if userDetails are set
    
    return updateUser;
  }
  

  private void initialize() {
    initializeFields();

    HorizontalLayout buttonsPanel = new HorizontalLayout();
    buttonsPanel.addComponent(clearFieldsBtn);
    buttonsPanel.addComponent(addUserBtn);
    buttonsPanel.addComponent(deleteUserBtn);
    buttonsPanel.addComponent(updateUserBtn);

    addComponent(userIdField);
    addComponent(usernameField);
    addComponent(passwordField);
    addComponent(typeField);
    addComponent(firstNameField);
    addComponent(lastNameField);
    addComponent(addressField);
    addComponent(birthdayField);
    addComponent(buttonsPanel);
  }

  private void initializeFields() {
    userIdField.setCaption(UsersTable.USER_ID);

    usernameField.setCaption(UsersTable.USERNAME);
    usernameField.setWidth("100%");

    passwordField.setCaption(UsersTable.PASSWORD);
    passwordField.setWidth("100%");

    typeField.setCaption(UsersTable.TYPE);
    typeField.setWidth("100%");

    firstNameField.setCaption(UsersTable.FIRST_NAME);
    firstNameField.setWidth("100%");

    lastNameField.setCaption(UsersTable.LAST_NAME);
    lastNameField.setWidth("100%");

    addressField.setCaption(UsersTable.ADDRESS);
    addressField.setWidth("100%");

    birthdayField.setCaption(UsersTable.BIRTHDAY);
    birthdayField.setWidth("100%");

    userIdField.setEnabled(false);
  }

  private Button clearFieldsBtn;
  private Button addUserBtn;
  private Button deleteUserBtn;
  private Button updateUserBtn;

  private TextField userIdField;
  private TextField usernameField;
  private TextField passwordField;
  private TextField typeField;
  private TextField firstNameField;
  private TextField lastNameField;
  private TextField addressField;
  private DateField birthdayField;

  private static final long serialVersionUID = 1L;

}
