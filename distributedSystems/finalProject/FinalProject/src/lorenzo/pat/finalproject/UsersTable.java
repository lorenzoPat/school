package lorenzo.pat.finalproject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lorenzo.pat.entities.User;

import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.Table;

public class UsersTable extends Table {

  public static final String USER_ID = "UserId";
  public static final String USERNAME = "Username";
  public static final String PASSWORD = "Password";
  public static final String TYPE = "Type";
  public static final String FIRST_NAME = "First name";
  public static final String LAST_NAME = "Last name";
  public static final String ADDRESS = "Address";
  public static final String BIRTHDAY = "Birthday";

  public UsersTable() {
    userList = new ArrayList<>();
    userContainer = new IndexedContainer();

    setSizeFull();
    setSelectable(true);
    setImmediate(true);
  }

  public void refreshContent() {
    userContainer = getUsersContainer();

    setContainerDataSource(userContainer);
    setVisibleColumns(new Object[] { USER_ID, USERNAME, FIRST_NAME, LAST_NAME });
  }
  
  public void filterUsersBy(String filter) {
    userContainer.removeAllContainerFilters();
    userContainer.addContainerFilter(new ContactFilter(filter));
  }

  public void setUsersList(List<User> usersList) {
    this.userList = usersList;
  }

  
  @SuppressWarnings("unchecked")//:)
  private IndexedContainer getUsersContainer() {
    IndexedContainer usrContainer = initializeUsersContainer();
    
    for (User user : userList) {
      Object id = usrContainer.addItem();
      usrContainer.getContainerProperty(id, USER_ID).setValue(user.getUserId());
      usrContainer.getContainerProperty(id, USERNAME).setValue(user.getUsername());
      usrContainer.getContainerProperty(id, PASSWORD).setValue(user.getPassword());
      usrContainer.getContainerProperty(id, TYPE).setValue(user.getType());
      usrContainer.getContainerProperty(id, FIRST_NAME).setValue(
          user.getUserDetails().getFirstName());
      usrContainer.getContainerProperty(id, LAST_NAME).setValue(
          user.getUserDetails().getLastName());
      usrContainer.getContainerProperty(id, ADDRESS).setValue(
          user.getUserDetails().getAddress());
      usrContainer.getContainerProperty(id, BIRTHDAY).setValue(
          user.getUserDetails().getBirthday());
    }

    return usrContainer;
  }

  private IndexedContainer initializeUsersContainer() {
    IndexedContainer ic = new IndexedContainer();

    ic.addContainerProperty(USER_ID, Integer.class, 0);
    ic.addContainerProperty(USERNAME, String.class, "");
    ic.addContainerProperty(PASSWORD, String.class, "");
    ic.addContainerProperty(TYPE, Integer.class, 0);
    ic.addContainerProperty(FIRST_NAME, String.class, "");
    ic.addContainerProperty(LAST_NAME, String.class, "");
    ic.addContainerProperty(ADDRESS, String.class, "");
    ic.addContainerProperty(BIRTHDAY, Date.class, null);

    return ic;
  }

  private class ContactFilter implements Container.Filter {
    private String filter;

    public ContactFilter(String filter) {
      this.filter = filter.toLowerCase();
    }

    @Override
    public boolean passesFilter(Object itemId, Item item) {
      String haystack = ("" + item.getItemProperty(UsersTable.USERNAME).getValue() 
          + item.getItemProperty(UsersTable.FIRST_NAME).getValue() 
          + item.getItemProperty(UsersTable.LAST_NAME).getValue()).toLowerCase();
      
      return haystack.contains(filter);
    }

    @Override
    public boolean appliesToProperty(Object id) {
      return true;
    }
    
    private static final long serialVersionUID = 1L;
  }
  
  private List<User> userList;

  private IndexedContainer userContainer;
  
  private static final long serialVersionUID = 1L;
}
