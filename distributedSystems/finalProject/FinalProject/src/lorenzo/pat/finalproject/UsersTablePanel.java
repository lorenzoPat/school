package lorenzo.pat.finalproject;

import java.util.List;

import lorenzo.pat.entities.User;

import com.vaadin.data.Item;
import com.vaadin.event.FieldEvents;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.ui.AbstractTextField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class UsersTablePanel extends VerticalLayout {

  public UsersTablePanel(List<User> usersList) {
    this.usersList = usersList;
    usersTable = new UsersTable();
    initialize();
  }

  public Item getSelectedData() {
    Object itemId = usersTable.getValue();
    return getSelectedData(itemId);
    
  }
  
  public Item getSelectedData(Object itemId) {
    return usersTable.getItem(itemId);
    
  }

  public void addClickListener(ItemClickListener clickListener) {
    usersTable.addItemClickListener(clickListener);
  }
  
  public void updateUsersTable(List<User> usersList) {
    usersTable.setUsersList(usersList);
    usersTable.refreshContent();
  }
  

  private void initialize() {
    initializeUsersTable();
    initializeSearchSection();

    addComponent(usersTable);
    addComponent(searchSection);

    setSizeFull();
  }

  private void initializeUsersTable() {
    updateUsersTable(usersList);
  }

  private void initializeSearchSection() {
    searchSection = new HorizontalLayout();
    searchField = new TextField();

    searchSection.addComponent(searchField);
    searchSection.setWidth("100%");
    searchSection.setExpandRatio(searchField, 1);

    searchField.setSizeFull();
    searchField.setInputPrompt("Search users");
    searchField.setTextChangeEventMode(AbstractTextField.TextChangeEventMode.LAZY);
    searchField.addTextChangeListener(new FieldEvents.TextChangeListener() {
      @Override
      public void textChange(final FieldEvents.TextChangeEvent event) {
        usersTable.filterUsersBy(event.getText());
      }

      private static final long serialVersionUID = 1L;
    });
  }

  
  private List<User>  usersList;
  private UsersTable usersTable;
  private HorizontalLayout searchSection;
  private TextField searchField;

  private static final long serialVersionUID = 1L;
}
