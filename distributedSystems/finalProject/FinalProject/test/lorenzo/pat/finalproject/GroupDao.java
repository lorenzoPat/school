package lorenzo.pat.finalproject;

import java.util.List;

import lorenzo.pat.dao.GroupDaoImpl;
import lorenzo.pat.entities.Group;
import org.junit.Before;
import org.junit.Test;

public class GroupDao {

  GroupDaoImpl instance = new GroupDaoImpl();
  
  @Before
  public void setUp() throws Exception {
    cleanGroupsTable();
    populateGroupsTable();
  }
  
  @Test
  public void create() {
    Group group = getTestGroup();
    instance.create(group);
    assert(isGroupInDB(group));
  }
  
  @Test
  public void remove() {
    Group group = getTestGroup();
    instance.create(group);
    instance.remove(group.getGroupId());
    assert (!isGroupInDB(group));
  }

  @Test
  public void update() {
    Group group = getTestGroup();
    instance.create(group);
    
    group.setName("newGroup");
    instance.update(group);

    assert (isGroupInDB(group));
  }
  
  private Group getTestGroup() {
    Group testGroup = new Group();
    testGroup.setName("testGroup");
    return testGroup;
  }
  
  private void cleanGroupsTable() {
    List<Group> groups = instance.getAll();
    for (Group group : groups) {
      instance.remove(group.getGroupId());
    }
  }

  private void populateGroupsTable() {
    Group group0 = new Group();
    Group group1 = new Group();
    Group group2 = new Group();
    Group group3 = new Group();
    Group group4 = new Group();
    
    group0.setName("group0");
    group1.setName("group1");
    group2.setName("group2");
    group3.setName("group3");
    group4.setName("group4");
    
    instance.create(group0);
    instance.create(group1);
    instance.create(group2);
    instance.create(group3);
    instance.create(group4);
  }

  private boolean isGroupInDB(Group group) {
    List<Group> allGroups = instance.getAll();
    return allGroups.contains(group);
  }

}
