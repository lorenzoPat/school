package lorenzo.pat.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import lorenzo.pat.model.PackageService;
import lorenzo.pat.model.UserService;
import lorenzo.pat.webservice.City;
import lorenzo.pat.webservice.User;
import org.tempuri.Package;
import org.tempuri1.PackageHistory;

@ManagedBean(name = "adminController")
@SessionScoped
public class AdminController {

  private UserService userService;
  private PackageService packageService;
  private Package pack;
  private List<PackageHistory> packageHistory;

  public AdminController() {
    userService = new UserService();
    pack = new Package();
    packageService = new PackageService();
    packageHistory = new ArrayList<>();
  }

  public String addPackage() {
    packageService.addPackage(pack);
    return "sysUser";
  }

  public void removePackage(int packageId) {
    packageService.removePackage(packageId);
  }

  public List<Package> getAllPackages() {
    return packageService.getAllPackages();
  }

  public String prepareUpdate(Package updatePackage) {
    packageHistory = packageService.getHistoryForPackage(updatePackage.getPackageId());
    pack = updatePackage;
    return "update";
  }

  public void registerForTracking(int packageId) {
    packageService.registerPackageForTracking(packageId);
  }

  public void updatePackageStatus(String status) {
    System.out.println("new status = " + status + " " + pack.getPackageId());
    packageService.updatePackageStatus(pack.getPackageId(), status);
  }

  public List<City> getAllCities() {
    return userService.getAllCities();
  }

  public String getCityName(int cityId) {
    return userService.getCity(cityId).getName();

  }

  public void addToHistory(int cityId) throws DatatypeConfigurationException {
    PackageHistory newLocation = new PackageHistory();
    newLocation.setCityId(cityId);
    newLocation.setPackageId(pack.getPackageId());

    packageService.addToHistory(newLocation);
    packageHistory = packageService.getHistoryForPackage(pack.getPackageId());//update history at page refresh
  }

  public List<User> getAllUsers() {
    return userService.getAllUsers();
  }

  public String redirectToAddPackage() {
    return "addPackage";
  }

  public String redirectToAdmin() {
    return "sysUser";
  }

  public String redirectToIndex() {
    return "index";
  }

  public Package getPack() {
    return pack;
  }

  public void setPack(Package pack) {
    this.pack = pack;
  }

  public List<PackageHistory> getPackageHistory() {
    return packageHistory;
  }

  public void setPackageHistory(List<PackageHistory> packageHistory) {
    this.packageHistory = packageHistory;
  }

}
