package lorenzo.pat.controller;

import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import lorenzo.pat.model.UserService;
import lorenzo.pat.webservice.City;
import lorenzo.pat.webservice.User;

@ManagedBean(name = "loginController")
@RequestScoped
public class LoginController {

  private UserService loginService;
  private User user;
  private User newUser;

  public LoginController() {
    loginService = new UserService();
    user = new User();
    newUser = new User();
  }

  public String checkLogin() {    
    if (!loginService.isValidLogin(user.getUsername(), user.getPassword())) {
      return "register";
    }

    user = loginService.getLoggedUser(user.getUsername());

    HttpSession session = getSession();
    session.setAttribute("loginUser", user);
    
    return redirectToPage(user);
  }
  
  public String registerUser() {
    newUser.setType(2);
    loginService.registerUser(newUser);
    return "index";
  }
  
  public String registerSysUser() {
    newUser.setType(1);
    loginService.registerUser(newUser);
    return "admin";
  }
  
  public List<City> getAllCities() {
    return loginService.getAllCities();
  }

  public String redirectToRegister() {
    return "register";
  }
  
  public String redirectToIndex() {
    return "index";
  }

  public String redirectToAdmin() {
    return "admin";
  }
  
  public String redirectToUser() {
    return "user";
  }
  
  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }
  
  public User getNewUser() {
    return newUser;
  }

  public void setNewUser(User newUser) {
    this.newUser = newUser;
  }
  
  private String redirectToPage(User user) {
    if ( loginService.isAdmin(user) )
      return "admin";
    
    if ( loginService.isSysUser(user) )
      return "sysUser";
    
    return "user";
  }
  
  private HttpSession getSession() {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
    return session;
  }

}
