package lorenzo.pat.controller;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import lorenzo.pat.model.PackageService;
import lorenzo.pat.model.UserService;
import lorenzo.pat.webservice.User;
import org.tempuri.Package;
import org.tempuri1.PackageHistory;

@ManagedBean
@RequestScoped
public class UserController {

  private PackageService packageService;
  private Package pack;
  private UserService userService;
  private User loggedUser;
  private List<PackageHistory> packageHistory;

  public UserController() {
    HttpSession session = getSession();
    loggedUser = (User) session.getAttribute("loginUser");
    pack = new Package();
    packageService = new PackageService();
    packageHistory = new ArrayList<>();
    userService = new UserService();
  }

  public String addPackage() {
    
    pack.setStatus("in deposit");
    pack.setTrackFlag(false);
    pack.setUserId(loggedUser.getUserId());
    System.out.println("from user " + pack.getTitle() + " " + pack.getPackageId());
    packageService.addPackage(pack);
    return "user";
  }
  
  public List<Package> getMyPackages() {
    return packageService.getPackagesForUser(loggedUser.getUserId());
  }

  public String getCityName(int cityId) {
    return userService.getCity(cityId).getName();

  }

  public String getHistoryForPackage(int packageId) {
    packageHistory = packageService.getHistoryForPackage(packageId);
    return "history";
  }
  
    public Package getPack() {
    return pack;
  }

  public void setPack(Package pack) {
    this.pack = pack;
  }

  public String redirectToIndex() {
    return "index";
  }

  public String redirectToUser() {
    return "user";
  }

  public List<PackageHistory> getPackageHistory() {
    return packageHistory;
  }

  public void setPackageHistory(List<PackageHistory> packageHistory) {
    this.packageHistory = packageHistory;
  }

  private HttpSession getSession() {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
    return session;
  }

}
