package lorenzo.pat.model;

import java.util.ArrayList;
import java.util.List;
import org.tempuri.Package;
import org.tempuri.Assignment51Soap;
import org.tempuri1.Assignment52Soap;
import org.tempuri1.PackageHistory;

public class PackageService {

  private Assignment51Soap packageWebService;
  private Assignment52Soap historyWebService;

  public PackageService() {
    packageWebService = getPackageWebService();
    historyWebService = getHistoryWebService();
  }

  public void addPackage(Package newPackage) {
    System.out.println("new pack " + newPackage.getTitle());
    packageWebService.addPackage(newPackage);
  }

  public List<Package> getAllPackages() {
    return packageWebService.getAllPackages().getPackage();
  }

  public void removePackage(int packageId) {
    packageWebService.removePackage(packageId);
  }

  public void registerPackageForTracking(int packageId) {
    packageWebService.registerPackageForTracking(packageId);
  }

  public List<Package> getPackagesForUser(int userId) {
    List<Package> allPackages = packageWebService.getAllPackages().getPackage();
    List<Package> result = new ArrayList<>();

    for (Package pack : allPackages) {
      if ((pack.getUserId() == userId) && pack.isTrackFlag()) {
        result.add(pack);
      }
    }

    return result;
  }

  public void updatePackageStatus(int packageId, String status) {
    historyWebService.updatePackageStatus(packageId, status);
  }

  public void addToHistory(PackageHistory newEntry) {
    historyWebService.addToPackageHistory(newEntry);
  }

  public List<PackageHistory> getHistoryForPackage(int packageId) {
    return historyWebService.getAllHistoryForPackage(packageId).getPackageHistory();
  }

  private Package getPackageById(int id) {
    return packageWebService.getPackageById(id);
  }

  private Assignment51Soap getPackageWebService() {
    org.tempuri.Assignment51 service = new org.tempuri.Assignment51();
    return service.getAssignment51Soap();
  }

  private Assignment52Soap getHistoryWebService() {
    org.tempuri1.Assignment52 service = new org.tempuri1.Assignment52();
    return service.getAssignment52Soap();
  }

}
