package lorenzo.pat.model;

import java.util.List;
import lorenzo.pat.webservice.City;
import lorenzo.pat.webservice.User;
import lorenzo.pat.webservice.JavaAssignment5;

public class UserService {

  JavaAssignment5 webService;

  public UserService() {
    webService = getWebServiceReference();
  }

  public boolean isValidLogin(String username, String password) {
    return webService.isValidLogin(username, password);
  }

  public User getLoggedUser(String username) {
    return webService.getLoggedUser(username);
  }

  public boolean isAdmin(User user) {
    return webService.isAdmin(user.getUsername());
  }
  
  public boolean isSysUser(User user) {
    return webService.isSysUser(user.getUsername());
  }

  public void registerUser(User newUser) {
    webService.registerUser(newUser);
  }

  public List<City> getAllCities() {
    return webService.getAllCities();
  }

  public City getCity(int cityId) {
    List<City> allCities = webService.getAllCities();
    for (City city : allCities) {
      if (city.getCityId() == cityId) {
        return city;
      }
    }

    return null;
  }

  public List<User> getAllUsers() {
    return webService.getAllUsers();
  }

  private lorenzo.pat.webservice.JavaAssignment5 getWebServiceReference() {
    lorenzo.pat.webservice.JavaAssignment5_Service service = new lorenzo.pat.webservice.JavaAssignment5_Service();
    return service.getJavaAssignment5Port();
  }

}
