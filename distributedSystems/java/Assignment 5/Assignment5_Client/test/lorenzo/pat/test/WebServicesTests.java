package lorenzo.pat.test;

import java.util.List;
import lorenzo.pat.webservice.JavaAssignment5;
import lorenzo.pat.webservice.User;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.tempuri.Assignment51Soap;
import org.tempuri.Package;

public class WebServicesTests {

  private JavaAssignment5 userWebService = getWebServiceReference();
  private Assignment51Soap packageWebService = getPackageWebService();

  public WebServicesTests() {
  }

  @Before
  public void setUp() {
  }

  @After
  public void tearDown() {
  }

  @Test
  public void registerUser() {
    User user = new User();
    user.setUsername("testUser1");
    user.setType(1);
    user.setPassword("testPass");
    user.setAddress("testaddr");
    user.setFullName("full");
    user.setCityId(1);
    userWebService.registerUser(user);
    
    assert(isUserInDB(user));
  }

  @Test
  public void isAdmin() {
    User user = new User();
    List<User> allUsers = userWebService.getAllUsers();

    for (User u : allUsers) {
      if (u.getType() == 0) {
        user = u;
      }
    }

    assert (userWebService.isAdmin(user.getUsername()));
  }

  @Test
  public void isSystemUser() {
    User user = new User();
    List<User> allUsers = userWebService.getAllUsers();

    for (User u : allUsers) {
      if (u.getType() == 1) {
        user = u;
      }
    }

    assert (userWebService.isSysUser(user.getUsername()));
  }
  
  @Test
  public void addPackage() {
    Package pack = new Package();
    pack.setTitle("testPack");
    pack.setStatus("delivered");
    pack.setUserId(1);
    pack.setTrackFlag(false);
    
    packageWebService.addPackage(pack);
    assert(isPackInDB(pack));
  }
  
  @Test
  public void removePackage() {
    List<org.tempuri.Package> allPacks = packageWebService.getAllPackages().getPackage();
    Package pack = allPacks.get(1);
    packageWebService.removePackage(pack.getPackageId());
    assert(!isPackInDB(pack));
  }
  
  @Test
  public void registerPackageForTacking() {
//    List<org.tempuri.Package> allPacks = packageWebService.getAllPackages().getPackage();
//    Package pack = allPacks.get(1);
//    packageWebService.registerPackageForTracking(pack.getPackageId());
//    
//    allPacks = packageWebService.getAllPackages().getPackage();
//
//    
//    
  }

  private boolean isUserInDB(User user) {
    List<User> allUsers = userWebService.getAllUsers();

    for (User u : allUsers) {
      if (u.getUsername().equals(user.getUsername())) {
        return true;
      }
    }

    return false;
  }
  
  private boolean isPackInDB(org.tempuri.Package pack) {
    List<org.tempuri.Package> allPacks = packageWebService.getAllPackages().getPackage();

    for (org.tempuri.Package p : allPacks) {
      if (p.getTitle().equals(pack.getTitle()))
        return true;
    }
    
    return false;
  }
  
//  private boolean isPackInDB(Package package) {
//    //List<Package> allPacks = packageWebService.getAllPackages();
//    
////    for (Package p : allPacks) {
////      if (p.getTitle().equals(package.ge))
////    }
////    
//      
//      return false;
//  }

  private Assignment51Soap getPackageWebService() {
    org.tempuri.Assignment51 service = new org.tempuri.Assignment51();
    return service.getAssignment51Soap();
  }

  private lorenzo.pat.webservice.JavaAssignment5 getWebServiceReference() {
    lorenzo.pat.webservice.JavaAssignment5_Service service = new lorenzo.pat.webservice.JavaAssignment5_Service();
    return service.getJavaAssignment5Port();
  }
}
