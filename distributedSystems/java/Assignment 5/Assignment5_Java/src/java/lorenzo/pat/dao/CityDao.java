package lorenzo.pat.dao;

import java.util.List;
import lorenzo.pat.entities.City;

public interface CityDao {
    public void create(City city);

  public void update(City city);

  public List<City> getAll();

  public City getCityById(Integer cityId);

  public void remove(Integer cityId);
}
