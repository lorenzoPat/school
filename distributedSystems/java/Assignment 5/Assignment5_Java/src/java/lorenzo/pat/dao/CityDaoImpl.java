package lorenzo.pat.dao;

import java.util.List;
import lorenzo.pat.entities.City;
import org.hibernate.Session;

public class CityDaoImpl implements CityDao {

  @Override
  public void create(City city) {
    Session session = getSession();
    session.save(city);
    commitAndClose(session);
  }

  @Override
  public void update(City city) {
    Session session = getSession();
    session.update(city);
    commitAndClose(session);
  }

  @Override
  public List<City> getAll() {
    Session session = getSession();
    List<City> cityList = session.createQuery("from City").list();
    commitAndClose(session);
    return cityList;
  }

  @Override
  public City getCityById(Integer cityId) {
    Session session = getSession();
    City city = (City) session.get(City.class, cityId);
    commitAndClose(session);
    return city;
  }

  @Override
  public void remove(Integer cityId) {
    Session session = getSession();
    City city = (City) session.load(City.class, cityId);
    session.delete(city);
    commitAndClose(session);
  }
  
  
  private Session getSession() {
    Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    session.beginTransaction();
    return session;
  }

  private void commitAndClose(Session session) {
    session.flush();
    session.getTransaction().commit();
  }
}
