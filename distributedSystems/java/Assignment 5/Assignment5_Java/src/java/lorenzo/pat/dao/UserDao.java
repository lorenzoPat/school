package lorenzo.pat.dao;

import lorenzo.pat.entities.User;
import java.util.List;

public interface UserDao {

  public void create(User user);

  public void update(User user);

  public List<User> getAll();

  public User getUserById(Integer userId);

  public void remove(Integer userId);
}
