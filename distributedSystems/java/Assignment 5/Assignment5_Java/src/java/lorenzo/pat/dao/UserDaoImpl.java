package lorenzo.pat.dao;

import java.util.List;
import lorenzo.pat.entities.User;
import org.hibernate.Session;

public class UserDaoImpl implements UserDao {

  @Override
  public void create(User user) {
    Session session = getSession();
    session.save(user);
    commitAndClose(session);
  }

  @Override
  public void update(User user) {
    Session session = getSession();
    session.update(user);
    commitAndClose(session);
  }

  @Override
  public List<User> getAll() {
    Session session = getSession();
    List<User> usersList = session.createQuery("from User").list();
    commitAndClose(session);
    return usersList;
  }

  @Override
  public User getUserById(Integer userId) {
    Session session = getSession();
    User user = (User) session.get(User.class, userId);
    commitAndClose(session);
    return user;
  }

  @Override
  public void remove(Integer userId) {
    Session session = getSession();
    User user = (User) session.load(User.class, userId);
    session.delete(user);
    commitAndClose(session);
  }

  private Session getSession() {
    Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    session.beginTransaction();
    return session;
  }

  private void commitAndClose(Session session) {
    session.flush();
    session.getTransaction().commit();
  }

}
