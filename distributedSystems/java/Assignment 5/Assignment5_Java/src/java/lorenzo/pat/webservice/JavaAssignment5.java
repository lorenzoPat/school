package lorenzo.pat.webservice;

import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import lorenzo.pat.dao.CityDao;
import lorenzo.pat.dao.CityDaoImpl;
import lorenzo.pat.dao.UserDao;
import lorenzo.pat.dao.UserDaoImpl;
import lorenzo.pat.entities.City;
import lorenzo.pat.entities.User;

@WebService(serviceName = "JavaAssignment5")
public class JavaAssignment5 {

  private UserDao userDao = new UserDaoImpl();

  
  @WebMethod(operationName = "isValidLogin")
  public boolean isValidLogin(@WebParam(name = "username") String username, @WebParam(name = "password") String password) {
    List<User> allUsers = userDao.getAll();

    for (User user : allUsers) {
      if (user.getUsername().equals(username) && user.getPassword().equals(password)) {
        return true;
      }
    }

    return false;
  }

  @WebMethod(operationName = "getLoggedUser")
  public User getLoggedUser(@WebParam(name = "username") String username) {
    List<User> allUsers = userDao.getAll();

    for (User user : allUsers) {
      if (user.getUsername().equals(username)) {
        return user;
      }
    }

    return null;
  }

  @WebMethod(operationName = "registerUser")
  public void registerUser(@WebParam(name = "newUser") User newUser) {
    userDao.create(newUser);
  }

  @WebMethod(operationName = "isAdmin")
  public boolean isAdmin(@WebParam(name = "username") String username) {
    List<User> allUsers = userDao.getAll();

    for (User user : allUsers) {
      if (user.getUsername().equals(username) && user.getType() == 0) {
        return true;
      }
    }
    
    return false;
  }
  
  @WebMethod(operationName = "isSysUser")
  public boolean isSysUser(@WebParam(name = "username") String username) {
    List<User> allUsers = userDao.getAll();

    for (User user : allUsers) {
      if (user.getUsername().equals(username) && user.getType() == 1) {
        return true;
      }
    }
    
    return false;
  }

  @WebMethod(operationName = "getAllCities")
  public List<City> getAllCities() {
    CityDao cityDao = new CityDaoImpl();
    return cityDao.getAll();
  }

  @WebMethod(operationName = "getAllUsers")
  public List<User> getAllUsers() {
    return userDao.getAll();
  }
}
