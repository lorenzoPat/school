package lorenzo.pat.tests;

import java.util.List;
import lorenzo.pat.entities.User;
import lorenzo.pat.dao.UserDaoImpl;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class UserDaoTest {

  UserDaoImpl instance = new UserDaoImpl();

  public UserDaoTest() {
  }

  @Before
  public void setUp() {
    cleanUserTable();
    populateUserTable();
  }

  @After
  public void tearDown() {
  }

  @Test
  public void create() {
    User user = getTestUser();
    instance.create(user);
    assert (isUserInDB(user));
  }
  
  
  @Test
  public void remove() {
    User user = getTestUser();
    instance.create(user);
    instance.remove(user.getUserId());
    assert (!isUserInDB(user));
  }

  @Test
  public void update() {
    User user = getTestUser();
    instance.create(user);

    user.setUsername("anotherUsername");
    instance.update(user);

    assert (isUserInDB(user));

  }
  

  private User getTestUser() {
    User testUser = new User();
    testUser.setUsername("testUsername");
    testUser.setPassword("testPassword");
    testUser.setType(1);
    testUser.setFullName("fullname");
    testUser.setAddress("addr");
    testUser.setCityId(1);
    return testUser;
  }

  private void cleanUserTable() {
    List<User> users = instance.getAll();
    for (User user : users) {
      instance.remove(user.getUserId());
    }
  }

  private void populateUserTable() {
    User user0 = new User();
    User user1 = new User();
    User user2 = new User();
    User user3 = new User();
    User user4 = new User();

    user0.setUsername("user0");
    user1.setUsername("user1");
    user2.setUsername("user2");
    user3.setUsername("user3");
    user4.setUsername("user4");

    user0.setPassword("user0");
    user1.setPassword("user1");
    user2.setPassword("user2");
    user3.setPassword("user3");
    user4.setPassword("user4");

    user0.setType(1);
    user1.setType(1);
    user2.setType(1);
    user3.setType(1);
    user4.setType(1);

    user0.setFullName("dummyName");
    user1.setFullName("dummyName");
    user2.setFullName("dummyName");
    user3.setFullName("dummyName");
    user4.setFullName("dummyName");

    user0.setAddress("dummyAddress");
    user1.setAddress("dummyAddress");
    user2.setAddress("dummyAddress");
    user3.setAddress("dummyAddress");
    user4.setAddress("dummyAddress");

    user0.setCityId(1);
    user1.setCityId(1);
    user2.setCityId(1);
    user3.setCityId(1);
    user4.setCityId(1);

    instance.create(user0);
    instance.create(user1);
    instance.create(user2);
    instance.create(user3);
    instance.create(user4);
  }

  private boolean isUserInDB(User user) {
    List<User> allUsers = instance.getAll();

    for (User usr : allUsers) {
      if (usr.getUsername().equals(user.getUsername())) {
        return true;
      }
    }

    return false;
  }

}
