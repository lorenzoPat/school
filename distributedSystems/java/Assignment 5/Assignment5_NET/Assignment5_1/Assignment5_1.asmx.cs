﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Assignment5_1 {
  /// <summary>
  /// Summary description for Assignment5_1
  /// </summary>
  [WebService(Namespace = "http://tempuri.org/")]
  [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
  [System.ComponentModel.ToolboxItem(false)]
  // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
  // [System.Web.Script.Services.ScriptService]
  public class Assignment5_1 : System.Web.Services.WebService {

    private distributedSystemsEntities1 dbAccess = new distributedSystemsEntities1();

    [WebMethod]
    public void AddPackage(package newPackage) {
      dbAccess.packages.Add(newPackage);
      dbAccess.SaveChanges();
    }

    [WebMethod]
    public void RemovePackage(int packageId) {
      package pack = dbAccess.packages.Find(packageId);
      dbAccess.packages.Remove(pack);
      dbAccess.SaveChanges();
    }

    [WebMethod]
    public List<package> GetAllPackages() {
      return dbAccess.packages.ToList();
    }

    [WebMethod]
    public package GetPackageById(int id) {
      return dbAccess.packages.Find(id);
    }

    [WebMethod]
    public void RegisterPackageForTracking(int id) {
      package existingPackage = dbAccess.packages.Find(id);
      existingPackage.track_flag = true;
      dbAccess.Entry(existingPackage).State = System.Data.EntityState.Modified;
      dbAccess.SaveChanges();
    }

  }
}
