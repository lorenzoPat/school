﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Assignment5_2 {

  [WebService(Namespace = "http://tempuri1.org/")]
  [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
  [System.ComponentModel.ToolboxItem(false)]
  // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
  // [System.Web.Script.Services.ScriptService]
  public class Assignment5_2 : System.Web.Services.WebService {

    private distributedSystemsEntities dbAccess = new distributedSystemsEntities();

    [WebMethod]
    public void AddToPackageHistory(package_history newEntry) {
      dbAccess.package_history.Add(newEntry);
      dbAccess.SaveChanges();
    }
    
    [WebMethod]
    public void UpdatePackageStatus(int packageId, String status) {
      package existingPackage = dbAccess.packages.Find(packageId);
      
      existingPackage.status = String.Copy(status);
      dbAccess.Entry(existingPackage).State = System.Data.EntityState.Modified;
      dbAccess.SaveChanges();
    }

    [WebMethod]
    public List<package_history> GetAllHistoryForPackage(int packageId) {
      List<package_history> allHistory = dbAccess.package_history.ToList();
      List<package_history> result = new List<package_history>();

      foreach (package_history hist in allHistory) {
        if (hist.package_id == packageId) {
          result.Add(hist);
        }
      }

      return result;
    }

  }
}
