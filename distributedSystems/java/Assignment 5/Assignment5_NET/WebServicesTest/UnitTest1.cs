﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using System.Collections.Generic;

namespace WebServicesTest {
  
  [TestClass]
  public class UnitTest1 {

    private Assign5_1.Assignment5_1 webService = new Assign5_1.Assignment5_1();

    [TestInitialize]
    public void Init() {
      CleanDB();
      PopulateDB();
    }
    
    [TestMethod]
    public void AddPackage() {
      Assign5_1.package pack = GetTestPackage();
      webService.AddPackage(pack);
      Assert.IsTrue(isPackageInDB(pack));
    }

    [TestMethod]
    public void RemovePackage() {
      Assign5_1.package pack = GetTestPackage();
      webService.AddPackage(pack);

      var allPackages = webService.GetAllPackages();      
      foreach (var package in allPackages) {
        if (package.title.Equals(pack.title)) {
          webService.RemovePackage(package.package_id);
        }
      }

      Assert.IsFalse(isPackageInDB(pack));
    }

    [TestMethod]
    public void UpdatePackage() {
      Assign5_1.package pack = GetTestPackage();
      webService.AddPackage(pack);

      pack.title = "newTitle";
      pack.status = "delivered";

      var allPackages = webService.GetAllPackages();
      foreach (var package in allPackages) {
        if (package.title.Equals("pack0")) {
          package.title = pack.title;
          package.status = pack.status;
          webService.UpdatePackage(package);
        }
      }

      Assert.IsTrue(isPackageInDB(pack));
    }

    private void PopulateDB() {
      Assign5_1.package package0 = new Assign5_1.package();
      Assign5_1.package package1 = new Assign5_1.package();
      Assign5_1.package package2 = new Assign5_1.package();
      Assign5_1.package package3 = new Assign5_1.package();
      Assign5_1.package package4 = new Assign5_1.package();

      package0.title = "pack0";
      package1.title = "pack1";
      package2.title = "pack2";
      package3.title = "pack3";
      package4.title = "pack4";

      package0.status = "in deposit";
      package1.status = "in deposit";
      package2.status = "in deposit";
      package3.status = "in deposit";
      package4.status = "in deposit";

      package0.user_id = 3;
      package1.user_id = 3;
      package2.user_id = 3;
      package3.user_id = 3;
      package4.user_id = 3;

      package0.track_flag = true;
      package1.track_flag = true;
      package2.track_flag = true;
      package3.track_flag = true;
      package4.track_flag = true;

      webService.AddPackage(package0);
      webService.AddPackage(package1);
      webService.AddPackage(package2);
      webService.AddPackage(package3);
      webService.AddPackage(package4);
    }

    private void CleanDB() {
      Assign5_1.package[] allPackages = webService.GetAllPackages();

      foreach (Assign5_1.package pack in allPackages) {
        webService.RemovePackage(pack.package_id);
      }
    }

    private Assign5_1.package GetTestPackage() {
      Assign5_1.package testPack = new Assign5_1.package();

      testPack.title = "testPack";
      testPack.user_id = 1;
      testPack.status = "in deposit";
      testPack.track_flag = false;

      return testPack;
    }

    private bool isPackageInDB(Assign5_1.package pack) {
      var allPackages = webService.GetAllPackages();

      foreach (var package in allPackages) {
        if (package.title.Equals(pack.title)) return true;
      }

      return false;
    }

 

  }

}
