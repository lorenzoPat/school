-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema distributedSystems
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `distributedSystems` ;

-- -----------------------------------------------------
-- Schema distributedSystems
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `distributedSystems` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `distributedSystems` ;

-- -----------------------------------------------------
-- Table `distributedSystems`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `distributedSystems`.`user` ;

CREATE TABLE IF NOT EXISTS `distributedSystems`.`user` (
  `user_id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `type` INT NULL,
  `full_name` VARCHAR(45) NOT NULL,
  `address` VARCHAR(45) NOT NULL,
  `city_id` INT NOT NULL,
  PRIMARY KEY (`user_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `distributedSystems`.`package`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `distributedSystems`.`package` ;

CREATE TABLE IF NOT EXISTS `distributedSystems`.`package` (
  `package_id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  `status` VARCHAR(45) NOT NULL,
  `user_id` INT NOT NULL,
  `track_flag` TINYINT(1) NOT NULL,
  PRIMARY KEY (`package_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `distributedSystems`.`city`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `distributedSystems`.`city` ;

CREATE TABLE IF NOT EXISTS `distributedSystems`.`city` (
  `city_id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`city_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `distributedSystems`.`package_history`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `distributedSystems`.`package_history` ;

CREATE TABLE IF NOT EXISTS `distributedSystems`.`package_history` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `hour` TIMESTAMP NOT NULL,
  `package_id` INT NOT NULL,
  `city_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_package_history_package_idx` (`package_id` ASC),
  INDEX `fk_package_history_city1_idx` (`city_id` ASC))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `distributedSystems`.`user`
-- -----------------------------------------------------
START TRANSACTION;
USE `distributedSystems`;
INSERT INTO `distributedSystems`.`user` (`user_id`, `username`, `password`, `type`, `full_name`, `address`, `city_id`) VALUES (NULL, 'admin', 'admin', 0, 'admin', 'admin', 1);
INSERT INTO `distributedSystems`.`user` (`user_id`, `username`, `password`, `type`, `full_name`, `address`, `city_id`) VALUES (NULL, 'systemUser', 'systemUser', 1, 'systemUser', 'abc', 1);
INSERT INTO `distributedSystems`.`user` (`user_id`, `username`, `password`, `type`, `full_name`, `address`, `city_id`) VALUES (NULL, 'user', 'user', 2, 'user user', 'abc', 4);
INSERT INTO `distributedSystems`.`user` (`user_id`, `username`, `password`, `type`, `full_name`, `address`, `city_id`) VALUES (NULL, 'user1', 'user1', 2, 'user1 user1', 'def', 2);
INSERT INTO `distributedSystems`.`user` (`user_id`, `username`, `password`, `type`, `full_name`, `address`, `city_id`) VALUES (NULL, 'user2', 'user2', 2, 'user2 user2', 'ghi', 5);



COMMIT;


-- -----------------------------------------------------
-- Data for table `distributedSystems`.`package`

-- -----------------------------------------------------
START TRANSACTION;
USE `distributedSystems`;
INSERT INTO `distributedSystems`.`package` (`package_id`, `title`, `status`, `user_id`, `track_flag`) VALUES (NULL, 'pack1', 'in deposit', 3, false);
INSERT INTO `distributedSystems`.`package` (`package_id`, `title`, `status`, `user_id`, `track_flag`) VALUES (NULL, 'pack2', 'in deposit', 4, false);
INSERT INTO `distributedSystems`.`package` (`package_id`, `title`, `status`, `user_id`, `track_flag`) VALUES (NULL, 'pack3', 'on route', 3, false);
INSERT INTO `distributedSystems`.`package` (`package_id`, `title`, `status`, `user_id`, `track_flag`) VALUES (NULL, 'pack4', 'on route', 3, false);
INSERT INTO `distributedSystems`.`package` (`package_id`, `title`, `status`, `user_id`, `track_flag`) VALUES (NULL, 'pack5', 'on route', 3, false);
INSERT INTO `distributedSystems`.`package` (`package_id`, `title`, `status`, `user_id`, `track_flag`) VALUES (NULL, 'pack6', 'on route', 4, false);
INSERT INTO `distributedSystems`.`package` (`package_id`, `title`, `status`, `user_id`, `track_flag`) VALUES (NULL, 'pack7', 'on route', 4, false);
INSERT INTO `distributedSystems`.`package` (`package_id`, `title`, `status`, `user_id`, `track_flag`) VALUES (NULL, 'pack8', 'on route', 5, false);
INSERT INTO `distributedSystems`.`package` (`package_id`, `title`, `status`, `user_id`, `track_flag`) VALUES (NULL, 'pack9', 'delivered', 3, true);
INSERT INTO `distributedSystems`.`package` (`package_id`, `title`, `status`, `user_id`, `track_flag`) VALUES (NULL, 'pack1', 'delivered', 5, true);

COMMIT;


-- -----------------------------------------------------
-- Data for table `distributedSystems`.`city`
-- -----------------------------------------------------
START TRANSACTION;
USE `distributedSystems`;
INSERT INTO `distributedSystems`.`city` (`city_id`, `name`) VALUES (NULL, 'Arad');
INSERT INTO `distributedSystems`.`city` (`city_id`, `name`) VALUES (NULL, 'Timisoara');
INSERT INTO `distributedSystems`.`city` (`city_id`, `name`) VALUES (NULL, 'Cluj');
INSERT INTO `distributedSystems`.`city` (`city_id`, `name`) VALUES (NULL, 'Brasov');
INSERT INTO `distributedSystems`.`city` (`city_id`, `name`) VALUES (NULL, 'Iasi');
INSERT INTO `distributedSystems`.`city` (`city_id`, `name`) VALUES (NULL, 'Suceava');

COMMIT;


-- -----------------------------------------------------
-- Data for table `distributedSystems`.`package_history`
-- -----------------------------------------------------
START TRANSACTION;
USE `distributedSystems`;
INSERT INTO `distributedSystems`.`package_history` (`id`, `hour`, `package_id`, `city_id`) VALUES (NULL, '2012-06-18 10:34:09', 3, 1);
INSERT INTO `distributedSystems`.`package_history` (`id`, `hour`, `package_id`, `city_id`) VALUES (NULL, '2012-06-18 11:34:09', 3, 2);
INSERT INTO `distributedSystems`.`package_history` (`id`, `hour`, `package_id`, `city_id`) VALUES (NULL, '2012-06-18 11:40:09', 4, 2);
INSERT INTO `distributedSystems`.`package_history` (`id`, `hour`, `package_id`, `city_id`) VALUES (NULL, '2012-06-18 10:12:09', 5, 1);
INSERT INTO `distributedSystems`.`package_history` (`id`, `hour`, `package_id`, `city_id`) VALUES (NULL, '2012-06-18 10:50:09', 6, 1);
INSERT INTO `distributedSystems`.`package_history` (`id`, `hour`, `package_id`, `city_id`) VALUES (NULL, '2012-06-18 12:34:09', 6, 4);
INSERT INTO `distributedSystems`.`package_history` (`id`, `hour`, `package_id`, `city_id`) VALUES (NULL, '2012-06-18 12:34:09', 7, 4);
INSERT INTO `distributedSystems`.`package_history` (`id`, `hour`, `package_id`, `city_id`) VALUES (NULL, '2012-06-18 12:34:09', 8, 4);
INSERT INTO `distributedSystems`.`package_history` (`id`, `hour`, `package_id`, `city_id`) VALUES (NULL, '2012-06-18 10:34:09', 9, 1);
INSERT INTO `distributedSystems`.`package_history` (`id`, `hour`, `package_id`, `city_id`) VALUES (NULL, '2012-06-18 11:35:09', 9, 2);
INSERT INTO `distributedSystems`.`package_history` (`id`, `hour`, `package_id`, `city_id`) VALUES (NULL, '2012-06-18 12:34:09', 9, 3);
INSERT INTO `distributedSystems`.`package_history` (`id`, `hour`, `package_id`, `city_id`) VALUES (NULL, '2012-06-18 15:34:09', 9, 4);
INSERT INTO `distributedSystems`.`package_history` (`id`, `hour`, `package_id`, `city_id`) VALUES (NULL, '2012-06-18 10:34:09', 10, 1);
INSERT INTO `distributedSystems`.`package_history` (`id`, `hour`, `package_id`, `city_id`) VALUES (NULL, '2012-06-18 17:34:09', 10, 3);
INSERT INTO `distributedSystems`.`package_history` (`id`, `hour`, `package_id`, `city_id`) VALUES (NULL, '2012-06-18 19:34:09', 10, 5);



COMMIT;

