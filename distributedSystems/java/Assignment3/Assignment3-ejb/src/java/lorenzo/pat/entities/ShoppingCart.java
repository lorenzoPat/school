package lorenzo.pat.entities;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "shopping_cart")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "ShoppingCart.findAll", query = "SELECT s FROM ShoppingCart s"),
  @NamedQuery(name = "ShoppingCart.findByUserId", query = "SELECT s FROM ShoppingCart s WHERE s.shoppingCartPK.userId = :userId"),
  @NamedQuery(name = "ShoppingCart.findByBookId", query = "SELECT s FROM ShoppingCart s WHERE s.shoppingCartPK.bookId = :bookId"),
  @NamedQuery(name = "ShoppingCart.findByItems", query = "SELECT s FROM ShoppingCart s WHERE s.items = :items")})
public class ShoppingCart implements Serializable {

  private static final long serialVersionUID = 1L;
  @EmbeddedId
  protected ShoppingCartPK shoppingCartPK;
  @Column(name = "items")
  private Integer items;

  public ShoppingCart() {
  }

  public ShoppingCart(ShoppingCartPK shoppingCartPK) {
    this.shoppingCartPK = shoppingCartPK;
  }

  public ShoppingCart(int userId, int bookId) {
    this.shoppingCartPK = new ShoppingCartPK(userId, bookId);
  }

  public ShoppingCartPK getShoppingCartPK() {
    return shoppingCartPK;
  }

  public void setShoppingCartPK(ShoppingCartPK shoppingCartPK) {
    this.shoppingCartPK = shoppingCartPK;
  }

  public Integer getItems() {
    return items;
  }

  public void setItems(Integer items) {
    this.items = items;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final ShoppingCart other = (ShoppingCart) obj;
    return Objects.equals(this.shoppingCartPK, other.shoppingCartPK);
  }

  
}
