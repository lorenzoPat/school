package lorenzo.pat.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
public class ShoppingCartPK implements Serializable {

  @Basic(optional = false)
  @NotNull
  @Column(name = "userId")
  private int userId;

  @Basic(optional = false)
  @NotNull
  @Column(name = "bookId")
  private int bookId;

  public ShoppingCartPK() {
  }

  public ShoppingCartPK(int userId, int bookId) {
    this.userId = userId;
    this.bookId = bookId;
  }

  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  public int getBookId() {
    return bookId;
  }

  public void setBookId(int bookId) {
    this.bookId = bookId;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final ShoppingCartPK other = (ShoppingCartPK) obj;
    
    return (this.userId == other.userId) && (this.bookId == other.bookId);
  }
  
  

}
