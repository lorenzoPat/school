package lorenzo.pat.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "user")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "User.findAll", query = "SELECT u FROM User u"),
  @NamedQuery(name = "User.findById", query = "SELECT u FROM User u WHERE u.id = :id"),
  @NamedQuery(name = "User.findByUsername", query = "SELECT u FROM User u WHERE u.username = :username"),
  @NamedQuery(name = "User.findByPassword", query = "SELECT u FROM User u WHERE u.password = :password"),
  @NamedQuery(name = "User.findByType", query = "SELECT u FROM User u WHERE u.type = :type"),
  @NamedQuery(name = "User.findByMoneyAmount", query = "SELECT u FROM User u WHERE u.moneyAmount = :moneyAmount"),
  @NamedQuery(name = "User.findByMail", query = "SELECT u FROM User u WHERE u.mail = :mail")})
public class User implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "id")
  private Integer id;

  @Basic(optional = false)
  @NotNull
  @Size(min = 1, max = 45)
  @Column(name = "username")
  private String username;

  @Basic(optional = false)
  @NotNull
  @Size(min = 1, max = 45)
  @Column(name = "password")
  private String password;

  @Basic(optional = false)
  @NotNull
  @Column(name = "type")
  private int type;

  @Basic(optional = false)
  @NotNull
  @Column(name = "money_amount")
  private int moneyAmount;

  @Basic(optional = false)
  @NotNull
  @Size(min = 1, max = 45)
  @Column(name = "mail")
  private String mail;

  public User() {
    type = 0;
  }

  public User(Integer id) {
    this.id = id;
  }

  public User(Integer id, String username, String password, int type, int moneyAmount, String mail) {
    this.id = id;
    this.username = username;
    this.password = password;
    this.type = type;
    this.moneyAmount = moneyAmount;
    this.mail = mail;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public int getType() {
    return type;
  }

  public void setType(int type) {
    this.type = type;
  }

  public int getMoneyAmount() {
    return moneyAmount;
  }

  public void setMoneyAmount(int moneyAmount) {
    this.moneyAmount = moneyAmount;
  }

  public String getMail() {
    return mail;
  }

  public void setMail(String mail) {
    this.mail = mail;
  }

  @Override
  public boolean equals(Object object) {
    if (!(object instanceof User)) {
      return false;
    }

    User other = (User) object;
    return this.username.equals(other.getUsername()) && this.password.equals(other.getPassword());
  }

  @Override
  public String toString() {
    return "User[ id=" + id + ", username= " + username + ", password= " + password + " ]";
  }

}
