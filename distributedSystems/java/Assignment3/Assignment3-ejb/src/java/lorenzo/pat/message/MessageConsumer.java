package lorenzo.pat.message;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.NamingException;
import lorenzo.pat.entities.Book;
import lorenzo.pat.entities.User;
import lorenzo.pat.persistence.UserFacadeLocal;

@MessageDriven(activationConfig = {
  @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jms/assignment4Queue"),
  @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class MessageConsumer implements MessageListener {
  
  @Resource(name = "mail/testMail")
  private Session mailSession;
  
  @EJB
  private UserFacadeLocal userFacade;
  
  public MessageConsumer() {
  }
  
  @Override
  public void onMessage(Message message) {
    if (message instanceof ObjectMessage) {
      ObjectMessage msg = (ObjectMessage)message;
      try {
        Book book = (Book)msg.getObject();
        System.out.println("Message consumer: received book " + book);
        boolean newBook = msg.getBooleanProperty("newBook");
        if (newBook) {
          System.out.println("Sending newsletter...");
          sendNewsletter(book);
        }
      } catch (JMSException ex) {
        Logger.getLogger(MessageConsumer.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }
  
  private void sendNewsletter(Book book) {
    List<User> users = userFacade.findAll();
    String subject = "New book available";
    
    for (User user : users) {
      if (user.getType() == 0) { // send only to regular ussers
        String body = composeMailBody(book, user);
        try {
          sendMail(user.getMail(), subject, body);
        } catch (NamingException | MessagingException ex) {
          System.out.println("Exception caught while sending the mail...");
          //Logger.getLogger(MessageConsumer.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    }
  }
  
  private String composeMailBody(Book book, User user) {
    String body = new String();
    body = body + "Hello " + user.getUsername() + "! ";
    body = body + "Book " + book.getTitle() + ", having the author " + book.getAuthor() +
            " is now available at the price " + book.getPrice() + ".";
    return body;
  }

  private void sendMail(String email, String subject, String body) throws NamingException, MessagingException {
    MimeMessage message = new MimeMessage(mailSession);
    message.setSubject(subject);
    message.setRecipients(javax.mail.Message.RecipientType.TO, InternetAddress.parse(email, false));
    message.setText(body);
    Transport.send(message);
  }
  
}
