package lorenzo.pat.message;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import lorenzo.pat.entities.Book;

@Stateless
public class MessageProducer implements MessageProducerLocal {

  @Resource(mappedName = "jms/assignment4Factory")
  private ConnectionFactory connectionFactory;

  @Resource(mappedName = "jms/assignment4Queue")
  private Queue queue;

  @Override
  public void sendNewBookMessage(Book book) {
    System.out.println("Message producer: sending book " + book);
    try (Connection connection = connectionFactory.createConnection()) {
      Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
      javax.jms.MessageProducer messageProducer = session.createProducer(queue);

      ObjectMessage objMsg = createObjectMessage(session, book);

      messageProducer.send(objMsg);
      messageProducer.close();
    } catch (JMSException ex) {
      Logger.getLogger(MessageProducer.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  private ObjectMessage createObjectMessage(Session session, Book book) throws JMSException {
    ObjectMessage objMsg = session.createObjectMessage();
    objMsg.setObject(book);
    objMsg.setBooleanProperty("newBook", true);

    return objMsg;
  }

}
