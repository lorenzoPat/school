package lorenzo.pat.message;

import javax.ejb.Local;
import lorenzo.pat.entities.Book;

@Local
public interface MessageProducerLocal {
  public void sendNewBookMessage(Book book);
}
