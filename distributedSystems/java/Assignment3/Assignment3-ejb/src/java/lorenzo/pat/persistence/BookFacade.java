package lorenzo.pat.persistence;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import lorenzo.pat.entities.Book;

@Stateless
public class BookFacade extends AbstractFacade<Book> implements BookFacadeLocal {

  @PersistenceContext(unitName = "Assignment3-ejbPU")
  private EntityManager em;

  @Override
  protected EntityManager getEntityManager() {
    return em;
  }

  public BookFacade() {
    super(Book.class);
  }

}
