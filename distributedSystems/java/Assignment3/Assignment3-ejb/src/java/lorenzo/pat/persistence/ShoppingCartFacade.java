package lorenzo.pat.persistence;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import lorenzo.pat.entities.ShoppingCart;

@Stateless
public class ShoppingCartFacade extends AbstractFacade<ShoppingCart> implements ShoppingCartFacadeLocal {

  @PersistenceContext(unitName = "Assignment3-ejbPU")
  private EntityManager em;

  @Override
  protected EntityManager getEntityManager() {
    return em;
  }

  public ShoppingCartFacade() {
    super(ShoppingCart.class);
  }

}
