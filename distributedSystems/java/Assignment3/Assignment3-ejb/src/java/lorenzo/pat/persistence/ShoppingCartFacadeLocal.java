package lorenzo.pat.persistence;

import java.util.List;
import javax.ejb.Local;
import lorenzo.pat.entities.ShoppingCart;

@Local
public interface ShoppingCartFacadeLocal {

  void create(ShoppingCart shoppingCart);

  void edit(ShoppingCart shoppingCart);

  void remove(ShoppingCart shoppingCart);

  ShoppingCart find(Object id);

  List<ShoppingCart> findAll();

  List<ShoppingCart> findRange(int[] range);

  int count();

}
