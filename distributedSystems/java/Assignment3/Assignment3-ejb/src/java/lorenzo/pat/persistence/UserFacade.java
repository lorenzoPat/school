package lorenzo.pat.persistence;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import lorenzo.pat.entities.User;

@Stateless
public class UserFacade extends AbstractFacade<User> implements UserFacadeLocal {

  @PersistenceContext(unitName = "Assignment3-ejbPU")
  private EntityManager em;

  @Override
  protected EntityManager getEntityManager() {
    return em;
  }

  public UserFacade() {
    super(User.class);
  }

}
