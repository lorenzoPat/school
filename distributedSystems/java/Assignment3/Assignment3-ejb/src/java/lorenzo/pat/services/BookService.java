package lorenzo.pat.services;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import lorenzo.pat.entities.Book;
import lorenzo.pat.message.MessageProducerLocal;
import lorenzo.pat.persistence.BookFacadeLocal;

@Stateless
public class BookService implements BookServiceLocal {
  @EJB
  private MessageProducerLocal messageProducer;
  @EJB
  private BookFacadeLocal bookFacade;
  
  @Override
  public void addBook(Book book) {
    messageProducer.sendNewBookMessage(book);
    bookFacade.create(book);
  }

  @Override
  public void updateBook(Book book) {
    bookFacade.edit(book);
  }

  @Override
  public void removeBook(Book book) {
    bookFacade.remove(book);
  }

  @Override
  public Book findBook(Object id) {
    return bookFacade.find(id);
  }

  @Override
  public List<Book> getAllBooks() {
    return bookFacade.findAll();
  }

}
