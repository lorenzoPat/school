package lorenzo.pat.services;

import java.util.List;
import javax.ejb.Local;
import lorenzo.pat.entities.Book;

@Local
public interface BookServiceLocal {
  
  public void addBook(Book book);

  public void updateBook(Book book);

  public void removeBook(Book book);

  public Book findBook(Object id);

  public List<Book> getAllBooks();
    
}
