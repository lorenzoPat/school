package lorenzo.pat.services;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateful;
import lorenzo.pat.entities.Book;
import lorenzo.pat.entities.ShoppingCart;
import lorenzo.pat.entities.ShoppingCartPK;
import lorenzo.pat.entities.User;
import lorenzo.pat.persistence.BookFacadeLocal;
import lorenzo.pat.persistence.ShoppingCartFacadeLocal;
import lorenzo.pat.persistence.UserFacadeLocal;

@Stateful
public class UserService implements UserServiceLocal {
  @EJB
  private BookFacadeLocal bookFacade;

  @EJB
  private ShoppingCartFacadeLocal shoppingCartFacade;
  
  @EJB
  private UserFacadeLocal userFacade;
  
  @Override
  public boolean isValidLogin(String username, String password) {
    List<User> users = userFacade.findAll();

    for (User u : users) {
      if ( (username.equals(u.getUsername())) && (password.equals(u.getPassword())) ) {
        return true;
      }
    }

    return false;
  }

  @Override
  public boolean isAdmin(User user) {
    List<User> users = userFacade.findAll();

    for (User u : users) {
      if (u.equals(user) && u.getType() == 1) {
        return true;
      }
    }
    return false;
  }

  @Override
  public User getLoggedUser(String username, String password) {
    List<User> users = userFacade.findAll();
    
    for (User u : users) {
      if ( ((username.equals(u.getUsername())) && (password.equals(u.getPassword())))) {
        return u;
      }
    }
    return null;
  }
  
  @Override
  public List<ShoppingCart> getShoppingCart(User user) {
    List<ShoppingCart> cart = shoppingCartFacade.findAll();
    List<ShoppingCart> result = new ArrayList<>();

    for (ShoppingCart sc : cart) {
      if (user.getId() == sc.getShoppingCartPK().getUserId()) {
        result.add(sc);
      }
    }
    
    return result;
  }

  @Override
  public int getTotalPaySum(User user) {
    int totalPrice = 0;
    List<ShoppingCart> shoppingCart = getShoppingCart(user);

    for (ShoppingCart item : shoppingCart) {
      Book book = bookFacade.find(item.getShoppingCartPK().getBookId());
      totalPrice = totalPrice + item.getItems() * book.getPrice();
    }

    return totalPrice;
  }

  @Override
  public void removeBookFromCart(Book book, User user) {
    List<ShoppingCart> shoppingCart = getShoppingCart(user);
    
    for (ShoppingCart item : shoppingCart) {
      if ( item.getShoppingCartPK().getBookId() == book.getId() ) {
        item.setItems(item.getItems() - 1);
        shoppingCartFacade.edit(item);
        book.setStock(book.getStock() + 1);
        bookFacade.edit(book);
      }
    }
  }
  
  @Override
  public void addBookToCart(Book book, User user) {
    ShoppingCartPK shoppingCartPK = createShoppingCartItem(book, user);
    //check if there is already an entry for this book
    ShoppingCart sc = shoppingCartFacade.find(shoppingCartPK);

    if (sc == null) { //first time this books is added to cart
      sc = new ShoppingCart(shoppingCartPK);
      sc.setItems(1);
      shoppingCartFacade.create(sc);
    } else { //books already added to cart; increment the nr of items
      sc.setItems(sc.getItems() + 1);
      shoppingCartFacade.edit(sc);
    }
    
    book.setStock(book.getStock() - 1);
    bookFacade.edit(book);
  }

  private ShoppingCartPK createShoppingCartItem(Book book, User user) {
    ShoppingCartPK shoppingCartPK = new ShoppingCartPK();
    shoppingCartPK.setBookId(book.getId());
    shoppingCartPK.setUserId(user.getId());
    return shoppingCartPK;
  }

  @Override
  public void buyBooks(User user) {
    List<ShoppingCart> shoppingCart = getShoppingCart(user);
    user.setMoneyAmount(user.getMoneyAmount() - getTotalPaySum(user));
    userFacade.edit(user);
    
    for (ShoppingCart sc : shoppingCart) {
      shoppingCartFacade.remove(sc);
    }
  }

  @Override
  public void registerUser(User user) {
    userFacade.create(user);
  }

}
