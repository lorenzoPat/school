package lorenzo.pat.services;

import java.util.List;
import javax.ejb.Local;
import lorenzo.pat.entities.Book;
import lorenzo.pat.entities.ShoppingCart;
import lorenzo.pat.entities.User;

@Local
public interface UserServiceLocal {

  public boolean isValidLogin(String username, String password);

  public boolean isAdmin(User user);
  
  public User getLoggedUser(String username, String password);
  
  public void registerUser(User user);
  
  public List<ShoppingCart> getShoppingCart(User user);
  
  public int getTotalPaySum(User user);
  
  public void addBookToCart(Book book, User user);
  
  public void removeBookFromCart(Book book, User user);
  
  public void buyBooks(User user);

}
