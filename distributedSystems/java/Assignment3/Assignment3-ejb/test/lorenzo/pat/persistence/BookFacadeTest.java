/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lorenzo.pat.persistence;

import java.util.List;
import javax.ejb.EJBException;
import javax.ejb.embeddable.EJBContainer;
import javax.naming.NamingException;
import lorenzo.pat.entities.Book;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class BookFacadeTest {
  BookFacadeLocal bookFacade;  
  EJBContainer container;

  @Before
  public void setUp() throws NamingException {
    container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
    bookFacade = (BookFacadeLocal) container.getContext().lookup("java:global/classes/BookFacade");
    cleanBookTable();
    populateBookTable();
  }
  
  @After
  public void tearDown() {
    container.close();
  }
  
  @Test
  public void createValidBook() throws Exception {
    Book book = getValidTestBook();
    bookFacade.create(book);
    
    List<Book> books = bookFacade.findAll();
    assert(books.contains(book));
  }

  @Test(expected = EJBException.class)
  public void createInvalidBook() throws Exception {
    Book book = getInvalidTestBook();
    bookFacade.create(book);
  }

  @Test
  public void removeValidBook() throws Exception {
    Book book = getValidTestBook();
    bookFacade.create(book);
    bookFacade.remove(book);
    
    List<Book> books = bookFacade.findAll();
    assert(!books.contains(book));
  }

  @Test(expected = EJBException.class)
  public void removeInvalidBook() throws Exception {
    Book book = getInvalidTestBook();
    bookFacade.remove(book);
  }
  
  @Test
  public void editValidBook() throws Exception {
    Book book = getValidTestBook();
    bookFacade.create(book);
    book.setTitle("EditTitle");
    bookFacade.edit(book);
    List<Book> books = bookFacade.findAll();
    assert(books.contains(book));
  }
  

  private void cleanBookTable() {
    List<Book> books = bookFacade.findAll();
    for (Book book : books) 
      bookFacade.remove(book);
  }
  
  private void populateBookTable() {
    Book book0 = new Book();
    Book book1 = new Book();
    Book book2 = new Book();
    Book book3 = new Book();
    Book book4 = new Book();

    book0.setTitle("Title0");
    book1.setTitle("Title1");
    book2.setTitle("Title2");
    book3.setTitle("Title3");
    book4.setTitle("Title4");
    
    book0.setAuthor("Author0");
    book1.setAuthor("Author1");
    book2.setAuthor("Author2");
    book3.setAuthor("Author3");
    book4.setAuthor("Author4");

    bookFacade.create(book0);
    bookFacade.create(book1);
    bookFacade.create(book2);
    bookFacade.create(book3);
    bookFacade.create(book4);
  }

  private Book getValidTestBook() {
    Book book = new Book();
    book.setTitle("TestTitle");
    book.setAuthor("TestAuthor");
    return book;
  }

  private Book getInvalidTestBook() {
    Book book = new Book();
    return book;
  }
  
}
