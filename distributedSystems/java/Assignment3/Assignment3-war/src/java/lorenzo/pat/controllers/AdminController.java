package lorenzo.pat.controllers;

import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import lorenzo.pat.entities.Book;
import lorenzo.pat.services.BookServiceLocal;


@ManagedBean(name = "adminController")
@RequestScoped
public class AdminController {
  
  @EJB
  private BookServiceLocal bookService;
  
  Book book;

  public AdminController() {
    book = new Book();
  }

  public String redirectToAddBook() {
    return "addBook";
  }

  public List<Book> getAllBooks() {
    return bookService.getAllBooks();
  }
  
  public String addBook() {
    bookService.addBook(book);
    return "admin";
  }
  
  public String deleteBook(int bookId) {
    book = bookService.findBook(bookId);
    bookService.removeBook(book);
    return "admin";
  }
  
  public String prepareUpdate(Book updateBook) {
    book = updateBook;
    return "update";
  }
  
  public String updateBook() {
    bookService.updateBook(book);
    return "admin";
  }
  
  public Book getBook() {
    return book;
  }

  public void setBook(Book book) {
    this.book = book;
  }
  
}
