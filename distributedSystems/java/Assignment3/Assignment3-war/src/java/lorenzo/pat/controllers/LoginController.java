package lorenzo.pat.controllers;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import lorenzo.pat.entities.User;
import lorenzo.pat.services.UserServiceLocal;

@ManagedBean(name = "loginController")
@SessionScoped
public class LoginController {

  @EJB
  private UserServiceLocal loginService;

  private User user;

  public LoginController() {
    user = new User();
  }

  public String checkLogin() {
    if (!loginService.isValidLogin(user.getUsername(), user.getPassword())) 
      return "register";
    
    user = loginService.getLoggedUser(user.getUsername(), user.getPassword());

    HttpSession session = getSession();
    session.setAttribute("loginUser", user);

    return (loginService.isAdmin(user)) ? "admin" : "user";
  }

  private HttpSession getSession() {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
    return session;
  }

  public String redirectToRegister() {
    return "register";
  }

  public String redirectToIndex() {
    return "index";
  }

  public String redirectToAdmin() {
    return "admin";
  }

  public String registerUser() {
    loginService.registerUser(user);
    return "index";
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

}
