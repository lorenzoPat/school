package lorenzo.pat.controllers;

import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import lorenzo.pat.entities.Book;
import lorenzo.pat.entities.ShoppingCart;
import lorenzo.pat.entities.User;
import lorenzo.pat.services.BookServiceLocal;
import lorenzo.pat.services.UserServiceLocal;

@ManagedBean
@RequestScoped
public class UserController {
  @EJB
  private UserServiceLocal userService;
  @EJB
  private BookServiceLocal bookService;

  public UserController() {
  }

  public void addToCart(Book book) {
    userService.addBookToCart(book, getLoggedUser());
  }

  public boolean isOnStock(Book book) {
    return book.getStock() > 0;
  }

  public Book getBook(ShoppingCart sc) {
    List<ShoppingCart> itemsInCart = userService.getShoppingCart(getLoggedUser());
    
    for (ShoppingCart s : itemsInCart) {
      if (s.equals(sc)) {
        return bookService.findBook(s.getShoppingCartPK().getBookId());
      }
    }

    return null;
  }

  public void buyBooks() {
    userService.buyBooks(getLoggedUser());
  }

  public void removeFromCart(Book book) {
    userService.removeBookFromCart(book, getLoggedUser());
  }

  public boolean isMoneyEnough() {
    User user = getLoggedUser();
    return (user.getMoneyAmount() >= getTotalPrice());
  }
  
  public int getTotalPrice() {
    return userService.getTotalPaySum(getLoggedUser());
  }

  public List<Book> getAllBooks() {
    return bookService.getAllBooks();
  }

  public List<ShoppingCart> getShoppingCart() {
    return userService.getShoppingCart(getLoggedUser());
  }

  public String redirectToCheckout() {
    return "checkout";
  }

  public String redirectToUser() {
    return "user";
  }

  public String redirectToindex() {
    return "index";
  }
  
  private User getLoggedUser() {
    HttpSession session = getSession();
    User user = (User) session.getAttribute("loginUser");
    return user;
  }

  private HttpSession getSession() {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
    return session;
  }

}
