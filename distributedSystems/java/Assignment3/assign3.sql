-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema distributedSystems
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `distributedSystems` ;

-- -----------------------------------------------------
-- Schema distributedSystems
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `distributedSystems` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `distributedSystems` ;

-- -----------------------------------------------------
-- Table `distributedSystems`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `distributedSystems`.`user` ;

CREATE TABLE IF NOT EXISTS `distributedSystems`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `type` INT NOT NULL,
  `money_amount` INT NOT NULL,
  `mail` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `distributedSystems`.`book`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `distributedSystems`.`book` ;

CREATE TABLE IF NOT EXISTS `distributedSystems`.`book` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  `author` VARCHAR(45) NOT NULL,
  `price` INT NOT NULL,
  `stock` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `distributedSystems`.`shopping_cart`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `distributedSystems`.`shopping_cart` ;

CREATE TABLE IF NOT EXISTS `distributedSystems`.`shopping_cart` (
  `userId` INT NOT NULL,
  `bookId` INT NOT NULL,
  `items` INT NULL,
  PRIMARY KEY (`userId`, `bookId`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `distributedSystems`.`user`
-- -----------------------------------------------------
START TRANSACTION;
USE `distributedSystems`;
INSERT INTO `distributedSystems`.`user` (`id`, `username`, `password`, `type`, `money_amount`, `mail`) VALUES (NULL, 'admin', 'admin', 1, 1000, 'lorenzo.patras@gmail.com');
INSERT INTO `distributedSystems`.`user` (`id`, `username`, `password`, `type`, `money_amount`, `mail`) VALUES (NULL, 'user', 'user', 0, 2000, 'lorenzo.patras@yahoo.com');

COMMIT;


-- -----------------------------------------------------
-- Data for table `distributedSystems`.`book`
-- -----------------------------------------------------
START TRANSACTION;
USE `distributedSystems`;
INSERT INTO `distributedSystems`.`book` (`id`, `title`, `author`, `price`, `stock`) VALUES (NULL, 'title1', 'author1', 200, 50);
INSERT INTO `distributedSystems`.`book` (`id`, `title`, `author`, `price`, `stock`) VALUES (NULL, 'title2', 'author2', 150, 25);
INSERT INTO `distributedSystems`.`book` (`id`, `title`, `author`, `price`, `stock`) VALUES (NULL, 'title3', 'author1', 124, 50);
INSERT INTO `distributedSystems`.`book` (`id`, `title`, `author`, `price`, `stock`) VALUES (NULL, 'title5', 'author3', 250, 10);
INSERT INTO `distributedSystems`.`book` (`id`, `title`, `author`, `price`, `stock`) VALUES (NULL, 'title6', 'author2', 100, 2);

COMMIT;

