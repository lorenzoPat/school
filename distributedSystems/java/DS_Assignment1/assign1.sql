-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema distributedSystems
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `distributedSystems` ;

-- -----------------------------------------------------
-- Schema distributedSystems
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `distributedSystems` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `distributedSystems` ;

-- -----------------------------------------------------
-- Table `distributedSystems`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `distributedSystems`.`users` ;

CREATE TABLE IF NOT EXISTS `distributedSystems`.`users` (
  `user_id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `type` INT NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `distributedSystems`.`user_details`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `distributedSystems`.`user_details` ;

CREATE TABLE IF NOT EXISTS `distributedSystems`.`user_details` (
  `user_id` INT NOT NULL,
  `name` VARCHAR(45) NULL,
  `surname` VARCHAR(45) NULL,
  `latitude` DOUBLE NOT NULL,
  `longitude` DOUBLE NOT NULL,
  PRIMARY KEY (`user_id`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `distributedSystems`.`users`
-- -----------------------------------------------------
START TRANSACTION;
USE `distributedSystems`;
INSERT INTO `distributedSystems`.`users` (`user_id`, `username`, `password`, `type`) VALUES (NULL, 'admin', 'admin', 0);
INSERT INTO `distributedSystems`.`users` (`user_id`, `username`, `password`, `type`) VALUES (NULL, 'user', 'user', 1);

COMMIT;


-- -----------------------------------------------------
-- Data for table `distributedSystems`.`user_details`
-- -----------------------------------------------------
START TRANSACTION;
USE `distributedSystems`;
INSERT INTO `distributedSystems`.`user_details` (`user_id`, `name`, `surname`, `latitude`, `longitude`) VALUES (1, 'admin', 'admin', 4.5, 4.5);
INSERT INTO `distributedSystems`.`user_details` (`user_id`, `name`, `surname`, `latitude`, `longitude`) VALUES (2, 'user', 'user', 4.6, 4.6);

COMMIT;

