Assignment A1 - Basics of Web Technologies

Design, implement and test a distributed system, consisting of a Web Application (called Front-End) for interacting with the user, a Database used by the Front-End and another Web Application (called Service) that generates dynamic data based on HTTP Requests coming from the Front-End.

The following operating scenarios should be considered:
I. The Front-End starts by requesting the users to login. The user�s login data will be confronted with login credentials stored in the Database.
II. Upon successful login, the user will be redirected to another web page in the Front End, based on his role. There are the �administrator user� and �simple user� roles. The administrators should be able to perform CRUD (Create, Read, Update and Delete) operations on the collection of users in the database, through the Front End application. The simple users will be redirected to a welcome page which will display the user specific information held in the database for each user (include here at least the following: Name, Home Address including latitude and longitude, Birth Date). The welcome page should also display the Time zone of the user, based on the home address geographical coordinates. In order to obtain this information, the Front End application will make an HTTP Request to the Service application, including in the request the geographical location of the user. The Service application will reply with XML Document. (NOTE: The Service application may return random data instead of computing the actual Time zones)

Your tasks:
A1.1
Design, implement and test the distributed system using Java specific technologies. (JSP and JSF technologies are compulsory)