package lorenzo.pat.controller;

import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import lorenzo.pat.dao.UserDao;
import lorenzo.pat.dao.UserDaoImpl;
import lorenzo.pat.model.User;

@ManagedBean(name="adminBean")
public class AdminController {
	private User user = new User();
	private UserDao userDao = new UserDaoImpl();
	
	public String create(){
		userDao.addUser(user);
		updateUsersList();
		return"admin";
	}
	
	public String prepareUpdate(){
		FacesContext fc = FacesContext.getCurrentInstance();
	    Map<String,String> params = 
	    		fc.getExternalContext().getRequestParameterMap();
	    String data =  params.get("userID");
	    int userId = Integer.parseInt(data);
	    
	    user = userDao.getUserById(userId);
	    System.out.println("in prepare " + user);
	    return "update";
	}
	
	public String update(){
		System.out.println("Updating " + user);
		userDao.updateUser(user);
		updateUsersList();
		return"admin";
	}
	
	public String delete(){
		FacesContext fc = FacesContext.getCurrentInstance();
	    Map<String,String> params = 
	    		fc.getExternalContext().getRequestParameterMap();
	    String data =  params.get("userID"); 
		
	    int userId = Integer.parseInt(data);
		userDao.removeUser(userId);
		
		updateUsersList();
		return"admin";
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	private void updateUsersList() {
		FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		List<User> userList = userDao.getAllUsers();
		session.setAttribute("allUsers", userList);
	}
}
