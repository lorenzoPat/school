package lorenzo.pat.controller;

import java.io.IOException;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import lorenzo.pat.dao.UserDao;
import lorenzo.pat.dao.UserDaoImpl;
import lorenzo.pat.model.User;
import lorenzo.pat.model.WebRequestHandler;

@ManagedBean(name="loginBean")
@SessionScoped
public class LoginController {
	private User user = new User();
	
	public String login() throws IOException {
		if (!isCorrectLogin()) 
			return "error";
		
		HttpSession session = getSession();
        session.setAttribute("loginUser", user);
				
		if (user.isAdmin()) {
			UserDao userDao = new UserDaoImpl();
			List<User> userList = userDao.getAllUsers();
			session.setAttribute("allUsers", userList);
			return "admin";
		}
		
		try {
			WebRequestHandler helper =  new WebRequestHandler(); 
			user.setTimezone(helper.getTimezone(user.getLatitude(), user.getLongitude()));
			return "user";
		} catch (Exception e) {
			return "error";
		}
	}
	
	private boolean isCorrectLogin(){
		UserDao userDao = new UserDaoImpl();
		List<User> userList = userDao.getAllUsers();
		
		for ( User testUser : userList ) {
			if (user.equals(testUser)){
				user = testUser;
				return true;
			}
		}
	    	
	    return false;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	private HttpSession getSession() {
		FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		return session;
	}
}
