package lorenzo.pat.dao;

import java.util.List;

import lorenzo.pat.model.User;

public interface UserDao {
	public void addUser(User user);
	public void updateUser(User user);
	public List<User> getAllUsers();
	public User getUserById(int userId);
	public User getUserByUsername(String username);
	public void removeUser(int userId);
	public void removeUser(User user);
}
