package lorenzo.pat.model;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class User implements Serializable{
	private int userId;
	private String username;
	private String password;
	private int type;
	private String name;
	private String surname;
	private double latitude;
	private double longitude;
	private String timezone;
	
	public User(){
		userId      = -1;
		username 	= "";
		password 	= "";
		type 		= -1;
		name 		= "";
		surname		= "";
		latitude 	= 0.0;
		longitude 	= 0.0;
		timezone 	= "";
	}
	
	public User(int userId, String username, String password, int type) {
		super();
		this.userId = userId;
		this.username = username;
		this.password = password;
		this.type = type;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	public String getTimezone() {
		return timezone;
	}
	
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		
		return username.equals(other.getUsername());
	}
	
	public boolean isAdmin(){
		return type == 0;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", username=" + username
				+ ", password=" + password + ", type=" + type + ", name="
				+ name + ", surname=" + surname + ", latitude=" + latitude
				+ ", longitude=" + longitude + "]";
	}	
}
