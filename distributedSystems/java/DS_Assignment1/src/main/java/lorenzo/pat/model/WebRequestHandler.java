package lorenzo.pat.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class WebRequestHandler{
	public String getTimezone(double latitude, double longitude) throws Exception{
		BufferedReader reader = getWebRequest(latitude, longitude);
		Document doc = getDocumentResponse( getRequestResult(reader) );
	    String finalResult = getFinalResult(doc);
		reader.close();
		return finalResult;
	}

	private String getFinalResult(Document doc) {
		NodeList nodes = doc.getElementsByTagName("localtime");
	    Element element = (Element) nodes.item(0);
	    Node child = element.getLastChild();
	    org.w3c.dom.CharacterData cd = (org.w3c.dom.CharacterData)child;
	    String result = cd.getData();
	    return result;
	}

	private Document getDocumentResponse(String response)
			throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	    DocumentBuilder db = dbf.newDocumentBuilder();
	    InputSource is = new InputSource();
	    is.setCharacterStream(new StringReader(response));
	    Document doc = db.parse(is);
		return doc;
	}

	private String getRequestResult(BufferedReader reader) throws IOException {
		StringBuilder responseBuilder = new StringBuilder();
		String line;
		while ((line = reader.readLine()) != null) {
			responseBuilder.append(line + '\n');
		}		
		String response = new String(responseBuilder);
		return response;
	}

	private BufferedReader getWebRequest(double latitude, double longitude)
			throws MalformedURLException, IOException {
		String request = new String("http://www.earthtools.org/timezone/" + latitude + "/" + longitude); 
		URL url = new URL(request);
		URLConnection urlConnection = url.openConnection();
		BufferedReader reader =  new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
		return reader;
	}
}
