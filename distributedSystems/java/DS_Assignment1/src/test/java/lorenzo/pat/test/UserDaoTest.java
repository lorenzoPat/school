package lorenzo.pat.test;

import java.util.List;

import org.hibernate.exception.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import junit.framework.TestCase;
import lorenzo.pat.dao.UserDao;
import lorenzo.pat.dao.UserDaoImpl;
import lorenzo.pat.model.User;

public class UserDaoTest extends TestCase {
	UserDao userDao = new UserDaoImpl();
	
	@Before
	public void setUp() {
		cleanDB();
		populateDB();
	}
	
	@Test
	public void testAddUser() {
		List<User> before = userDao.getAllUsers();
		User user = getTestUser();
		userDao.addUser(user);
		List<User> after = userDao.getAllUsers();
        assertEquals(before.size(), after.size() - 1);
        
	}
	
	/*
	@Test(expected=ConstraintViolationException.class)
	public void testAddExistingUser() {
		User user = getTestUser();
		userDao.addUser(user);
		userDao.addUser(user);
	}
	*/
	
	
	@Test
	public void testReadUser(){
		User user = getTestUser();
		userDao.addUser(user);
		List<User> userList = userDao.getAllUsers();
		assertEquals(userList.get(userList.size()-1), user);
	}
	
	@Test
	public void testUpdateUser() {
		User user = getTestUser();
		userDao.addUser(user);
		user.setUsername("blaTest");
		userDao.updateUser(user);
		List<User> userList = userDao.getAllUsers();
		assertEquals(userList.get(userList.size()-1), user);
	}	
	
	@Test
	public void testDeleteUser() {
		User user = getTestUser();
		userDao.addUser(user);
		List<User> before = userDao.getAllUsers();
		userDao.removeUser(user);
		List<User> after = userDao.getAllUsers();
        assertEquals(before.size(), after.size() + 1);
	}
	
	@Test
	public void testValidLogin() {
	  User user = new User();
    user.setUsername("user0");
    user.setPassword("user0");
    assertEquals(true, isCorrectLogin(user));
	}
	
	@Test
  public void testInvalidLogin() {
    User user = new User();
    user.setUsername("userbla");
    user.setPassword("userbla");
    assertEquals(false, isCorrectLogin(user));
  }
	
	private void cleanDB(){
		List<User> usersList = userDao.getAllUsers();
		for ( User user : usersList ) 
			userDao.removeUser(user.getUserId());
	}
	
	private void populateDB() {
		User user0 = new User();
		user0.setUsername("user0");
		user0.setPassword("user0");
		user0.setType(1);
		User user1 = new User();
		user1.setUsername("user1");
		user1.setPassword("user1");
		user1.setType(1);
		User user2 = new User();
		user2.setUsername("user2");
		user2.setPassword("user2");
		user2.setType(1);
		User user3 = new User();
		user3.setUsername("user3");
		user3.setPassword("user3");
		user3.setType(1);
		userDao.addUser(user0);
		userDao.addUser(user1);
		userDao.addUser(user2);
		userDao.addUser(user3);
	}
	
	private User getTestUser(){
		User user = new User();
		user.setUsername("test1");
		user.setPassword("test1");
		user.setType(1);
		return user;
	}
	
	private boolean isCorrectLogin(User user){    
	  List<User> userList = userDao.getAllUsers();
    
    for ( User testUser : userList ) {
      if (user.equals(testUser)){
        user = testUser;
        return true;
      }
    }
        
      return false;
  }
}
