-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema distributedSystems
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `distributedSystems` ;

-- -----------------------------------------------------
-- Schema distributedSystems
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `distributedSystems` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `distributedSystems` ;

-- -----------------------------------------------------
-- Table `distributedSystems`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `distributedSystems`.`user` ;

CREATE TABLE IF NOT EXISTS `distributedSystems`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `distributedSystems`.`job`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `distributedSystems`.`job` ;

CREATE TABLE IF NOT EXISTS `distributedSystems`.`job` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NULL,
  `company` VARCHAR(45) NULL,
  `specification` VARCHAR(45) NULL,
  `contact` VARCHAR(45) NULL,
  `category` VARCHAR(45) NULL,
  `post_date` DATE NULL,
  `deadline` DATE NULL,
  `taken` BOOLEAN NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `distributedSystems`.`user`
-- -----------------------------------------------------
START TRANSACTION;
USE `distributedSystems`;
INSERT INTO `distributedSystems`.`user` (`id`, `username`, `password`) VALUES (NULL, 'admin', 'admin');
INSERT INTO `distributedSystems`.`user` (`id`, `username`, `password`) VALUES (NULL, 'user', 'user');

COMMIT;


-- -----------------------------------------------------
-- Data for table `distributedSystems`.`job`
-- -----------------------------------------------------
START TRANSACTION;
USE `distributedSystems`;
INSERT INTO `distributedSystems`.`job` (`id`, `title`, `company`, `specification`, `contact`, `category`, `post_date`, `deadline`, `taken`) VALUES (NULL, 'job1', 'company1', 'spec1', '000-000000', 'it', '2013-2-26', '2013-9-10', true);
INSERT INTO `distributedSystems`.`job` (`id`, `title`, `company`, `specification`, `contact`, `category`, `post_date`, `deadline`, `taken`) VALUES (NULL, 'job2', 'comp2', 'spec2', '111-111111', 'economy', '2012-5-30', '2013-1-1', true);
INSERT INTO `distributedSystems`.`job` (`id`, `title`, `company`, `specification`, `contact`, `category`, `post_date`, `deadline`, `taken`) VALUES (NULL, 'job3', 'company1', 'specbla', '123-546789', 'healthcare', '2013-4-20', '2013-9-20', false);
INSERT INTO `distributedSystems`.`job` (`id`, `title`, `company`, `specification`, `contact`, `category`, `post_date`, `deadline`, `taken`) VALUES (NULL, 'job4', 'comp1', 'blabla', '456-894815', 'law', '2014-1-8', '2014-12-30', false);
INSERT INTO `distributedSystems`.`job` (`id`, `title`, `company`, `specification`, `contact`, `category`, `post_date`, `deadline`, `taken`) VALUES (NULL, 'job5', 'comp2', 'ajfh', '4589522', 'economy', '2014-5-5', '2014-8-8', true);
INSERT INTO `distributedSystems`.`job` (`id`, `title`, `company`, `specification`, `contact`, `category`, `post_date`, `deadline`, `taken`) VALUES (NULL, 'job6', 'comp2X', 'ajfasdsah', '458952asd2', 'economy', '2014-5-5', '2014-8-8', false);

COMMIT;

