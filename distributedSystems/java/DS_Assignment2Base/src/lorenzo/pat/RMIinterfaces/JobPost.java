package lorenzo.pat.RMIinterfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

import lorenzo.pat.beans.Job;

public interface JobPost extends Remote {
  public void postJob(Job job) throws RemoteException;
}
