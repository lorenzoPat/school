package lorenzo.pat.RMIinterfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.List;

import lorenzo.pat.beans.Job;

public interface JobSearch extends Remote {
  public List<Job> searchJobs(Date date) throws RemoteException;
  public List<Job> searchJobs(Date beginDate, Date endDate) throws RemoteException;
  public List<Job> searchJobs(String category) throws RemoteException;
}

