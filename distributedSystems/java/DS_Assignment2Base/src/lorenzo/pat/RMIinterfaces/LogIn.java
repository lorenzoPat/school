package lorenzo.pat.RMIinterfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface LogIn extends Remote{
  public boolean isValidLogin(String username, String password) throws RemoteException;
}
