package lorenzo.pat.beans;

import java.io.Serializable;
import java.util.Date;

public class Job implements Serializable{
  private static final long serialVersionUID = 1L;
  
  private int id;
  private String title;
  private String company;
  private String specification;
  private String contact;
  private String category;
  private Date postDate;
  private Date deadline;
  private boolean taken;

  public Job() { }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getCompany() {
    return company;
  }

  public void setCompany(String company) {
    this.company = company;
  }
  
  public String getSpecification() {
    return specification;
  }

  public void setSpecification(String specification) {
    this.specification = specification;
  }

  public String getContact() {
    return contact;
  }

  public void setContact(String contact) {
    this.contact = contact;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public Date getPostDate() {
    return postDate;
  }

  public void setPostDate(Date postDate) {
    this.postDate = postDate;
  }

  public Date getDeadline() {
    return deadline;
  }

  public void setDeadline(Date deadline) {
    this.deadline = deadline;
  }
  
  public boolean isTaken() {
    return taken;
  }

  public void setTaken(boolean taken) {
    this.taken = taken;
  }
  
  public boolean equals(Object arg0) {
    if (arg0 == null)
      return false;
    
    Job arg = (Job)arg0;
    
    if ( this == arg )
      return true;
    
    if ( this.title.equals(arg.getTitle()) )
      return true;
    
    return false;
  }

  @Override
  public String toString() {
    return "Job [id=" + id + ", title=" + title + ", company=" + company
        + ", specification=" + specification + ", contact=" + contact
        + ", category=" + category + ", postDate=" + postDate + ", deadline="
        + deadline + ", taken=" + taken + "]";
  }
  
}
