package lorenzo.pat.beans;

import java.io.Serializable;

public class User implements Serializable{
  private static final long serialVersionUID = 1L;
  
  private int id;
  private String username;
  private String password;
  
  public User() {}
  
  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getUsername() {
    return username;
  }
  public void setUsername(String username) {
    this.username = username;
  }
  public String getPassword() {
    return password;
  }
  public void setPassword(String password) {
    this.password = password;
  }

  @Override
  public boolean equals(Object arg0) {
    if ( arg0 == null )
      return false;
    
    User arg = (User)arg0;
    
    if ( arg0 == this )
      return true;
    
    if ( this.username.equals(arg.getUsername()) &&
         this.password.equals(arg.getPassword())
        )
      return true;
    
    return false;
  }
    
}
