package lorenzo.pat.RMIclient;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import lorenzo.pat.view.View;

public class Assign2Client {
  public static void main(String[] args) throws RemoteException, NotBoundException {
    Model model = new Model();
    View view = new View(model);
    @SuppressWarnings("unused")
    Controller controller = new Controller(model, view);
  }
}
