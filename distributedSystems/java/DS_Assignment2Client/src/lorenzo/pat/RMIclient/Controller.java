package lorenzo.pat.RMIclient;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.List;

import lorenzo.pat.beans.Job;
import lorenzo.pat.view.View;

public class Controller {
  Model model;
  View view;
  
  public Controller(Model model, View view) {
    this.model = model;
    this.view = view;
    
    addListeners();
  }
  
  private void addListeners() {
    view.addPostJobListener(new PostListener());
    view.addSearchByCategoryListener(new SearchByCategoryListener());
    view.addSearchByDateListener(new SearchByDateListener());
    view.addSearchByIntervalListener(new SearchByIntervalListener());
    view.addLoginListener(new LoginController());
  }
  
  class LoginController implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      String[] credentials = view.getCredentials();
      try {
        if (model.isValidLogin(credentials[0], credentials[1])) {
          view.validateLogin();
        }
      } catch (RemoteException e1) {
        System.out.println("Exception in login");
      } 
    }
  }
  
  class PostListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      if ( view.checkJobDetailsValidity() ) {
        Job newJob = view.getJob();
        try {
          model.postJob(newJob);
          view.clearPostFields();
        } catch (RemoteException e1) {
          System.out.println("Exception in post job");
        }
      } else {
        view.showPostError();
      }
    }
    
  }
  
  class SearchByCategoryListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      if ( view.checkCategorySearchValidity() ) {
        String category = view.getCategory();
        try {
          List<Job> jobs = model.searchJobs(category);
          view.listJobsInCategorySearch(jobs);
        } catch (RemoteException e1) {
          System.out.println("Exception in search by catgory");
        }
      }
      
      view.showCategorySearchError();      
    }    
  }
  
  class SearchByDateListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent arg0) {
      if ( view.checkDateSearchValidity() ) {
        Date date = view.getDate();
        try {
          List<Job> jobs = model.searchJobs(date);
          view.listJobsInDateSearch(jobs);
        } catch (RemoteException e1) {
          System.out.println("Exception in search by catgory");
        }
      }
      
      view.showDateSearchError();
      
    }
    
  }
  
  class SearchByIntervalListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent arg0) {
      if ( view.checkIntervalSearchValidity() ) {
        Date[] dates = view.getInterval();
        try {
          List<Job> jobs = model.searchJobs(dates[0], dates[1]);
          view.listJobsInIntervalSearch(jobs);
        } catch (RemoteException e1) {
          System.out.println("Exception in search by catgory");
        }
      }
      
      view.showIntervalSearchError();
      
    }
    
  }
}
