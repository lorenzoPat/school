package lorenzo.pat.RMIclient;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Date;
import java.util.List;

import lorenzo.pat.RMIinterfaces.JobPost;
import lorenzo.pat.RMIinterfaces.JobSearch;
import lorenzo.pat.RMIinterfaces.LogIn;
import lorenzo.pat.beans.Job;
import lorenzo.pat.util.Constant;

public class Model {
  private Registry registry;
  private JobPost jobPost;
  private JobSearch jobSearch;
  private LogIn logIn;
  
  public Model() throws RemoteException, NotBoundException {
    registry = LocateRegistry.getRegistry("localhost", Constant.RMI_PORT);
    jobPost = (JobPost) registry.lookup(Constant.RMI_ID); 
    jobSearch = (JobSearch) registry.lookup(Constant.RMI_ID);
    logIn = (LogIn) registry.lookup(Constant.RMI_ID);
  }
  
  public void postJob(Job job) throws RemoteException {
    //System.out.println(job);
    jobPost.postJob(job);
  }
  
  public List<Job> searchJobs(String category) throws RemoteException {
    return jobSearch.searchJobs(category);
  }
  
  public List<Job> searchJobs(Date date) throws RemoteException {
    return jobSearch.searchJobs(date);
  }
  
  public List<Job> searchJobs(Date startDate, Date endDate) throws RemoteException {
    return jobSearch.searchJobs(startDate, endDate);
  }
  
  public boolean isValidLogin(String username, String password) throws RemoteException {
    return logIn.isValidLogin(username, password);
  }
}
