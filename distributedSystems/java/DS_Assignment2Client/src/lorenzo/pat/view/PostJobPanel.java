package lorenzo.pat.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import lorenzo.pat.beans.Job;
import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;

public class PostJobPanel extends JPanel{
  private static final long serialVersionUID = 1L;
  
  private JPanel fieldsPanel;
  private JButton postBtn;
  
  private JLabel title;
  private JLabel company;
  private JLabel specification;
  private JLabel contact;
  private JLabel category;
  private JLabel deadline;
  
  private JLabel titleValidation;
  private JLabel companyValidation;
  private JLabel specificationValidation;
  private JLabel contactValidation;
  private JLabel categoryValidation;
  private JLabel deadlineValidation;
  
  private JTextField titleField;
  private JTextField companyField;
  private JTextField specificationField;
  private JTextField contactField;
  private JTextField categoryField;
  private JDatePickerImpl deadlineField;
  
  public PostJobPanel() {
    fieldsPanel = new JPanel();
    postBtn = new JButton("Post jbo");
    
    title = new JLabel("Title: ");
    company = new JLabel("Company: ");;
    specification = new JLabel("Specification: ");
    contact = new JLabel("Contact: ");
    category = new JLabel("Category: ");
    deadline = new JLabel("Deadline: ");
    
    titleValidation = new JLabel("Title is mandatory");
    companyValidation = new JLabel("Company is mandatory");
    specificationValidation = new JLabel("Specification is mandatory");
    contactValidation = new JLabel("Contact is mandatory");
    categoryValidation = new JLabel("Category is mandatory");
    deadlineValidation  = new JLabel("Deadline is mandatory");
    
    titleField = new JTextField();
    companyField = new JTextField(100);
    specificationField = new JTextField(100);
    contactField = new JTextField(100);
    categoryField = new JTextField(100);
    UtilDateModel deadlineModel = new UtilDateModel();
    JDatePanelImpl deadlinePanel = new JDatePanelImpl(deadlineModel);
    deadlineField = new JDatePickerImpl(deadlinePanel);
    
    initializeFieldsPanel();
    
    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    add(fieldsPanel);
    add(postBtn);
    setBackground(new Color(150, 255, 150));
    postBtn.setAlignmentX(Component.CENTER_ALIGNMENT);
  }
  
  private void initializeFieldsPanel() {
    fieldsPanel.setBackground(new Color(150, 255, 150));
    GridLayout layout = new GridLayout(0,3);
    layout.setVgap(20);
    fieldsPanel.setLayout(layout);
    
    fieldsPanel.add(title);
    fieldsPanel.add(titleField);
    fieldsPanel.add(titleValidation);
    
    fieldsPanel.add(company);
    fieldsPanel.add(companyField);
    fieldsPanel.add(companyValidation);
    
    fieldsPanel.add(specification);
    fieldsPanel.add(specificationField);
    fieldsPanel.add(specificationValidation);
    
    fieldsPanel.add(contact);
    fieldsPanel.add(contactField);
    fieldsPanel.add(contactValidation);
    
    fieldsPanel.add(category);
    fieldsPanel.add(categoryField);
    fieldsPanel.add(categoryValidation);
    
    fieldsPanel.add(deadline);
    fieldsPanel.add(deadlineField);
    fieldsPanel.add(deadlineValidation);
    
    hideValidations();
  }
  
  private void hideValidations() {
    titleValidation.setVisible(false);
    companyValidation.setVisible(false);
    specificationValidation.setVisible(false);
    contactValidation.setVisible(false);
    categoryValidation.setVisible(false);
    deadlineValidation.setVisible(false);
  }
 
  public boolean checkValidity() {
    return !titleField.getText().equals("") &&
           !companyField.getText().equals("") &&
           !specificationField.getText().equals("") &&
           !contactField.getText().equals("") &&
           !categoryField.getText().equals("") &&
           (deadlineField.getModel().getValue() != null);
  }
  
  public void showError () {
    titleValidation.setVisible(titleField.getText().equals(""));
    companyValidation.setVisible(companyField.getText().equals(""));
    specificationValidation.setVisible(specificationField.getText().equals(""));
    contactValidation.setVisible(contactField.getText().equals(""));
    categoryValidation.setVisible(categoryField.getText().equals(""));
    deadlineValidation.setVisible(deadlineField.getModel().getValue() == null);
  }
  
  public Job getJob() {
    Job job = new Job();
    job.setTitle(titleField.getText());
    job.setCompany(companyField.getText());
    job.setSpecification(specificationField.getText());
    job.setContact(contactField.getText());
    job.setCategory(categoryField.getText());
    job.setPostDate(new Date());
    job.setDeadline((Date)deadlineField.getModel().getValue());
    job.setTaken(false);
    return job;
  }
  
  public void addListener(ActionListener listener) {
    postBtn.addActionListener(listener);
  }
  
  public void clearFields() {
    titleField.setText("");
    companyField.setText("");
    specificationField.setText("");
    contactField.setText("");
    categoryField.setText("");
    deadlineField.getModel().setValue(null);
  }
}
