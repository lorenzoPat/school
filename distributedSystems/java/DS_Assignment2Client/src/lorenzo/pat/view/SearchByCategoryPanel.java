package lorenzo.pat.view;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JTextField;


public class SearchByCategoryPanel extends SearchPanel {
  private static final long serialVersionUID = 1L;  
  private JLabel categoryLabel;
  private JTextField categoryField;

  public SearchByCategoryPanel() {
    categoryLabel = new JLabel("Category: ");
    categoryField = new JTextField();
    
    initializeSearchPanel();
  }
  
  private void initializeSearchPanel() {
    searchPanel.setBackground(new Color(150, 255, 150));
    searchPanel.setLayout(new GridLayout(0, 4));
    
    searchPanel.add(categoryLabel);
    searchPanel.add(categoryField);
    searchPanel.add(searchBtn);
    searchPanel.add(searchCheck);
    searchCheck.setVisible(false);
  }
  
  public String getCategory() {
    return categoryField.getText();
  }
  
  public boolean checkValidity() {
    return !categoryField.getText().equals("");
  }
}
