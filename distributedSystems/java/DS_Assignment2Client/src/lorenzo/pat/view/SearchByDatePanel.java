package lorenzo.pat.view;

import java.awt.Color;
import java.awt.GridLayout;
import java.util.Date;

import javax.swing.JLabel;

import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;

public class SearchByDatePanel extends SearchPanel {
  private static final long serialVersionUID = 1L;
  private JLabel dateLabel;
  private JDatePickerImpl dateField;  


  public SearchByDatePanel() {
    dateLabel = new JLabel("Date: ");
    UtilDateModel dateModel = new UtilDateModel();
    JDatePanelImpl datePanel = new JDatePanelImpl(dateModel);
    dateField = new JDatePickerImpl(datePanel);    
    
    initializeSearchPanel();
  }
  
  private void initializeSearchPanel() {
    searchPanel.setBackground(new Color(150, 255, 150));
    searchPanel.setLayout(new GridLayout(0, 4));
    
    searchPanel.add(dateLabel);
    searchPanel.add(dateField);
    searchPanel.add(searchBtn);
    searchPanel.add(searchCheck);
    searchCheck.setVisible(false);
  }
  
  public Date getDate() {
    return (Date)dateField.getModel().getValue();
  }
  
  public boolean checkValidity() {
    return dateField.getModel().getValue() != null; 
  }
}
