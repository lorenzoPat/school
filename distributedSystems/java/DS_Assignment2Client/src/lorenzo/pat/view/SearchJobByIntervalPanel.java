package lorenzo.pat.view;

import java.awt.Color;
import java.awt.GridLayout;
import java.util.Date;
import javax.swing.JLabel;
import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;

public class SearchJobByIntervalPanel extends SearchPanel {
  private static final long serialVersionUID = 1L;
    
  private JLabel intervalLabel;
  private JDatePickerImpl startField;
  private JDatePickerImpl endField;
  
  public SearchJobByIntervalPanel() {
    intervalLabel = new JLabel("Interval: ");
    
    UtilDateModel startDateModel = new UtilDateModel();
    JDatePanelImpl startDatePanel = new JDatePanelImpl(startDateModel);
    startField = new JDatePickerImpl(startDatePanel);
    UtilDateModel endDateModel = new UtilDateModel();
    JDatePanelImpl endDatePanel = new JDatePanelImpl(endDateModel);
    endField = new JDatePickerImpl(endDatePanel);
    
    initializeSearchPanel();
  }
  
  private void initializeSearchPanel() {
    searchPanel.setBackground(new Color(150, 255, 150));
    searchPanel.setLayout(new GridLayout(0, 5));
    
    searchPanel.add(intervalLabel);
    searchPanel.add(startField);
    searchPanel.add(endField);
    searchPanel.add(searchBtn);
    searchPanel.add(searchCheck);
    searchCheck.setVisible(false);
  }
    
  public Date[] getInterval() {
    Date[] result = new Date[2]; 
    result[0] = (Date)startField.getModel().getValue();
    result[1] = (Date)endField.getModel().getValue();
    return result;
  }
  
  public boolean checkValidity() {
    return startField.getModel().getValue() != null &&
           endField.getModel().getValue() != null; 
  }
}
