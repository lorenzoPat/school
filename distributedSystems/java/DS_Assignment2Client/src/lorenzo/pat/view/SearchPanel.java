package lorenzo.pat.view;

import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import lorenzo.pat.beans.Job;

public abstract class SearchPanel extends JPanel{
  protected JPanel searchPanel;
  private Object[][] data;
  private JTable table;
  protected JLabel searchCheck;
  protected JButton searchBtn;
  
  SearchPanel() {
    searchPanel = new JPanel();
    searchCheck = new JLabel("Search field cannot be empty");
    searchBtn = new JButton("Search");
    
    data = new Object[20][9];
    
    String[] columnNames = {
                "Job Id",
                "Title",
                "Company",
                "Specification",
                "Contact",
                "Category",
                "Post Date",
                "Deadline",
                "Taken"
        };
    
    table = new JTable(data,columnNames);
    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    add(searchPanel);
    add(new JScrollPane(table));
  }
  
  public void addSearchJobListener(ActionListener listener) {
    searchBtn.addActionListener(listener);
  }
  
  public boolean checkValidity() {
    return false; 
  }
  
  public void showError() {
    searchCheck.setVisible(!checkValidity());
  }
  
  public void showResult(List<Job> jobs) {
    clearTable();
    int i = 0;
    for ( Job job : jobs ) {
      data[i][0] = job.getId();
      data[i][1] = job.getTitle();
      data[i][2] = job.getCompany();
      data[i][3] = job.getSpecification();
      data[i][4] = job.getContact();
      data[i][5] = job.getCategory();
      data[i][6] = job.getPostDate();
      data[i][7] = job.getDeadline();
      data[i][8] = job.isTaken();
      i++;
    }
    table.validate();
    table.repaint();
  }
  
  private void clearTable() {
    for ( int i = 0; i < table.getRowCount(); i++ ) {
      data[i][0] = "";
      data[i][1] = "";
      data[i][2] = "";
      data[i][3] = "";
      data[i][4] = "";
      data[i][5] = "";
      data[i][6] = "";
      data[i][7] = "";
      data[i][8] = "";
    }
    table.validate();
    table.repaint();
  }
}
