package lorenzo.pat.view;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

import lorenzo.pat.RMIclient.Model;
import lorenzo.pat.beans.Job;

public class View extends JFrame{
  private static final long serialVersionUID = 1L;

  @SuppressWarnings("unused")
  private Model model;
  private JPanel daemonPanel;
  private JPanel loginPanel;
  private JTabbedPane mainPanel;
  private PostJobPanel postJobPanel;
  private SearchByCategoryPanel searchJobByCategoryPanel;
  private SearchByDatePanel searchJobByDatePanel;
  private SearchJobByIntervalPanel searchJobByIntervalPanel;
  
  private JLabel usernameLabel;
  private JLabel passwordLabel;
  private JTextField username;
  private JPasswordField password;
  private JButton loginBtn;  
  
  
  public View(Model model) {
    this.model = model;
    daemonPanel = new JPanel();
    loginPanel = new JPanel();
    mainPanel = new JTabbedPane();
    
    postJobPanel = new PostJobPanel();
    searchJobByCategoryPanel = new SearchByCategoryPanel();
    searchJobByDatePanel = new SearchByDatePanel();
    searchJobByIntervalPanel = new SearchJobByIntervalPanel();
    
    usernameLabel = new JLabel("Username: ");
    passwordLabel = new JLabel("Password: ");
    username = new JTextField();
    password = new JPasswordField();
    loginBtn = new JButton("Log In");
    
    add(daemonPanel);
    
        
    initializeLoginPanel();
    initializeTabsPanel();
    initializeDaemonPanel();
    
    this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    this.setSize(600, 400);
    this.setVisible(true);
    this.setResizable(true);
  }
  
  private void initializeDaemonPanel() {
    daemonPanel.setLayout(new CardLayout());
    daemonPanel.add(loginPanel, "0");
    daemonPanel.add(mainPanel, "1");
  }
  
  private void initializeLoginPanel() {
    loginPanel.setBackground(new Color(150, 255, 150));
    
    loginPanel.setLayout(null);
    loginPanel.add(usernameLabel);
    loginPanel.add(username);
    loginPanel.add(passwordLabel);
    loginPanel.add(password);
    loginPanel.add(loginBtn);
    Insets insets = loginPanel.getInsets();
    usernameLabel.setBounds(100 + insets.left, 100 + insets.top, 100, 20);
    username.setBounds(200 + insets.left, 100 + insets.top, 200, 20);
    passwordLabel.setBounds(100 + insets.left, 150 + insets.top, 100, 20);
    password.setBounds(200 + insets.left, 150 + insets.top, 200, 20);
    loginBtn.setBounds(150 + insets.left, 200 + insets.top, 100, 20);
  }

  private void initializeTabsPanel() {
    mainPanel.add("Post Job", postJobPanel);
    mainPanel.add("Search by category", searchJobByCategoryPanel);
    mainPanel.add("Search by date", searchJobByDatePanel);
    mainPanel.add("Seach by interval", searchJobByIntervalPanel);
  }
  
  public void addLoginListener(ActionListener listener) {
    loginBtn.addActionListener(listener);
  }
  
  public String[] getCredentials() {
    String[] result = new String[2];
    result[0] = username.getText();
    result[1] = password.getText();
    return result;
  }
  
  public void validateLogin() {
    CardLayout cl = (CardLayout)(daemonPanel.getLayout());
    cl.show(daemonPanel, "1");
  }
  
  //post job methods
  public void addPostJobListener(ActionListener listener) {
    postJobPanel.addListener(listener);
  }
  
  public boolean checkJobDetailsValidity() {
    return postJobPanel.checkValidity();
  }
  
  public void showPostError() {
    postJobPanel.showError();
  }
  
  public Job getJob() {
    return postJobPanel.getJob();
  }
  
  public void clearPostFields() {
    postJobPanel.clearFields();
  }
  
  //search by category methods
  public void addSearchByCategoryListener(ActionListener listener) {
    searchJobByCategoryPanel.addSearchJobListener(listener);
  }
  
  public String getCategory() {
    return searchJobByCategoryPanel.getCategory();
  }
  
  public boolean checkCategorySearchValidity() {
    return searchJobByCategoryPanel.checkValidity();
  }
  
  public void showCategorySearchError() {
    searchJobByCategoryPanel.showError();
  }
  
  public void listJobsInCategorySearch(List<Job> jobs) {
    searchJobByCategoryPanel.showResult(jobs);
  }
  
  //search by date methods
  public void addSearchByDateListener(ActionListener listener) {
    searchJobByDatePanel.addSearchJobListener(listener);
  }
  
  public Date getDate() {
    return searchJobByDatePanel.getDate();
  }
  
  public boolean checkDateSearchValidity() {
    return searchJobByDatePanel.checkValidity();
  }
  
  public void showDateSearchError() {
    searchJobByDatePanel.showError();
  }
  
  public void listJobsInDateSearch(List<Job> jobs) {
    searchJobByDatePanel.showResult(jobs);
  }
  
//search by interval methods
  public void addSearchByIntervalListener(ActionListener listener) {
    searchJobByIntervalPanel.addSearchJobListener(listener);
  }
  
  public Date[] getInterval() {
    return searchJobByIntervalPanel.getInterval();
  }
  
  public boolean checkIntervalSearchValidity() {
    return searchJobByIntervalPanel.checkValidity();
  }
  
  public void showIntervalSearchError() {
    searchJobByIntervalPanel.showError();
  }
  
  public void listJobsInIntervalSearch(List<Job> jobs) {
    searchJobByIntervalPanel.showResult(jobs);
  }
}
