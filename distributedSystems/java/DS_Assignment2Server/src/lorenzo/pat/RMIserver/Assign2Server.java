package lorenzo.pat.RMIserver;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import lorenzo.pat.RMIinterfaces.JobPost;
import lorenzo.pat.RMIinterfaces.JobSearch;
import lorenzo.pat.RMIinterfaces.LogIn;
import lorenzo.pat.beans.Job;
import lorenzo.pat.beans.User;
import lorenzo.pat.dataAccess.JobDao;
import lorenzo.pat.dataAccess.JobDaoImpl;
import lorenzo.pat.dataAccess.UserDao;
import lorenzo.pat.dataAccess.UserDaoImpl;

public class Assign2Server extends UnicastRemoteObject
    implements JobPost, JobSearch, LogIn{

  private static final long serialVersionUID = 1L;
  private JobDao jobDao;
  
  protected Assign2Server() throws RemoteException {
    super();
    jobDao = new JobDaoImpl();
  }
  
  public List<Job> searchJobs(Date date) throws RemoteException {
    List<Job> allJobs = jobDao.getAllJobs();
    List<Job> validJobs = new ArrayList<>();
    
    for ( Job job : allJobs ) {
      if ( areDatesEqual( date, job.getDeadline()) ) {
        validJobs.add(job);
      }
    }
    return validJobs;
  }
  
  public List<Job> searchJobs(Date beginDate, Date endDate) throws RemoteException {
    List<Job> allJobs = jobDao.getAllJobs();
    List<Job> validJobs = new ArrayList<>();
    
    for ( Job job : allJobs ) {
      if ( job.getDeadline().after(beginDate) && job.getDeadline().before(endDate) )
        validJobs.add(job);
    }
    
    return validJobs;
  }
  
  public List<Job> searchJobs(String category) throws RemoteException {
    List<Job> allJobs = jobDao.getAllJobs();
    List<Job> validJobs = new ArrayList<>();
    
    for ( Job job : allJobs ) {
      if (job.getCategory().equals(category))
        validJobs.add(job);
    }
    
    return validJobs;
  }
  
  public void postJob(Job job) throws RemoteException {
    if (!isValidJob(job)) {
      jobDao.addJob(job);
    } else {
      throw new RemoteException();
    }
  }

  @Override
  public boolean isValidLogin(String username, String password)
      throws RemoteException {
    UserDao userDao = new UserDaoImpl();
    List<User> users = userDao.getAllUsers();
    
    User test = new User();
    test.setUsername(username);
    test.setPassword(password);
    
    for ( User user : users ) {
      if (test.equals(user))
        return true;
    }
    
    return false;
  }
  
  private boolean areDatesEqual(Date date1, Date date2) {    
    Calendar cal1 = Calendar.getInstance();  
    Calendar cal2 = Calendar.getInstance();
    cal1.setTime(date1);
    cal2.setTime(date2);
    
    return (cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)) &&
        (cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH)) &&
        (cal1.get(Calendar.DAY_OF_MONTH) == cal2.get(Calendar.DAY_OF_MONTH));
  }
  
  
  private boolean isValidJob(Job job) {
    Date now = new Date();
    Calendar deadline = Calendar.getInstance();  
    deadline.setTime(job.getDeadline());
    Calendar currentDate = Calendar.getInstance();  
    currentDate.setTime(now);
    
    return isDeadlineInThePast(deadline, currentDate);
  }
  
  private boolean isDeadlineInThePast(Calendar deadline, Calendar currentDate) {
    
    return (deadline.get(Calendar.YEAR) < currentDate.get(Calendar.YEAR) ) &&
        (deadline.get(Calendar.MONTH) < currentDate.get(Calendar.MONDAY) ) &&
        (deadline.get(Calendar.DAY_OF_MONTH) < currentDate.get(Calendar.DAY_OF_MONTH) );
  }
}




































