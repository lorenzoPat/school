package lorenzo.pat.RMIserver;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import lorenzo.pat.util.Constant;

public class ServerMain {
  public static void main(String[] args) throws RemoteException, AlreadyBoundException {
    Assign2Server server = new Assign2Server();
    Registry registry = LocateRegistry.createRegistry(Constant.RMI_PORT);
    registry.bind(Constant.RMI_ID, server);
    System.out.println("server started");
  }
}
