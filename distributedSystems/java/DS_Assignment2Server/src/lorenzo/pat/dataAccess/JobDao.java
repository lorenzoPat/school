package lorenzo.pat.dataAccess;

import java.util.List;

import lorenzo.pat.beans.Job;

public interface JobDao {
  public void addJob(Job job);
  public List<Job> getAllJobs();
  public Job getJobById(int jobId);
  public void updateJob(Job job);
  public void removeJob(int jobId);
}
