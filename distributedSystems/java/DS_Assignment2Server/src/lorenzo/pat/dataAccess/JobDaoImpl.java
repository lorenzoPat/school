package lorenzo.pat.dataAccess;

import java.util.List;

import org.hibernate.Session;

import lorenzo.pat.beans.Job;

public class JobDaoImpl implements JobDao {

  @Override
  public void addJob(Job job) {
    Session session = preprocess();
    session.save(job);
    commit(session);
  }

  @Override
  public List<Job> getAllJobs() {
    Session session = preprocess();
    @SuppressWarnings("unchecked")
    List<Job> jobsList = session.createQuery("from Job").list();
    commit(session);
    return jobsList;
  }

  @Override
  public Job getJobById(int jobId) {
    Session session = preprocess();
    Job job = (Job) session.get(Job.class, new Integer(jobId));
    commit(session);
    return job;
  }

  @Override
  public void updateJob(Job job) {
    Session session = preprocess();
    session.update(job);
    commit(session);
  }

  @Override
  public void removeJob(int jobId) {
    Session session = preprocess();
    Job job = (Job) session.load(Job.class, new Integer(jobId));
    session.delete(job);
    commit(session);
  }
  
  private void commit(Session session) {
    session.flush();
    session.getTransaction().commit();
  }

  private Session preprocess() {
    Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    session.beginTransaction();
    return session;
  }
}
