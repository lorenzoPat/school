package lorenzo.pat.dataAccess;

import java.util.List;

import lorenzo.pat.beans.User;

public interface UserDao {
  public void addUser(User user);
  public List<User> getAllUsers();
  public User getUserById(int userId);
  public void updateUser(User user);
  public void removeUser(int userId);
}
