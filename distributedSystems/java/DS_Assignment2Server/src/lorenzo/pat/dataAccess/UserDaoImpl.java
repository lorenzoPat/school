package lorenzo.pat.dataAccess;

import java.util.List;

import lorenzo.pat.beans.User;

import org.hibernate.Session;

public class UserDaoImpl implements UserDao {
  
  public void addUser(User user) {  
    Session session = preprocess();
    session.save(user);
    commitAndClose(session);
    
  }
  
  public List<User> getAllUsers() {
    Session session = preprocess();
    @SuppressWarnings("unchecked")
    List<User> usersList = session.createQuery("from User").list();
    commitAndClose(session);
    return usersList;
  }
  
  public User getUserById(int userId) {
    Session session = preprocess();
    User user = (User) session.get(User.class, new Integer(userId));
    commitAndClose(session);
    return user;
  }
  
  public void updateUser(User user) {
    Session session = preprocess();
      session.update(user);
      commitAndClose(session);
  }
  
  public void removeUser(int userId) {
    Session session = preprocess();
    User user = (User) session.load(User.class, new Integer(userId));
    session.delete(user);
    commitAndClose(session);
  }
  
  private void commitAndClose(Session session) {
    session.flush();
    session.getTransaction().commit();
    //HibernateUtil.getSessionFactory().close();
  }

  private Session preprocess() {
    Session session = HibernateUtil.getSessionFactory().getCurrentSession();
      session.beginTransaction();
    return session;
  }
  
}
