package lorenzo.pat.test;

import static org.junit.Assert.*;

import java.util.List;

import lorenzo.pat.beans.Job;
import lorenzo.pat.dataAccess.JobDao;
import lorenzo.pat.dataAccess.JobDaoImpl;

import org.junit.Before;
import org.junit.Test;

public class JobDataAccessTest {
  JobDao jobDao = new JobDaoImpl();
  
  @Before
  public void setUp() {
    cleanJobTable();
    populateJobTable();
  }
  
  @Test
  public void addJob() {
    Job job = getTestJob();
    
    List<Job> before = jobDao.getAllJobs();
    jobDao.addJob(job);
    List<Job> after = jobDao.getAllJobs();
    
    assertEquals( before.size() + 1, after.size() );
  }
  
  @Test
  public void updateJob() {
    Job job = getTestJob();
    
    jobDao.addJob(job);
    job.setTitle("blabla");
    jobDao.updateJob(job);
    
    List<Job> jobsList = jobDao.getAllJobs();
    boolean testFlag = false;
    
    for ( Job testJob : jobsList )
      if ( job.equals(testJob) )
        testFlag = true;
    
    assertTrue(testFlag);
  }
  
  @Test
  public void deleteJob() {
    Job job = getTestJob();
    
    jobDao.addJob(job);
    List<Job> before = jobDao.getAllJobs();
    jobDao.removeJob(job.getId());
    List<Job> after = jobDao.getAllJobs();
    
    assertEquals( before.size() - 1, after.size() );
  }
  
  private void populateJobTable() {
    Job job0 = new Job();
    job0.setTitle("job0");
    job0.setCategory("cat0");
    Job job1 = new Job();
    job1.setTitle("job1");
    job1.setCategory("cat1");
    Job job2 = new Job();
    job2.setTitle("job2");
    job2.setCategory("cat2");
    Job job3 = new Job();
    job3.setTitle("job3");
    job3.setCategory("cat3");
    
    jobDao.addJob(job0);
    jobDao.addJob(job1);
    jobDao.addJob(job2);
    jobDao.addJob(job3);
  }

  private void cleanJobTable() {
    List<Job> jobsList = jobDao.getAllJobs();
    for ( Job job : jobsList )
      jobDao.removeJob(job.getId());
  }
  
  private Job getTestJob() {
    Job job = new Job();
    job.setTitle("testJob");
    job.setCategory("testCat");
    return job;
  }
  
}
