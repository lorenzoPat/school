package lorenzo.pat.test;

import static org.junit.Assert.*;

import java.util.List;

import lorenzo.pat.beans.User;
import lorenzo.pat.dataAccess.UserDao;
import lorenzo.pat.dataAccess.UserDaoImpl;

import org.junit.Before;
import org.junit.Test;

public class UserDataAccessTest {
  UserDao userDao = new UserDaoImpl();
  
  @Before
  public void setUp() {
    cleanUserTable();
    populateUserTable();
  }
  
  @Test
  public void addUser() {
    User user = getTestUser();
    
    List<User> before = userDao.getAllUsers();
    userDao.addUser(user);
    List<User> after = userDao.getAllUsers();
    
    assertEquals(before.size() + 1, after.size()); 
  }
  
  @Test
  public void updateUser() {
    User testUser = getTestUser();
    
    userDao.addUser(testUser);
    testUser.setUsername("blaTest");
    userDao.updateUser(testUser);
    
    List<User> usersList = userDao.getAllUsers();
    boolean testFlag = false;
    for ( User user : usersList )
      if ( testUser.equals(user) )
        testFlag = true;
    
    assertTrue(testFlag);
  } 
  
  @Test
  public void deleteUser() {
    User user = getTestUser();
    
    userDao.addUser(user);
    List<User> before = userDao.getAllUsers();
    userDao.removeUser(user.getId());
    List<User> after = userDao.getAllUsers();
    
    assertEquals(before.size() - 1, after.size());
  }
  
  private void cleanUserTable(){
    List<User> usersList = userDao.getAllUsers();
    for ( User user : usersList ) 
      userDao.removeUser(user.getId());
  }
  
  private void populateUserTable() {
    User user0 = new User();
    user0.setUsername("user0");
    user0.setPassword("user0");
    User user1 = new User();
    user1.setUsername("user1");
    user1.setPassword("user1");
    User user2 = new User();
    user2.setUsername("user2");
    user2.setPassword("user2");
    User user3 = new User();
    user3.setUsername("user3");
    user3.setPassword("user3");

    userDao.addUser(user0);
    userDao.addUser(user1);
    userDao.addUser(user2);
    userDao.addUser(user3);
  }
  
  private User getTestUser(){
    User user = new User();
    user.setUsername("test1");
    user.setPassword("test1");
    return user;
  }

}
