package lorenzo.pat.business;

import java.util.List;
import lorenzo.pat.dao.UserDao;
import lorenzo.pat.dao.UserDaoImpl;
import lorenzo.pat.entities.User;

public class UserManager {

  private UserDao dao = new UserDaoImpl();

  public UserManager() {
  }

  public boolean isValidUser(String[] credentials) {
    List<User> allUsers = dao.getAll();

    for (User user : allUsers) {
      if (user.getUsername().equals(credentials[0]) && user.getPassword().equals(credentials[1])) {
        return true;
      }
    }

    return false;
  }

  public boolean isAdmin(String[] credentials) {
    List<User> allUsers = dao.getAll();

    for (User user : allUsers) {
      if (user.getUsername().equals(credentials[0]) && user.getType() == 0) {
        return true;
      }
    }

    return false;
  }

  public boolean isStudent(String[] credentials) {
    List<User> allUsers = dao.getAll();

    for (User user : allUsers) {
      if (user.getUsername().equals(credentials[0]) && user.getType() == 1) {
        return true;
      }
    }

    return false;
  }

  public boolean isTeacher(String[] credentials) {
    List<User> allUsers = dao.getAll();

    for (User user : allUsers) {
      if (user.getUsername().equals(credentials[0]) && user.getType() == 2) {
        return true;
      }
    }

    return false;
  }
  
  public List<User> getAllUsers() {
    return dao.getAll();
  }

}
