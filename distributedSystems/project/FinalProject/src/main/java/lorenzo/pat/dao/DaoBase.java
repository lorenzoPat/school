package lorenzo.pat.dao;

import java.util.List;
import lorenzo.pat.entities.User;
import lorenzo.pat.util.HibernateUtil;
import org.hibernate.Session;

public abstract class DaoBase<T> {

  public void create(T genericEntry) {
    Session session = getSession();
    session.save(genericEntry);
    commitAndClose(session);
  }

  public void update(T genericEntry) {
    Session session = getSession();
    session.update(genericEntry);
    commitAndClose(session);
  }

  //getAll & remove - todo: find out how to do it
  
  protected Session getSession() {
    Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    session.beginTransaction();
    return session;
  }

  protected void commitAndClose(Session session) {
    session.flush();
    session.getTransaction().commit();
    //HibernateUtil.getSessionFactory().close();
  }

}
