package lorenzo.pat.dao;

import java.util.List;
import lorenzo.pat.entities.User;

public interface UserDao {

  public void create(User user);

  public void update(User user);

  public List<User> getAll();

  public User getUserById(Integer userId);

  public void remove(Integer userId);

}
