package lorenzo.pat.dao;

import java.util.List;
import lorenzo.pat.entities.UserDetails;

public interface UserDetailsDao {
  
  public void create( UserDetails userDetails );

  public void update( UserDetails userDetails );

  public List<UserDetails> getAll();
  
  public UserDetails getDetailsById(Integer userId);

  public void remove( Integer userId );
  
}
