package lorenzo.pat.dao;

import java.util.List;
import lorenzo.pat.entities.UserDetails;
import org.hibernate.Session;

public class UserDetailsDaoImpl extends DaoBase<UserDetails>
        implements UserDetailsDao {

  @Override
  public List<UserDetails> getAll() {
    Session session = getSession();
    List<UserDetails> usersList = session.createQuery("from UserDetails").list();
    commitAndClose(session);
    return usersList;
  }
  
  @Override
  public UserDetails getDetailsById(Integer userId) {
    Session session = getSession();
    UserDetails userDetails = (UserDetails) session.get(UserDetails.class, userId);
    commitAndClose(session);
    return userDetails;
  }

  @Override
  public void remove(Integer userId) {
    Session session = getSession();
    UserDetails userDetails = (UserDetails) session.load(UserDetails.class, userId);
    session.delete(userDetails);
    commitAndClose(session);
  }

}
