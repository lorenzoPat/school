package lorenzo.pat.entities;

import java.util.HashSet;
import java.util.Set;

public class Group {

  private Integer groupId;
  private String name;
  private Set<User> assignedUsers = new HashSet<User>(0);

  public Group() { }

  public Integer getGroupId() {
    return groupId;
  }

  public void setGroupId(Integer groupId) {
    this.groupId = groupId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Set<User> getAssignedUsers() {
    return assignedUsers;
  }

  public void setAssignedUsers(Set<User> assignedUsers) {
    this.assignedUsers = assignedUsers;
  }

  @Override
  public String toString() {
    return "Group{" + "groupId=" + groupId + ", name=" + name + '}';
  }

}
