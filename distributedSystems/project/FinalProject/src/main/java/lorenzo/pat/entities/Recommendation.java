package lorenzo.pat.entities;

public class Recommendation {

  private Integer recommendationId;
  private String text;
  //private Integer ownerId;
  private Integer authorId;
  private User owner;
  
  public Recommendation() {
  }

  public Integer getRecommendationId() {
    return recommendationId;
  }

  public void setRecommendationId(Integer recommendationId) {
    this.recommendationId = recommendationId;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public User getOwner() {
    return owner;
  }

  public void setOwner(User owner) {
    this.owner = owner;
  }

  public Integer getAuthorId() {
    return authorId;
  }

  public void setAuthorId(Integer authorId) {
    this.authorId = authorId;
  }

  @Override
  public String toString() {
    return "Recommendation{" + "recommendationId=" + recommendationId 
            + ", recommendationText=" + text 
            + ", owner=" + owner.getUsername() 
            + ", authorid=" + authorId + '}';
  }

}
