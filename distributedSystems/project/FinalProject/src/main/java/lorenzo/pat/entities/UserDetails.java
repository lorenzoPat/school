package lorenzo.pat.entities;

import java.util.Date;

public class UserDetails {

  private Integer userId;
  private String firstName;
  private String lastName;
  private String address;
  private Date birthday;
  private String info;
  private User user;

  public UserDetails() {
  }

  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public Date getBirthday() {
    return birthday;
  }

  public void setBirthday(Date birthday) {
    this.birthday = birthday;
  }

  public String getInfo() {
    return info;
  }

  public void setInfo(String info) {
    this.info = info;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  @Override
  public String toString() {
    return "UserDetails{" + "userId=" + userId + ", firstName=" + firstName + ", lastName=" + lastName + ", address=" + address + ", birthday=" + birthday + ", info=" + info + '}';
  }

}
