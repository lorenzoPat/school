package lorenzo.pat.finalproject;

import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.FieldEvents;
import com.vaadin.ui.AbstractTextField;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import java.util.Date;
import java.util.List;
import lorenzo.pat.business.UserManager;
import lorenzo.pat.entities.User;

public class AdminPanel extends Panel {

  private UserManager userManager = new UserManager();
  private Table usersTable = new Table();
  private TextField searchField = new TextField();
  private Button addNewUserBtn = new Button("New user");
  private Button removeUserBtn = new Button("Remove this user");
  private FormLayout editorLayout = new FormLayout();
  private FieldGroup editorFields = new FieldGroup();
  private IndexedContainer usersList = getUsersList();
  
  private TextField usernameField;
  private TextField passwordField;
  private TextField typeField;
  private TextField firstNameField;
  private TextField lastNameField;
  private TextField addressField;
  private TextField birthdayField;
  

  public AdminPanel() {
    setCaption("Admin panel");
    
    usernameField = new TextField();
    passwordField = new TextField();
    typeField = new TextField();
    firstNameField = new TextField();
    lastNameField = new TextField();
    addressField = new TextField();
    birthdayField = new TextField();
    
    initLayout();
    initUsersTable();
    initSearch();
    initEditor();
  }

  private void initLayout() {
    HorizontalSplitPanel splitPanel = new HorizontalSplitPanel();
    VerticalLayout leftLayout = new VerticalLayout();
    HorizontalLayout searchSection = new HorizontalLayout();

    setContent(splitPanel);

    splitPanel.addComponent(leftLayout);
    splitPanel.addComponent(editorLayout);

    leftLayout.addComponent(usersTable);
    leftLayout.addComponent(searchSection);

    searchSection.addComponent(searchField);
    searchSection.addComponent(addNewUserBtn);

    leftLayout.setSizeFull();
    leftLayout.setExpandRatio(usersTable, 1);
    usersTable.setSizeFull();

    searchSection.setWidth("100%");
    searchField.setWidth("100%");
    searchSection.setExpandRatio(searchField, 1);

    editorLayout.setMargin(true);
    //editorLayout.setVisible(false);
  }

  private void initUsersTable() {
    List<User> allUsers = userManager.getAllUsers();

    for (User user : allUsers) {
      Object id = usersList.addItem();
      usersList.getContainerProperty(id, "UserId").setValue(user.getUserId());
      usersList.getContainerProperty(id, "Username").setValue(user.getUsername());
      usersList.getContainerProperty(id, "Password").setValue(user.getPassword());
      usersList.getContainerProperty(id, "Type").setValue(user.getType());
      usersList.getContainerProperty(id, "First name").setValue(user.getUserDetails().getFirstName());
      usersList.getContainerProperty(id, "Last name").setValue(user.getUserDetails().getLastName());
      usersList.getContainerProperty(id, "Address").setValue(user.getUserDetails().getAddress());
      usersList.getContainerProperty(id, "Birthday").setValue(user.getUserDetails().getBirthday());
    }

    usersTable.setContainerDataSource(usersList);
    usersTable.setVisibleColumns(new String[]{"UserId", "Username", "Password", "Type", "First name", "Last name", "Address", "Birthday"});
    usersTable.setSelectable(true);
    usersTable.setImmediate(true);

    usersTable.addValueChangeListener( new Property.ValueChangeListener() {

      @Override
      public void valueChange(Property.ValueChangeEvent event) {

        Object contactId = usersTable.getValue();
        if ( contactId != null ) {
          editorFields.setItemDataSource(usersTable.getItem(contactId));
          editorLayout.setVisible(true);
        }
        
      }
    } );
    
  }
  
  private IndexedContainer getUsersList() {
    IndexedContainer ic = new IndexedContainer();

    ic.addContainerProperty("UserId", Integer.class, 0);
    ic.addContainerProperty("Username", String.class, "");
    ic.addContainerProperty("Password", String.class, "");
    ic.addContainerProperty("Type", Integer.class, 0);
    ic.addContainerProperty("First name", String.class, "");
    ic.addContainerProperty("Last name", String.class, "");
    ic.addContainerProperty("Address", String.class, "");
    ic.addContainerProperty("Birthday", Date.class, null);
    
    return ic;
  }
  
  private void initEditor() {
    editorLayout.addComponent(usernameField);
    editorLayout.addComponent(passwordField);
    editorLayout.addComponent(typeField);
    editorLayout.addComponent(firstNameField);
    editorLayout.addComponent(lastNameField);
    usernameField.setWidth("100%");
    editorFields.bind(usernameField, "Username: ");
    
//    // add a field for each field in the list of fieldNames, create a input text field for each one
//    // and bind the text field to the field name
//    for (String fieldName : fieldNames) {
//      TextField field = new TextField(fieldName);
//      editorLayout.addComponent(field);
//      field.setWidth("100%");
//
//      editorFields.bind(field, fieldName);
//    }
    
//    editorLayout.addComponent(removeUserBtn);
    
    //editorLayout.setVisible(true);
  //editorFields.setBuffered(false);
  }

  private void initSearch() {
    searchField.setInputPrompt("Search users");
    searchField.setTextChangeEventMode(AbstractTextField.TextChangeEventMode.EAGER);
    searchField.addTextChangeListener(new FieldEvents.TextChangeListener() {
      
      @Override
      public void textChange(final FieldEvents.TextChangeEvent event) {
        usersList.removeAllContainerFilters();
        usersList.addContainerFilter(new ContactFilter(event.getText()));
      }
    });
  }
  
  private class ContactFilter implements Container.Filter {
    private String needle;

    public ContactFilter(String needle) {
      this.needle = needle.toLowerCase();
    }

    @Override
    public boolean passesFilter(Object itemId, Item item) {
      
      String haystack = ("" + item.getItemProperty("UserId").getValue()
                        + item.getItemProperty("Username").getValue() 
                        + item.getItemProperty("First name").getValue()).toLowerCase();
      return haystack.contains(needle);
    }

    @Override
    public boolean appliesToProperty(Object id) {
      return true;
    }
  }
  
  
}
