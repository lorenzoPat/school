package lorenzo.pat.finalproject;

import com.vaadin.ui.Button;
import lorenzo.pat.business.UserManager;

public class Controller {

  private UserManager userManager;
  private MyVaadinUI view;

  public Controller(MyVaadinUI view) {
    this.view = view;
    userManager = new UserManager();

    addEventHandlers();
  }

  private void addEventHandlers() {
    addLoginHandler();
  }

  private void addLoginHandler() {
    view.addClickListenerForLoginButton(new Button.ClickListener() {

      @Override
      public void buttonClick(Button.ClickEvent event) {
        String[] credentials = view.getLoginCredentials();
        if (userManager.isValidUser(credentials)) {
          if (userManager.isAdmin(credentials)) {
            view.setPanel(MyVaadinUI.ADMIN);
          }

          if (userManager.isStudent(credentials)) {
            view.setPanel(MyVaadinUI.STUDENT);
          }

          if (userManager.isTeacher(credentials)) {
            view.setPanel(MyVaadinUI.TEACHER);
          }
        }
      }
    });
  }

}
