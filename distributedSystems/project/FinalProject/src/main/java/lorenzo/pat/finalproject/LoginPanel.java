package lorenzo.pat.finalproject;

import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class LoginPanel extends Panel {

  private Label usernameLabel;
  private Label passwordLabel;
  private TextField username;
  private PasswordField password;
  private Button loginBtn;

  public LoginPanel() {
    setCaption("Login panel");

    usernameLabel = new Label("Username:");
    passwordLabel = new Label("Password");
    username = new TextField();
    password = new PasswordField();
    loginBtn = new Button("Log in");

    VerticalLayout mainLayout = new VerticalLayout();
    mainLayout.addComponent(usernameLabel);
    mainLayout.addComponent(username);
    mainLayout.addComponent(passwordLabel);
    mainLayout.addComponent(password);
    mainLayout.addComponent(loginBtn);

    setContent(mainLayout);
  }

  public void addClickListenerForLoginButton(Button.ClickListener listener) {
    loginBtn.addClickListener(listener);
  }

  public String[] getLoginCredentials() {
    String result[] = new String[2];

    result[0] = username.getValue();
    result[1] = password.getValue();

    return result;
  }

}
