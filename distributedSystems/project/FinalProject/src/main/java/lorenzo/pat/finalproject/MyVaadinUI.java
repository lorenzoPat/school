package lorenzo.pat.finalproject;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Button;
import com.vaadin.ui.UI;

@Theme("mytheme")
@SuppressWarnings("serial")
public class MyVaadinUI extends UI {

  public static final String LOGIN = "login";
  public static final String STUDENT = "student";
  public static final String ADMIN = "admin";
  public static final String TEACHER = "teacher";


  private LoginPanel loginPanel;
  private StudentPanel studentPanel;
  private AdminPanel adminPanel;
  private TeacherPanel teacherPanel;

  @WebServlet(value = "/*", asyncSupported = true)
  @VaadinServletConfiguration(productionMode = false, ui = MyVaadinUI.class, widgetset = "lorenzo.pat.finalproject.AppWidgetSet")
  public static class Servlet extends VaadinServlet {
  }

  @Override
  protected void init(VaadinRequest request) {
    loginPanel = new LoginPanel();
    studentPanel = new StudentPanel();
    adminPanel = new AdminPanel();
    teacherPanel = new TeacherPanel();
    
    Controller controller = new Controller(this);
    
    setPanel(ADMIN);
  }

  public void setPanel(String panelName) {
    if (panelName.equals(LOGIN))  setContent(loginPanel);
    
    if (panelName.equals(STUDENT))  setContent(studentPanel);
    
    if (panelName.equals(TEACHER))  setContent(teacherPanel);

    if (panelName.equals(ADMIN))  setContent(adminPanel);
  }

  //login methods
  public void addClickListenerForLoginButton(Button.ClickListener listener) {
    loginPanel.addClickListenerForLoginButton(listener);
  }

  public String[] getLoginCredentials() {
    return loginPanel.getLoginCredentials();
  }

}
