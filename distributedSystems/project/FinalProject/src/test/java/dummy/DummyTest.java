package dummy;

import java.util.List;
import java.util.Set;
import lorenzo.pat.entities.Group;
import lorenzo.pat.entities.Recommendation;
import lorenzo.pat.entities.User;
import lorenzo.pat.entities.UserDetails;
import lorenzo.pat.util.HibernateUtil;
import org.hibernate.Session;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class DummyTest {

  public DummyTest() {
  }
/*
  @BeforeClass
  public static void setUpClass() {
  }

  @AfterClass
  public static void tearDownClass() {
  }

  @Before
  public void setUp() {
  }

  @After
  public void tearDown() {
  }

  @Test
  public void hello() {
    System.out.println("Hello World!");
    Session session = HibernateUtil.getSessionFactory().openSession();

    session.beginTransaction();

//    addSimpleUser(session);
//    addUserWithDetails(session);
//    addNewGroup(session);
//    addUsersToGroup(session);
//    removeUsersFromGroup(session);
//    showUsersAnGroup(session);
//    addRecommendation(session);
    playWithRecommendations(session);

    session.getTransaction().commit();
    session.close();

  }

  private void addSimpleUser(Session session) {
    User u = new User();
    u.setPassword("testPass1");
    u.setUsername("testUsername1");
    u.setType(1);
    session.save(u);
  }

  private void addUserWithDetails(Session session) {
    User u = new User();
    u.setPassword("testPass1");
    u.setUsername("testUsername1");
    u.setType(1);

    UserDetails ud = new UserDetails();
    ud.setFirstName("testName1");
    ud.setLastName("test1");

    u.setUserDetails(null);
    //ud.setUser(u);
    session.save(u);
    //session.save(ud);
  }

  private void addUsersToGroup(Session session) {

    List<Group> groups = session.createQuery("from Group").list();
    List<User> users = session.createQuery("from User").list();

    Group gr = groups.get(2);
    int index = 0;
    for (User u : users) {

      if (index++ % 2 == 0) {
        System.out.println("adding a user");
        gr.getAssignedUsers().add(u);
        u.getGroups().add(gr);
        session.saveOrUpdate(u);
      }

    }
    session.saveOrUpdate(gr);
  }

  private void removeUsersFromGroup(Session session) {
    List<Group> groups = session.createQuery("from Group").list();
    Group gr = groups.get(2);

    Set<User> users = gr.getAssignedUsers();
    User del = new User();
    for (User u : users) {
      del = u;
      break;
    }

    System.out.println("user to delete is " + del);
    gr.getAssignedUsers().remove(del);
    del.getGroups().remove(gr);
    //session.update(gr); // merge si asa
    session.update(del);

  }

  void showUsersAnGroup(Session session) {

    List<User> users = session.createQuery("from User").list();

    System.out.println("list test");
    for (User u : users) {
      System.out.println("potato " + u.getGroups());
    }

  }

  private void addNewGroup(Session session) {
    Group gr = new Group();
    gr.setName("white");
    session.save(gr);
  }

  private void addRecommendation(Session session) {
    Recommendation reco = new Recommendation();
    List<User> users = session.createQuery("from User").list();
    User u1 = new User();
    User u2 = new User();
    int index = 0;
    for (User u : users) {
      if (index == 2) {
        break;
      }
      if (index == 1) {
        u1 = u;
      }
      if (index == 0) {
        u2 = u;
      }
      index++;
    }

    System.out.println("u1: " + u1);
    System.out.println("u2: " + u2);

    reco.setText("alba bala portocala");
    reco.setAuthorId(u1.getUserId());
    reco.setOwner(u2);
    u2.getRecommendations().add(reco);

    session.save(reco);
//    session.save(u2);

  }

  private void playWithRecommendations(Session session) {
    List<User> users = session.createQuery("from User").list();
    User u = users.get(0);
    System.out.println("user " + u);
    Set<Recommendation> recs = u.getRecommendations();
    int i = 0;
    for ( Recommendation r : recs ) {
      //System.out.println("rec: " + r);
      r.setText("a new text");
      session.update(r);
    }
  }
*/
}
