package lorenzo.pat.dao;

import java.util.List;
import lorenzo.pat.entities.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class UserDaoImplTest {

  UserDao instance = new UserDaoImpl();

  public UserDaoImplTest() {
  }
/*
  @Before
  public void setUp() {
    cleanUserTable();
    populateUserTable();
  }

  @After
  public void tearDown() {
    //cleanUserTable();
  }

  @Test
  public void create() {
    User user = getTestUser();
    instance.create(user);
    assert (isUserInDB(user));
  }

  @Test(expected=org.hibernate.PropertyValueException.class)
  public void createInvalidUser() {
    User user = getEmptyUser();
    instance.create(user);
  }
//  
//  @Test(expected=org.hibernate.exception.ConstraintViolationException.class)
//  public void createAlreadyExistingUser() {
//    User user = getTestUser();
//    instance.create(user);
//    instance.create(user);
//  }
  
  @Test
  public void remove() {
    User user = getTestUser();
    instance.create(user);
    instance.remove(user.getUserId());
    assert (!isUserInDB(user));
  }

  @Test
  public void update() {
    User user = getTestUser();
    instance.create(user);

    user.setUsername("anotherUsername");
    instance.update(user);

    assert (isUserInDB(user));

  }

  private User getTestUser() {
    User testUser = new User();
    testUser.setUsername("testUsername");
    testUser.setPassword("testPassword");
    testUser.setType(1);
    return testUser;
  }

  private User getEmptyUser() {
    User user = new User();
    return user;
  }

  private void cleanUserTable() {
    List<User> users = instance.getAll();
    for (User user : users) {
      instance.remove(user.getUserId());
    }
  }

  private void populateUserTable() {
    User user0 = new User();
    User user1 = new User();
    User user2 = new User();
    User user3 = new User();
    User user4 = new User();

    user0.setUsername("user0");
    user1.setUsername("user1");
    user2.setUsername("user2");
    user3.setUsername("user3");
    user4.setUsername("user4");

    user0.setPassword("user0");
    user1.setPassword("user1");
    user2.setPassword("user2");
    user3.setPassword("user3");
    user4.setPassword("user4");

    user0.setType(1);
    user1.setType(1);
    user2.setType(1);
    user3.setType(1);
    user4.setType(1);

    instance.create(user0);
    instance.create(user1);
    instance.create(user2);
    instance.create(user3);
    instance.create(user4);
  }

  private boolean isUserInDB(User user) {
    List<User> allUsers = instance.getAll();
    return allUsers.contains(user);
  }
*/
}
