#include <stdio.h>
#include <conio.h>
#include <iostream>
#include <fstream>
#include "pvm3.h"
#include "defines.h"

using namespace std;

#define SMALL_MATR_SIZE 4

void showTIDMatrix(int blocks, int* tids);
void generateMatrix(int** matrix, int size, int rand);
void showMatrix(int** matrix, int size);
void formatMatrix(int** matrix, int size, int smallSize, bool up_left);
void copyMatrix(int** origMatrix, int** matrix, int matrixSize);

main(int argc, char* argv[])
{
  int* tids;
  int** matrix1;
  int** matrix2;
  int** matrix1_orig;
  int** matrix2_orig;
  int** slaveResult;
  int** myResult;
  int** final;
  int matrixSize = 8, smallSize = 4;
  int blocks; // how many blocks are on line & column
  int slavesNr;
	int openProcs;
  
  
  int** auxMatr;
  int* matA;
  int* matB;
  int* matC;
  
  int flag;
  int iterations = 0;
  int i, j, k, ii, jj, k1,k2;
	int leftNeighb, rightNeighb, upNeighb, bottomNeighb;
  
  blocks = matrixSize / smallSize;
  slavesNr = blocks * blocks;
  
  //start the slaves

  cout << "slaves: " << slavesNr <<endl;
  tids = new int[slavesNr];
  openProcs = pvm_spawn("Slave", NULL, 0, "", slavesNr, tids);
  
  if (openProcs != slavesNr) {
    cout << "Warning: could not open all slaves" << endl;
    pvm_exit();
    exit(0);
  }
  
  cout << "slaves matrix: " << endl; showTIDMatrix(blocks, tids);

  //create the matrices
  matrix1 = new int*[matrixSize];
  matrix2 = new int*[matrixSize];
  matrix1_orig = new int*[matrixSize];
  matrix2_orig = new int*[matrixSize];
  slaveResult = new int*[matrixSize];
  myResult = new int*[matrixSize];
  final = new int*[matrixSize];

  for (i = 0; i < matrixSize; i++) {
    matrix1[i] = new int[matrixSize];
    matrix2[i] = new int[matrixSize];
    matrix1_orig[i] = new int[matrixSize];
    matrix2_orig[i] = new int[matrixSize];
    slaveResult[i] = new int[matrixSize];
    myResult[i] = new int[matrixSize];
    final[i] = new int[matrixSize];
  }

  generateMatrix(matrix1, matrixSize, 11);
  generateMatrix(matrix2, matrixSize, 19);

  //make a copy of matrices
  copyMatrix(matrix1, matrix1_orig, matrixSize);
  copyMatrix(matrix2, matrix2_orig, matrixSize);

  cout << endl << "matrix 1" <<endl;
  showMatrix(matrix1, matrixSize);
  cout << endl << "matrix 2" << endl;
  showMatrix(matrix2, matrixSize);

  //here
  // format matrices
  formatMatrix(matrix1, matrixSize, smallSize, true);
  formatMatrix(matrix2, matrixSize, smallSize, false);
  
  cout << endl << "done formatting..." << endl;
  cout << endl << "matrix 1" <<endl;
  showMatrix(matrix1, matrixSize);
  cout << endl << "matrix 2" << endl;
  showMatrix(matrix2, matrixSize);


  //initialize the matrices to be sent to slaves
  auxMatr = new int*[smallSize];
  for ( i = 0; i < smallSize; i++ )
    auxMatr[i] = new int[smallSize];

  matA = new int[smallSize * smallSize];
  matB = new int[smallSize * smallSize];
  matC = new int[smallSize * smallSize];

  //send each process its neighbours, their matrices and the size of matrices
  for ( i = 0; i < blocks; i++ ) {
    for ( j = 0; j < blocks; j++ ) {
      
      //right neighbour
      if ( j == blocks-1 )
        rightNeighb = tids[i * blocks];
      else
        rightNeighb = tids[i * blocks + j + 1];

      //left neighbour
      if ( j == 0 )
        leftNeighb = tids[i * blocks + blocks - 1];
      else
        leftNeighb = tids[i * blocks + j - 1];

      //up neighbour
      if ( i == 0 )
        upNeighb = tids[(blocks-1) * blocks + j];
      else
        upNeighb = tids[(i-1) * blocks + j];
      
      //bottom neighbour
      if ( i == blocks-1 )
        bottomNeighb = tids[j];
      else
        bottomNeighb = tids[(i+1) * blocks + j];
   
      //i si j reprezinta si pozitia in matrice
      for ( ii = 0 ; ii < smallSize; ii++ ) {
        for ( jj = 0 ; jj < smallSize; jj++ ) {
          matA[ii * smallSize + jj] = matrix1[i * smallSize + ii][j * smallSize + jj];
          matB[ii * smallSize + jj] = matrix2[i * smallSize + ii][j * smallSize + jj];
        }
      }

      //check
      cout << "for tid[" << i <<"][" << j << "] neighbours are:"<<endl;
      cout << "rightN = " << rightNeighb << endl;
      cout << "leftN = " << leftNeighb << endl;
      cout << "upN = " << upNeighb << endl;
      cout << "bottomN = " << bottomNeighb << endl;
      cout << "matrixA: " << endl;
      for ( ii = 0 ; ii < smallSize; ii++ ) {
        cout << endl;
        for ( jj = 0 ; jj < smallSize; jj++ ) {
          cout << matA[ii * smallSize + jj] << " ";
        }
      }
      cout << endl << "matrixB: " << endl;
      for ( ii = 0 ; ii < smallSize; ii++ ) {
        cout << endl;
        for ( jj = 0 ; jj < smallSize; jj++ ) {
          cout << matB[ii * smallSize + jj] << " ";
        }
      }
      
      
      pvm_initsend(PvmDataDefault);
      pvm_pkint(&upNeighb, 1, 1);
      pvm_pkint(&rightNeighb, 1, 1);
      pvm_pkint(&bottomNeighb, 1, 1);
      pvm_pkint(&leftNeighb, 1, 1);
      pvm_pkint(&smallSize, 1, 1);
      pvm_pkint(&blocks, 1, 1);
      pvm_pkint(matA, smallSize*smallSize, 1);
      pvm_pkint(matB, smallSize*smallSize, 1);
      pvm_send(tids[i*blocks + j],InitMsg);
    }
  }

  
  //now each slave knows its position in the matrix and its neighbours

  //sinchronize slaves
  //steps 2 & 3 will be repeated [division] times
  //iterations++;
  while (iterations < blocks) {
    cout << "iteration " << iterations << endl;
    iterations++;
    
    //wait untill all slaves have computed their part
    for ( i = 0 ; i < slavesNr; i++ ) {
      pvm_recv(tids[i], SlaveMasterMsg);
      pvm_upkint(&flag, 1, 1);
      cout << "received from slave " << i << " msg " << flag << endl; 
    }
    
    ++flag;

    //send back continue msg
    for ( i = 0 ; i < slavesNr; i++ ) {
      pvm_initsend(PvmDataDefault);
      pvm_pkint(&flag, 1, 1);
      pvm_send(tids[i], MasterSlaveMsg);
    }

  }
  

  cout << "done... assembling divisions" << endl;
  
  for ( i = 0 ; i < blocks; i++ ) {
    for ( j = 0 ; j < blocks; j++ ) {
      pvm_recv(tids[i * blocks + j], SlaveMasterMsg);
      pvm_upkint(matC, smallSize * smallSize, 1);
      
      for ( ii = 0; ii < smallSize; ii++ ) {
        for ( jj = 0; jj < smallSize; jj++ ) {
          slaveResult[i*smallSize + ii][j * smallSize + jj] = matC[ii * smallSize + jj];
        }
      }
            
    }
  }

  cout << "received: " << endl;
  for ( i = 0; i < matrixSize; i++ ) {
    cout << endl;
    for ( j = 0; j < matrixSize; j++ ) {
      cout << slaveResult[i][j] << " ";// = 0;
    }
  }


  cout << "compute my result..." << endl;

  //first initialize my result with 0
  for ( i = 0; i < matrixSize; i++ ) {
    for ( j = 0; j < matrixSize; j++ ) {
      myResult[i][j] = 0;
    }
  }


  for ( i = 0; i < matrixSize; i++ ) {
    for ( j = 0; j < matrixSize; j++ ) {
      for ( k = 0; k < matrixSize; k++ ) {
        myResult[i][j] = myResult[i][j] + matrix1_orig[i][k] * matrix2_orig[k][j];
      }
    }
  }
  
  cout << "computed: " << endl;
  for ( i = 0; i < matrixSize; i++ ) {
    cout << endl;
    for ( j = 0; j < matrixSize; j++ ) {
      cout << myResult[i][j] << " ";
    }
  }

  cout << "subtract matrices..." <<endl;
  for ( i = 0; i < matrixSize; i++ ) {
    for ( j = 0; j < matrixSize; j++ ) {
      final[i][j] = myResult[i][j] - slaveResult[i][j];
    }
  }

  cout << "print subtract..." <<endl;
  for ( i = 0; i < matrixSize; i++ ) {
    cout << endl;
    for ( j = 0; j < matrixSize; j++ ) {
      cout << final[i][j] << " ";// = myResult[i][j] - slaveResult[i][j];
    }
  }

  getch();

  delete tids;
  for ( i = 0; i < smallSize; i++ )
    delete auxMatr[i];
  delete auxMatr;
  
  for ( i = 0; i < matrixSize; i++ ) {
    delete matrix1[i];
    delete matrix2[i];
    delete matrix1_orig[i];
    delete matrix2_orig[i];
    delete slaveResult[i];
    delete myResult[i];
    delete final[i];
  }
  
  delete matrix1;
  delete matrix2;
  delete matrix1_orig;
  delete matrix2_orig;
  delete slaveResult;
  delete myResult;
  delete final;
	
  pvm_exit();
	exit(0);
}




void generateMatrix(int** matrix, int size, int random) {
  int i, j;
  for ( i = 0; i < size; i++ ) {
    for ( j = 0; j < size; j++ ) {
      matrix[i][j] = rand() % random;
    }
  }
}

void showMatrix(int** matrix, int size) {
  int i, j;
  for ( i = 0; i < size; i++ ) {
    cout << endl;
    for ( j = 0; j < size; j++ ) {
      cout << matrix[i][j] << " ";
    }
  }
}

void formatMatrix(int** matrix, int size, int smallSize, bool up_left) {
  int i, j, ii, jj, k1, k2;
  int** auxMatr;
  auxMatr = new int*[smallSize];
  for ( i = 0; i < smallSize; i++ )
    auxMatr[i] = new int[smallSize];

  int division = size / smallSize;  

  if (up_left) { // move blocks to left
    
    for ( i = 0; i < division; i++ ) {              //pt fiecare row
      for ( ii = 0; ii < i; ii++ ) {                //iterez de i ori
      
        //tin minte primul bloc pt fiecare iteratie
        for (k1 = 0; k1 < smallSize; k1++) 
          for (k2 = 0; k2 < smallSize; k2++)
            auxMatr[k1][k2] = matrix[i*smallSize + k1][k2];
      
        //mut blocurile de la 0 la division-1
        for ( j = 0; j < division-1; j++ ) {        
          for (k1 = 0; k1 < smallSize; k1++) 
            for (k2 = 0; k2 < smallSize; k2++)
              matrix[i*smallSize + k1][j*smallSize + k2] = matrix[i*smallSize + k1][(j+1)*smallSize + k2];      
        }

        //pun in capat ce era in fata
        for (k1 = 0; k1 < smallSize; k1++) 
          for (k2 = 0; k2 < smallSize; k2++) 
            matrix[i*smallSize + k1][(division-1)*smallSize + k2] = auxMatr[k1][k2];
      }
    }
  } else {
    for ( j = 0; j < division; j++ ) {              //pt fiecare coloana
      for ( jj = 0; jj < j; jj++ ) {                //iterez de j ori
      
        //tin minte primul bloc pt fiecare iteratie
        for (k1 = 0; k1 < smallSize; k1++) 
          for (k2 = 0; k2 < smallSize; k2++) 
            auxMatr[k1][k2] = matrix[k1][j * smallSize + k2];
         
      
        //mut blocurile de la 0 la division-1
        for ( i = 0; i < division-1; i++ ) {        
          for (k1 = 0; k1 < smallSize; k1++) 
            for (k2 = 0; k2 < smallSize; k2++)
              matrix[i*smallSize + k1][j*smallSize + k2] = matrix[(i+1)*smallSize + k1][j * smallSize + k2];      
        }

        //pun in capat ce era in fata
        for (k1 = 0; k1 < smallSize; k1++) 
          for (k2 = 0; k2 < smallSize; k2++) 
            matrix[(division-1)*smallSize + k1][j*smallSize + k2] = auxMatr[k1][k2];
      }
    }
  }

  for ( i = 0; i < smallSize; i++ )
    delete auxMatr[i];
  delete auxMatr;
}

void showTIDMatrix(int blocks, int* tids){
  int i, j;
  for ( i = 0; i < blocks; i++ ) {
    cout << endl;
    for ( j = 0; j < blocks; j++ ) {
      cout  << tids[ i * blocks + j] << " ";
    }
  }
}

void copyMatrix(int** origMatrix, int** matrix, int matrixSize) {
  int i, j;
  for ( i = 0; i < matrixSize; i++ ) {
    for ( j = 0; j < matrixSize; j++ ) {
      matrix[i][j] = origMatrix[i][j];
    }
  }
}