#include <stdio.h>
#include <iostream>
#include <fstream>
#include "pvm3.h"
#include "defines.h"

using namespace std;

main(int argc, char* argv[]) {
	int parentTID = pvm_parent();
  int i, j, k;
  int upNeighb, rightNeighb, bottomNeighb, leftNeighb;
  int smallSize, blocks;
  int* matA;
  int* matB;
  int* matC;
  int iterations = 0, msg;
  int testFlag;
  int matrixSize;
  ofstream log("log.txt");

  //get init data from master
  pvm_recv(parentTID, InitMsg);
  pvm_upkint(&upNeighb, 1, 1);
  pvm_upkint(&rightNeighb, 1, 1);
  pvm_upkint(&bottomNeighb, 1, 1);
  pvm_upkint(&leftNeighb, 1, 1);
  pvm_upkint(&smallSize, 1, 1);
  pvm_upkint(&blocks, 1, 1);
  
  matA = new int[smallSize * smallSize];
  matB = new int[smallSize * smallSize];
  
  pvm_upkint(matA, smallSize*smallSize, 1);
  pvm_upkint(matB, smallSize*smallSize, 1);
  
  matC = new int[smallSize * smallSize];
  for ( i = 0 ; i < smallSize * smallSize; i++)
    matC[i] = 0;
  
  iterations = 0;
  while ( iterations < blocks ) {  //repeat steps 2 & 3 [division] times
    iterations++;
    msg = 0; //msg is sent to master, master increments it and sends back to slave which continues the execution
    Sleep(10);
    
    //step2 - multiplication
    for ( i = 0; i < smallSize; i++ ) {
      for ( j = 0; j < smallSize; j++ ) {
        for ( k = 0; k < smallSize; k++ ) {
          matC[i * smallSize + j] = matC[i * smallSize + j] + matA[i * smallSize + k] * matB[k * smallSize + j];
        }
      }
    }
    
   
    //send master finish message
    testFlag = msg;
    pvm_initsend(PvmDataDefault);
    pvm_pkint(&msg, 1, 1);
    pvm_send(parentTID, SlaveMasterMsg);
    
    log << "sent msg to master" << endl;

    //step3 - movement
    //wait until master responds
    
    while ( msg != testFlag ) {
      pvm_recv(parentTID, MasterSlaveMsg);
      pvm_upkint(&msg, 1, 1);
    }
    
    //actual move of the blocks
    //send matA to leftN
    //send matB to upN
    //receive matA from rightN
    //receive matB from bottomN
    pvm_initsend(PvmDataDefault);
    pvm_pkint(matA, smallSize*smallSize, 1);
    pvm_send(leftNeighb, SlaveSlaveMsg);

    pvm_initsend(PvmDataDefault);
    pvm_pkint(matB, smallSize*smallSize, 1);
    pvm_send(upNeighb, SlaveSlaveMsg);

    pvm_recv(rightNeighb, SlaveSlaveMsg);
    pvm_upkint(matA, smallSize * smallSize, 1);

    pvm_recv(bottomNeighb, SlaveSlaveMsg);
    pvm_upkint(matB, smallSize * smallSize, 1);
  }
    
  
  pvm_initsend(PvmDataDefault);
  pvm_pkint(matC, smallSize*smallSize, 1);
  pvm_send(parentTID, SlaveMasterMsg);
  
/*  
  delete matA;
  delete matB;
  */
	pvm_exit();
	exit(0);
}
