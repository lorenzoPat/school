#define MAX_SLAVES_NR 20
#define RGB_COLORS 256
#define INIT_MSG_TAG 1

enum MsgTags { InitMsg, SlaveMasterMsg, MasterSlaveMsg };