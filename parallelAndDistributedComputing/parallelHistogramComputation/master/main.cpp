#include <stdio.h>
#include <iostream>
#include <fstream>
#include "pvm3.h"
#include "defines.h"

using namespace std;

main(int argc, char* argv[])
{
  int slavesNr;
	int openProcs;
	int tids[MAX_SLAVES_NR];
  int i, j;
  int bufID;
  int receivedHistogram[RGB_COLORS];
  int finalHistogram[RGB_COLORS];

  if (argc < 3) {
    cout << "few arguments: " << "please call the program using the following sintax: pvm_master [fileName] [slaveNr]" << endl;
    pvm_exit();
    exit(0);
  }

  //get the nr of slaves
  sscanf(argv[2], "%d", &slavesNr);
  
  if (slavesNr > MAX_SLAVES_NR) {
    cout<< "nr of procs exceeds the limit of " << MAX_SLAVES_NR << endl;
    pvm_exit();
    exit(0);
  }
  
  //start the slaves passing them the same args
  openProcs = pvm_spawn("PVM_Slave", argv, 0, "", slavesNr, tids);
  
	if ( slavesNr != openProcs ) {
    cout << "warning: could not open all slaves" << endl;
  }

  // send slaves info about how many procs have been started to split the work
  // send them their position in the list of processes to know which part of the image to compute
  for (i = 0; i < openProcs; i++) {
    int initData[2] = {i, openProcs};
    pvm_initsend(PvmDataDefault);
    cout << "sending initMsg to process " << i << "/" << openProcs <<endl; 
    pvm_pkint(initData, 2, 1);
    pvm_send(tids[i], InitMsg);
  }
  
  //initialize final histogram
  for (i = 0; i < RGB_COLORS; i++)
    finalHistogram[i] = 0;
  
  //read the partial histograms computed by slaves
  for (i = 0; i < openProcs; i++){
    bufID = pvm_recv(tids[i], SlaveMasterMsg);
	  pvm_upkint(receivedHistogram, RGB_COLORS, 1);
   
    //compute the final histogram
    for (j = 0; j < RGB_COLORS; j++) 
      finalHistogram[j] += receivedHistogram[j];
  }
  
  //print the final histogram
  for (i = 0; i < RGB_COLORS; i++) {
    cout<<"histogram[" << i << "] = " << finalHistogram[i] << endl;
  }
  
	pvm_exit();
	exit(0);
}


