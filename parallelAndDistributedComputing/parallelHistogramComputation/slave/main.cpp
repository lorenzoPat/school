#include <stdio.h>
#include <iostream>
#include <fstream>
#include "pvm3.h"
#include "defines.h"

using namespace std;

main(int argc, char* argv[]) {
	int parentTID = pvm_parent();
  int bufID;
  int initData[2];
  int i, j;  
  int rows, columns;
  int offset;
  int image[IMAGE_LIMITS][IMAGE_LIMITS];
  int histogram[RGB_COLORS];
  int lowerBound, upperBound;
    
  //receive init message from master
  bufID = pvm_recv(parentTID, InitMsg);
  pvm_upkint(initData, 2, 1);

  //open the img file
  ifstream input(argv[2]);
  if (!input.is_open()) {
    //treat this case - should it retry to open the file or return error msg?
  }
  
  input >> rows;
  input >> columns;
  offset = rows / initData[1];//how many rows will be computed by each slave

  //read the image from the file
  for (i = 0; i < rows; i++) {
    for (j = 0; j < columns; j++){
      input >> image[i][j];
    }
  }

  lowerBound = offset * initData[0];
  //if the nr of rows is not multiple of slaveNr, last slave should 
  //compute the lines that otherwise would be rejected 
  upperBound = (initData[0] == initData[1] - 1) ? rows : offset * (initData[0] + 1);
    
  //init histogram
  for (i = 0; i < RGB_COLORS; i++) {
    histogram[i] = 0;
  }

  //compute histogram
  for (i = lowerBound; i < upperBound; i++) {
    for (j = 0; j < columns; j++){
      histogram[image[i][j]]++;
    }
  }

  //send response to master
	pvm_initsend(PvmDataDefault);
	pvm_pkint(histogram, RGB_COLORS, 1);
  pvm_send(parentTID, SlaveMasterMsg);

	pvm_exit();
	exit(0);
}

