#include "stdafx.h"
#include "common.h"

pointVector getListOfPoints( BYTE* sourceImage, int imgHeight, int imgWidth, int imgOffset ) {
  pointVector pointsList;
  for (int i = 0; i < imgHeight; i++){
    for (int j = 0; j < imgWidth; j++) {	
      if ( sourceImage[i * imgOffset + j] == 0 ){
        POINT p; p.x = j; p.y = i;
        pointsList.push_back(p);
      }
    }
  }

  return pointsList;
}

POINT getRandomPoint( pointVector pointsList ) {
  int random = rand() % pointsList.size();
  return pointsList.at(random);
}