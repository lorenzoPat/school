#ifndef _COMMON_H_
#define _COMMON_H_

#include <vector>

#define pointVector std::vector<POINT>
#define intMatrix2D std::vector<std::vector<int>>
#define intVector std::vector<int>

#define PI 3.141592

pointVector   getListOfPoints( BYTE* sourceImage, int imgHeight, int imgWidth, int imgOffset );
POINT         getRandomPoint( pointVector pointsList );


#endif