#include "stdafx.h"
#include "lab1.h"

int           getInliers( pointVector pointsList, POINT p1, POINT p2, int threshold );
void          getLineParameters( POINT p1, POINT p2, int& a, int& b, int& c );
void          getLineWithMaximumInliers( pointVector pointsList, int iterationsNr, int threshold, POINT& p1, POINT& p2 );


void computeRANSACLine( BYTE* sourceImage
                       , int imgHeight
                       , int imgWidth
                       , int imgOffset
                       , POINT& start
                       , POINT& end) {
  
  pointVector pointsList = getListOfPoints(sourceImage, imgHeight, imgWidth, imgOffset);
  int threshold = 10;
	float probability = 0.5;
	int iterationsNr = log(1-probability)/ log(1 - 0.3 * 0.3);
	
  int a,b,c;
  POINT p1,p2;

  getLineWithMaximumInliers( pointsList, iterationsNr, threshold, p1, p2 );
  getLineParameters( p1, p2, a, b, c );

  start.x = 0;
	start.y = -c / b;
	end.x = imgWidth;
	end.y = (-a * imgWidth - c) / b;
}


void getLineParameters( POINT p1, POINT p2, int& a, int& b, int& c ) {
  a = p1.y - p2.y;
  b = p2.x - p1.x;
  c = p1.x*p2.y - p2.x*p1.y;
}

int getInliers( pointVector pointsList, POINT p1, POINT p2, int threshold ) {
  int a,b,c;
  getLineParameters(p1, p2, a, b, c);
  
  int inliers = 0;
		for (int i = 0; i < pointsList.size(); i++){
			POINT pt = pointsList.at(i);
			float distance = abs(a* pt.x + b * pt.y + c) / sqrt((double)a *a + b * b);
			if (threshold > distance) inliers++;
		}
  
  return inliers;
}

void getLineWithMaximumInliers( pointVector pointsList, int iterationsNr, int threshold, POINT& p1, POINT& p2 ) {
  int max = 0;
  
  for (int i = 0; i < iterationsNr; i++){
    POINT randPt1 = getRandomPoint( pointsList );
    POINT randPt2 = getRandomPoint( pointsList );
    
    int inliers = getInliers( pointsList, randPt1, randPt2, threshold );

		if (inliers >= max){
			max = inliers;
			p1 = randPt1;
			p2 = randPt2;
		}
	}

}