#ifndef _LAB1_H_
#define _LAB1_H_

#include "common.h"


// Gets the imput image
// Returns 2 points representing the start and the end of the line that fits to a set of 2D points
void computeRANSACLine( BYTE* sourceImage
                       , int imgHeight
                       , int imgWidth
                       , int imgOffset
                       , POINT& start
                       , POINT& end );

#endif