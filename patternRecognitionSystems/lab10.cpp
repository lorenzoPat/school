#include "stdafx.h"
#include "lab10.h"
#include <vector>

void computeMeanVector(std::vector<POINT> points, double meanVector[2]) {
  double xFeatureSum = 0;
  double yFeatureSum = 0;

  for (std::vector<POINT>::iterator it = points.begin(); it != points.end(); ++it) {
    xFeatureSum += it->x;
    yFeatureSum += it->y;
  }

  meanVector[0] = xFeatureSum / points.size();
  meanVector[1] = yFeatureSum / points.size();
}

void computeScatter(std::vector<POINT> points, double meanVector[2], double scatter[2][2]) {
  float scatterSum[2][2] = { {0,0}, {0,0} };
  
  for (std::vector<POINT>::iterator it = points.begin(); it != points.end(); ++it) {
    double diffVec[2] = {it->x - meanVector[0], it->y - meanVector[1]};

    //multiply diffVec with its transpose
    /*             
      |a|           |a * a   a * b|
      | | * |a b| = |             |
      |b|           |b * a   b * b|
    */
    scatterSum[0][0] += diffVec[0] * diffVec[0]; 
    scatterSum[0][1] += diffVec[0] * diffVec[1]; 
    scatterSum[1][0] += diffVec[1] * diffVec[0]; 
    scatterSum[1][1] += diffVec[1] * diffVec[1]; 
  }

  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 2; j++)
      scatter[i][j] = scatterSum[i][j] / points.size();
}

void computeBetweenClassScatter(double meanVec1[2], double meanVec2[2], double betweenScatter[2][2]) {
  double diffVec[2] = {meanVec1[0] - meanVec2[0], meanVec1[1] - meanVec2[1]};

  //multiply diffVec with its transpose
  betweenScatter[0][0] = diffVec[0] * diffVec[0];
  betweenScatter[0][1] = diffVec[0] * diffVec[1];
  betweenScatter[1][0] = diffVec[1] * diffVec[0];
  betweenScatter[1][1] = diffVec[1] * diffVec[1];
}

void computeWithinClassScatter(double scatter1[2][2], double scatter2[2][2], double withinScatter[2][2]) {
  for (int i = 0; i < 2; ++i)
    for (int j = 0; j < 2; ++j) 
      withinScatter[i][j] = scatter1[i][j] + scatter2[i][j];
}

// computes inverse of a matrix using exlicit formula for 2x2 matrix http://mathworld.wolfram.com/MatrixInverse.html
void getInverseMatrix(double inputMatr[2][2], double inverseMatr[2][2]) {
  double determinant = inputMatr[0][0] * inputMatr[1][1] - inputMatr[0][1] * inputMatr[1][0];

  inverseMatr[0][0] = inputMatr[1][1] / determinant;
  inverseMatr[0][1] = -inputMatr[0][1] / determinant;
  inverseMatr[1][0] = -inputMatr[1][0] / determinant;
  inverseMatr[1][1] = inputMatr[0][0] / determinant;
}

void computeProjectionVector(double meanVec1[2], double meanVec2[2], double withinScatter[2][2], double projectionVec[2]) {
  double scatterInverse[2][2];
  double diffVec[2] = {meanVec1[0] - meanVec2[0], meanVec1[1] - meanVec2[1]};
  getInverseMatrix(withinScatter, scatterInverse);

  projectionVec[0] = scatterInverse[0][0] * diffVec[0] + scatterInverse[0][1] * diffVec[1];
  projectionVec[1] = scatterInverse[1][0] * diffVec[0] + scatterInverse[1][1] * diffVec[1];
}

void getClassVectors(BYTE* sourceImage
                         , int imgHeight
                         , int imgWidth
                         , int imgOffset
                         , std::vector<POINT>& redPoints 
                         , std::vector<POINT>& bluePoints) {
                        
  // put points into two vectors
  for (int i = 0; i < imgHeight; i++) {
		for (int j = 0; j < imgWidth; j++) {	
      if (sourceImage[i * imgOffset + j] == 1) {
        POINT redPt;
        redPt.x = j; redPt.y = i;
        redPoints.push_back(redPt);
      }
      
      if (sourceImage[i * imgOffset + j] == 2) {
        POINT bluePt;
        bluePt.x = j; bluePt.y = i;
        bluePoints.push_back(bluePt);
      }
	  }
  }
}

void getDiscriminantPoints(double projectionVector[2], int imgWidth, POINT& p1, POINT& p2) {
  if (projectionVector[1]/projectionVector[0] >= 0) {
    p1.x = 0;
    p1.y = 0;

    p2.x = imgWidth;
    p2.y = projectionVector[1]/projectionVector[0] * p2.x;
  } else {
    p1.x = imgWidth;
    p1.y = 0;

    p2.x = 0;
    p2.y = projectionVector[1]/projectionVector[0] * (-imgWidth);
  }
}


void getLinearDiscriminant(BYTE* sourceImage
                         , int imgHeight
                         , int imgWidth
                         , int imgOffset
                         , POINT& p1
                         , POINT& p2) {
  
  std::vector<POINT> redPoints;
  std::vector<POINT> bluePoints;
  getClassVectors(sourceImage, imgHeight, imgWidth, imgOffset, redPoints, bluePoints);

  double meanVector1[2], meanVector2[2];
  computeMeanVector(redPoints, meanVector1);
  computeMeanVector(bluePoints, meanVector2);

  double scatter1[2][2], scatter2[2][2];
  computeScatter(redPoints, meanVector1, scatter1);
  computeScatter(bluePoints, meanVector2, scatter2);

  double betweenScatter[2][2], withinScatter[2][2];
  computeBetweenClassScatter(meanVector1, meanVector2, betweenScatter);
  computeWithinClassScatter(scatter1, scatter2, withinScatter);

  double projectionVector[2];
  computeProjectionVector(meanVector1, meanVector2, withinScatter, projectionVector);

  getDiscriminantPoints(projectionVector, imgWidth, p1, p2);
}