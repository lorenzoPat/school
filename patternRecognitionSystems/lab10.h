#ifndef _LAB10_H_
#define _LAB10_H_

// gets the input image and returns two points that describe the linear discriminant
void getLinearDiscriminant(BYTE* sourceImage
                         , int imgHeight
                         , int imgWidth
                         , int imgOffset
                         , POINT& p1
                         , POINT& p2);

#endif