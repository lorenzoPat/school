#include "stdafx.h"
#include "lab11.h"
#include <vector>

using namespace std;

// 1 -> red class, -1 -> blue class
void buildTrainingSet(BYTE* sourceImage, int imgHeight, int imgWidth, int imgOffset, vector<AdaBoostSample>& trainingSet) {
  for (int i = 0; i < imgHeight; i++) {
    for (int j = 0; j < imgWidth; j++) {	
      if (sourceImage[i * imgOffset + j] == 0) { // red point
        POINT samplePt;
        samplePt.x = j; samplePt.y = i;
        AdaBoostSample sample = { samplePt, 1, 0, 0};
        trainingSet.push_back(sample);
      }

      if (sourceImage[i * imgOffset + j] == 2) { // blue point
        POINT samplePt;
        samplePt.x = j; samplePt.y = i;
        AdaBoostSample sample = { samplePt, -1, 0, 0};
        trainingSet.push_back(sample);
      }
    }
  }
}

void normalizeWeights(vector<AdaBoostSample>& trainingSet) {
  // compute denominator
  float denominator = 0;
  for (vector<AdaBoostSample>::iterator it = trainingSet.begin(); it != trainingSet.end(); ++it) {
    denominator += it->weight;
  }

  // normalize weights
  for (vector<AdaBoostSample>::iterator it = trainingSet.begin(); it != trainingSet.end(); ++it) {
    it->weight /= denominator;
  }
}

int getWeakClassifier(POINT currentPoint, int feature, int parity, int threshold) {
  if (feature == 1) { // compare on x axis
    return (parity * currentPoint.x < parity * threshold) ? 1 : -1;
  } else {  // compare on y axis
    return (parity * currentPoint.y < parity * threshold) ? 1 : -1;
  }
}

void computeWeakClassifier(vector<AdaBoostSample>& trainingSet, int feature, int parity, double& classifierError, int& threshold) {
  classifierError = 10;

  if (feature == 1) { // x axis
    // consider the x of each point as being the threshold
    for (auto thresholdIterator = trainingSet.begin(); thresholdIterator != trainingSet.end(); ++thresholdIterator) {
      int localThreshold = thresholdIterator->p.x;
    
      // classify each point based on threshold
      for (auto pointsIterator = trainingSet.begin(); pointsIterator != trainingSet.end(); ++pointsIterator) {
        pointsIterator->adaClassification = getWeakClassifier(pointsIterator->p, feature, parity, localThreshold);
      }

      double localError = 0;
      for (auto pointsIterator = trainingSet.begin(); pointsIterator != trainingSet.end(); ++pointsIterator) {
        if (pointsIterator->adaClassification != pointsIterator->group) {
          localError += pointsIterator->weight;
        }
      }

      // keep this threshold if localr error is the smallest
      if (localError < classifierError) {
        classifierError = localError;
        threshold = localThreshold;
      }
    }
  } else { // y axis
    // consider the x of each point as being the threshold
    for (auto thresholdIterator = trainingSet.begin(); thresholdIterator != trainingSet.end(); ++thresholdIterator) {
      int localThreshold = thresholdIterator->p.y;
    
      // classify each point based on threshold
      for (auto pointsIterator = trainingSet.begin(); pointsIterator != trainingSet.end(); ++pointsIterator) {
        pointsIterator->adaClassification = getWeakClassifier(pointsIterator->p, feature, parity, localThreshold);
      }

      double localError = 0;
      for (auto pointsIterator = trainingSet.begin(); pointsIterator != trainingSet.end(); ++pointsIterator) {
        if (pointsIterator->adaClassification != pointsIterator->group) {
          localError += pointsIterator->weight;
        }
      }

      // keep this threshold if localr error is the smallest
      if (localError < classifierError) {
        classifierError = localError;
        threshold = localThreshold;
      }
    }
  }

}

int getBestWeakClassifier(double weakClassifierError[4], int threshold[4], int& finalClass, double& finalError, int& finalThreshold, int& finalParity) {
  finalError = 10;
  int ret;
  for (int i = 0; i < 4; ++i) {
    if (weakClassifierError[i] < finalError) {
      finalError = weakClassifierError[i];
      finalThreshold = threshold[i];
      finalClass = (i < 2) ? 1 : -1; // first two belong to x pass, last two to y pass
      finalParity = (i % 2 == 0) ? 1 : -1;
      ret = i;
    }
  }
  return ret;
}

void computeAdaBoost(BYTE* sourceImage
                    ,BYTE* destinationImage
                    , int imgHeight
                    , int imgWidth
                    , int imgOffset) {

  vector<AdaBoostSample> trainingSet;

  buildTrainingSet(sourceImage, imgHeight, imgWidth, imgOffset, trainingSet);  
  
  // initialize weights for first iteration
  for (vector<AdaBoostSample>::iterator it = trainingSet.begin(); it != trainingSet.end(); ++it) {
    it->weight = 1/(double)trainingSet.size();
  }

  vector<AdaResult> result;
  int iterationsNr = 5;
  
  for (int i = 0; i < iterationsNr; ++i) {
    normalizeWeights(trainingSet);
  
    int weakClassifier[2] = {-1, 1};            // -1 -> go in x axis; 1 -> go on y axis
    double weakClassifierError[4] = {0,0,0,0};  // holds the classifier error for each pass
    int threshold[4] = {0,0,0,0};               // holds the threshold for each pass
    vector<AdaBoostSample> trainingSet0(trainingSet), trainingSet1(trainingSet), trainingSet2(trainingSet), trainingSet3(trainingSet);
 
    // compute weak classifier
    computeWeakClassifier(trainingSet0, 1, 1, weakClassifierError[0], threshold[0]);    // x axis left red, right blue
    computeWeakClassifier(trainingSet1, 1, -1, weakClassifierError[1], threshold[1]);   // x axis right red, left blue
    computeWeakClassifier(trainingSet2, -1, 1, weakClassifierError[2], threshold[2]);   // y axis up red, down blue
    computeWeakClassifier(trainingSet3, -1, -1, weakClassifierError[3], threshold[3]);  // y axis up blue, down red

    double finalError = 0;
    int finalThreshold = 0;
    int finalClass;
    int finalParity;
    int index = getBestWeakClassifier(weakClassifierError, threshold, finalClass, finalError, finalThreshold, finalParity);  
    
    if (finalError >= 0.5)   break;

    double beta = finalError / (1.0 - finalError);
    double alpha = 0.5 * log(1.0/beta);

    // assign to original training set the training set for which the error was minimum
    if (index == 0) trainingSet = trainingSet0;
    if (index == 1) trainingSet = trainingSet1;
    if (index == 2) trainingSet = trainingSet2;
    if (index == 3) trainingSet = trainingSet3;

    // lower the weight of the points that are classified correctly beta times 
    for (vector<AdaBoostSample>::iterator it = trainingSet.begin(); it != trainingSet.end(); ++it) {
      if (it->group == it->adaClassification) { // correctly classified
        it->weight *= beta; 
      }
    }

    AdaResult tempResult = {finalClass, finalThreshold, finalParity, alpha};
    result.push_back(tempResult);

  }

  vector<vector<double>> bla(imgHeight);
  for (int i = 0; i < imgHeight; ++i) 
    bla[i].resize(imgWidth);

  for (int i = 0; i < iterationsNr; ++i) {
    double alpha = result.at(i).alpha;
    int feature = result.at(i).classifier;
    int parity = result.at(i).parity;
    int threshold = result.at(i).threshold;

    for (int i = 0; i < imgHeight; i++) {
      for (int j = 0; j < imgWidth; j++) {	
        POINT p;
        p.x = j; p.y = i;
        bla[i][j] += (alpha * getWeakClassifier(p, feature, parity, threshold));  
      }
    }
  }

  // draw result
  for (vector<AdaResult>::iterator it = result.begin(); it != result.end(); ++it) {
    for (int i = 0; i < imgHeight; i++) {
      for (int j = 0; j < imgWidth; j++) {	
        if ( bla[i][j] < 0 )
          destinationImage[i * imgOffset + j]= 3;
        else
          destinationImage[i * imgOffset + j]= 4;
        
      }
    }
  }

  // draw again the initial points
  for (auto it = trainingSet.begin(); it != trainingSet.end(); ++it ) {
    if (it->group == -1) {
      destinationImage[it->p.y * imgOffset + it->p.x] = 2;
    } else {
      destinationImage[it->p.y * imgOffset + it->p.x] = 0;
    }
  }
}