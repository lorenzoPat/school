#ifndef _LAB11_H_
#define _LAB11_H_

typedef struct _AdaBoostSample {
  POINT   p;
  int     group;
  double  weight;
  int     adaClassification;
} AdaBoostSample;

typedef struct _AdaResult {
  int classifier;
  int threshold;
  int parity;
  double alpha;
} AdaResult;

void computeAdaBoost(BYTE* sourceImage
                    ,BYTE* destinationImage
                    , int imgHeight
                    , int imgWidth
                    , int imgOffset);

#endif