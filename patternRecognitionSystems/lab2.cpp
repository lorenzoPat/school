#include "stdafx.h"
#include "lab2.h"

void          getCircleWithMaximumInliers( pointVector pointsList, int iterationsNr, int threshold, POINT& center, int& radius );
void          getRandomCircle( pointVector pointsList, POINT& center, int& radius );
float         getSlope( POINT p1, POINT p2 );
int           getInliers( pointVector pointsList, POINT center, int radius, int threshold );


void computeRANSACCircle( BYTE* sourceImage
                        , int imgHeight
                        , int imgWidth
                        , int imgOffset
                        , POINT& center
                        , int& radius ) {

  pointVector pointsList = getListOfPoints(sourceImage, imgHeight, imgWidth, imgOffset);
  int threshold = 10;
	float probability = 0.99;
	float weight = 0.5;
	int iterationsNr = log(1-probability)/ log(1 - weight * weight * weight);//nr of samples
  POINT circleCenter;
  int circleRadius;

  getCircleWithMaximumInliers( pointsList, iterationsNr, threshold, circleCenter, circleRadius );
	
  center.x = circleCenter.x;
  center.y = circleCenter.y;
  radius = circleRadius;

}



void getCircleWithMaximumInliers( pointVector pointsList, int iterationsNr, int threshold, POINT& center, int& radius ) {
  POINT centerTmp;
  int radiusTmp;
  int max = 0;

  for (int i = 0; i < iterationsNr; i++){
    getRandomCircle( pointsList, centerTmp, radiusTmp);
    int inliers = getInliers( pointsList, centerTmp, radiusTmp, threshold );

    if (inliers > max){
      max = inliers;
      center = centerTmp;
      radius = radiusTmp;
    }	

  }
}

void getRandomCircle( pointVector pointsList, POINT& center, int& radius ) {
  POINT p1 = getRandomPoint( pointsList );
  POINT p2 = getRandomPoint( pointsList );
  POINT p3 = getRandomPoint( pointsList );

  float slopeA = getSlope( p1, p2 );
  float slopeB = getSlope( p2, p3 );

  center.x = ( ( slopeA * slopeB * (p1.y - p3.y) ) + 
               ( slopeB * (p1.x + p2.x) ) - 
               ( slopeA * (p2.x + p3.x) ) ) / 
             ( 2 * (slopeB - slopeA) );
  center.y = ( (-1/slopeA) * ( center.x - (p1.x + p2.x)/2 ) ) + 
             ( (p1.y + p2.y) / 2 ) ;
  radius = sqrt((float) (center.x - p1.x) * (center.x - p1.x) + 
                        (center.y - p1.y) * (center.y - p1.y) );
}

int getInliers( pointVector pointsList, POINT center, int radius, int threshold ) {
  int inliers = 0;
  for (int i = 0; i < pointsList.size(); i++){
    POINT pt = pointsList.at(i);
    float distance = abs( sqrt( (float) (center.x - pt.x) * (center.x - pt.x) + (center.y - pt.y) * (center.y - pt.y) ) - 
                          radius );

    if (threshold > distance) inliers++;
  }

  return inliers;
}

float         getSlope( POINT p1, POINT p2 ) {
  return (p2.y - p1.y) / (float)(p2.x - p1.x);
}

