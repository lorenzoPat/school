#ifndef _LAB2_H_
#define _LAB2_H_

#include "common.h"
#include <vector>

// Gets the input image
// Returns the center point and the radius of the circle that fits to a set of 2D points
void computeRANSACCircle( BYTE* sourceImage
                        , int imgHeight
                        , int imgWidth
                        , int imgOffset
                        , POINT& center
                        , int& radius );


#endif