#include "stdafx.h"
#include "lab3.h"

int** houghMatrix;

void initializeHoughMatrix(int pMax, int thetaMax);
void computeHoughMatrix( pointVector pointsList, int pMin, int pMax, int thetaMin, int thetaMax );
int getMaxFromHough( int imgHeight, int imgWidth );

void computeHoughMatrix( BYTE* sourceImage
                       , int imgHeight
                       , int imgWidth
                       , int imgOffset ) {

  int pMax = (int)sqrt((float) (imgHeight * imgHeight + imgWidth * imgWidth) ); 
  int pMin = 0;
	int thetaMax = 360;
	int thetaMin = 0;
	int deltaTheta = 1;
	int deltaP = 1;

  pointVector pointsList = getListOfPoints( sourceImage, imgHeight, imgWidth, imgOffset );

  initializeHoughMatrix( pMax, thetaMax );
  computeHoughMatrix( pointsList, pMin, pMax, thetaMin, thetaMax );
 
}



void showHoughMatrix( BYTE* destinationImage
                       , int imgHeight
                       , int imgWidth
                       , int imgOffset ) {
                         
  int max = getMaxFromHough( imgHeight, imgWidth );

  for (int i=0; i<imgHeight -1; i++){
    for (int j=0; j<imgWidth -1; j++) {
      destinationImage[i*imgOffset+j] = (houghMatrix[i][j] * 255)/max;
    }
  }
}


void initializeHoughMatrix(int pMax, int thetaMax) {
	houghMatrix = new int*[pMax];
	for(int i = 0; i < pMax; i++)
    houghMatrix[i] = new int[thetaMax];
	
	for (int i = 0; i < pMax; i++) {
		for (int j = 0; j < thetaMax; j++) {
			houghMatrix[i][j] = 0;
		}
	}
}

void computeHoughMatrix( pointVector pointsList, int pMin, int pMax, int thetaMin, int thetaMax ) {
  int size = pointsList.size();

	//for each interest point compute the hough matrix
	for (int i = 0; i < size; i++) {
		POINT point = pointsList.at(i);

		for (int theta = thetaMin; theta < thetaMax; theta++){
			int p =(int) point.x * cos(theta * PI / 180.0) + point.y * sin(theta * PI / 180.0);
			if ( (p > pMin) && (p < pMax) )
				houghMatrix[p][theta]++;
		}
	}
}

int getMaxFromHough( int imgHeight, int imgWidth ) {
  int max = 0;

  for (int i=0; i<imgHeight -1; i++)
    for (int j=0; j<imgWidth -1; j++)
      if ( houghMatrix[i][j] > max )
        max = houghMatrix[i][j];

  return max;
}