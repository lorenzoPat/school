#ifndef _LAB3_H_
#define _LAB3_H_

#include "common.h"

// Computes the Hough matrix for the input image
void computeHoughMatrix( BYTE* sourceImage
                       , int imgHeight
                       , int imgWidth
                       , int imgOffset );

// Displays the hough matrix
void showHoughMatrix( BYTE* destinationImage
                       , int imgHeight
                       , int imgWidth
                       , int imgOffset );

#endif