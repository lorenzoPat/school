#include "stdafx.h"
#include "lab4.h"

int** chamferBuffer;

void  initializeChamferBuffer( BYTE* sourceImage, int imgHeight, int imgWidth, int imgOffset );
void  createResultImage( BYTE* destinationImage, int imgHeight, int imgWidth, int imgOffset );
void  scanTopDownLeftRight( int imgHeight, int imgWidth, int kernel[3][3] );
void  scanBottomUpRightLeft(int imgHeight, int imgWidth, int kernel[3][3]);


void computeChamferDistanceTransform( BYTE* sourceImage
                                    , BYTE* destinationImage
                                    , int imgHeight
                                    , int imgWidth
                                    , int imgOffset ) {

  int wHV = 2;
  int wD = 3;
  
  //1. initialize the DT buffer: if (i,j) belong to object DT(i,j) = 0; else DT(i,j) = 255
  initializeChamferBuffer( sourceImage, imgHeight, imgWidth, imgOffset );
  
  //2. initialize the kernel
  int kernel[3][3] = { { wD , wHV, wD }, 
                       { wHV,  0 , wHV}, 
                       { wD , wHV, wD } };

  //3. scan the image
  scanTopDownLeftRight( imgHeight, imgWidth, kernel );
  scanBottomUpRightLeft( imgHeight, imgWidth, kernel );
  createResultImage( destinationImage, imgHeight, imgWidth, imgOffset );
}

int patternMatching( BYTE* sourceImage
                       , int imgHeight
                       , int imgWidth
                       , int imgOffset) {

  int sum=0, n=0;

	//pass the new image
	for ( int i = 0; i < imgHeight; i++ ){
		for ( int j = 0; j < imgWidth; j++ ) {
      if ( sourceImage[i * imgOffset + j] == 0 ){
				n++;
				sum += chamferBuffer[i][j];
			}
		}
	}	

	return sum / n;
}


void initializeChamferBuffer( BYTE* sourceImage, int imgHeight, int imgWidth, int imgOffset ) {
  chamferBuffer = new int*[imgHeight];

	for(int i = 0; i < imgHeight; i++)
    chamferBuffer[i] = new int[imgWidth];

	for ( int i = 0; i < imgHeight; i++ ) {
		for ( int j = 0; j < imgWidth; j++ ) {
      chamferBuffer[i][j] = sourceImage[i * imgOffset + j];
		}
	}
}

void createResultImage( BYTE* destinationImage, int imgHeight, int imgWidth, int imgOffset ) {
  for ( int i = 0; i < imgHeight; i++ ) {
		for ( int j = 0; j < imgWidth; j++ ) {
      destinationImage[i * imgOffset + j] = chamferBuffer[i][j];
		}
	}
}

void scanTopDownLeftRight( int imgHeight, int imgWidth, int kernel[3][3] ) {
  for ( int i = imgHeight-2; i > 1; i-- ) {
		for ( int j = 1; j < imgWidth-1; j++ ) {
      int DTmin = 255;//init the min with max possible value
      int localDT;

      for ( int k = -1; k <= 1; k++ ) {
        localDT = chamferBuffer[i+1][j] + kernel[0][1 + k];
        DTmin = ( localDT < DTmin ) ? localDT : DTmin;
      }

      localDT = chamferBuffer[i][j-1] + kernel[1][0];
      DTmin = ( localDT < DTmin ) ? localDT : DTmin;

      chamferBuffer[i][j] = (chamferBuffer[i][j] < DTmin) ? chamferBuffer[i][j] : DTmin;
    }
  }
}

void scanBottomUpRightLeft(int imgHeight, int imgWidth, int kernel[3][3]) {
  for ( int i = 1; i < imgHeight-1; i++ ) {
		for ( int j = imgWidth-2; j > 1; j-- ) {
      int DTmin = 255;//init the min with max possible value
      int localDT;

      for ( int k = -1; k <= 1; k++ ) {
        localDT = chamferBuffer[i-1][j+k] + kernel[2][1+k];
        DTmin = ( localDT < DTmin ) ? localDT : DTmin;
      }

      localDT = chamferBuffer[i][j+1] + kernel[1][2];
      DTmin = ( localDT < DTmin ) ? localDT : DTmin;
      
      chamferBuffer[i][j] = (chamferBuffer[i][j] < DTmin) ? chamferBuffer[i][j] : DTmin;
    }
  }
}