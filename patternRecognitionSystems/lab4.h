#ifndef _LAB4_H_
#define _LAB4_H_

#include "common.h"

void computeChamferDistanceTransform( BYTE* sourceImage
                                    , BYTE* destinationImage
                                    , int imgHeight
                                    , int imgWidth
                                    , int imgOffset );

// Performs the pattern matching between two images returning a number indicating how much the two
// images look alike. The smaller the number, the bigger is the chance that the two images look alike
int patternMatching( BYTE* sourceImage
                       , int imgHeight
                       , int imgWidth
                       , int imgOffset);

#endif