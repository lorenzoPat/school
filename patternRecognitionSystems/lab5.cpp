#include "stdafx.h"
#include "lab5.h"

intMatrix2D magnitude, orientation, HOGFeatures; 

void  init2DMatrix( intMatrix2D& matrix, int imgHeight, int imgWidth );
void  computeDerivatives( BYTE* sourceImage, int imgHeight, int imgWidth, int imgOffset, intMatrix2D& xDerivative, intMatrix2D& yDerivative );
void  computeMagnitude( intMatrix2D& xDerivative, intMatrix2D& yDerivative, int imgHeight, int imgWidth );
void  computeOrientation( intMatrix2D& xDerivative, intMatrix2D& yDerivative, int imgHeight, int imgWidth );
int   getMaxOrientation( int* orientations, int binsNr );
int   getOrientationForRegion( int i, int j, int binsNr, int cellHeight, int cellWidth, int* orientations );
void  showRegion( BYTE* destinationImage, int i, int j, int cellHeight, int cellWidth, int imgOffset, int feature ) ;
void  showMagnitude( BYTE* destinationImage, int imgHeight, int imgWidth, int imgOffset);
void  showOrientation( BYTE* destinationImage, int imgHeight, int imgWidth, int imgOffset);


void computeGradientMagnitude( BYTE* sourceImage
                             , BYTE* destinationImage
                             , int imgHeight                                                       
                             , int imgWidth
                             , int imgOffset ) {

  intMatrix2D xDerivative, yDerivative;
  
  init2DMatrix( xDerivative, imgHeight, imgWidth );
  init2DMatrix( yDerivative, imgHeight, imgWidth );
  init2DMatrix( magnitude, imgHeight, imgWidth );
  
  computeDerivatives( sourceImage, imgHeight, imgWidth, imgOffset, xDerivative, yDerivative );
  computeMagnitude( xDerivative, yDerivative, imgHeight, imgWidth );

  showMagnitude( destinationImage, imgHeight, imgWidth, imgOffset );
}

void computeGradientOrientation( BYTE* sourceImage
                               , BYTE* destinationImage
                               , int imgHeight
                               , int imgWidth
                               , int imgOffset ) {
  
  intMatrix2D xDerivative, yDerivative;
  
  init2DMatrix( xDerivative, imgHeight, imgWidth );
  init2DMatrix( yDerivative, imgHeight, imgWidth );
  init2DMatrix( orientation, imgHeight, imgWidth );
  
  computeDerivatives( sourceImage, imgHeight, imgWidth, imgOffset, xDerivative, yDerivative );
  computeOrientation( xDerivative, yDerivative, imgHeight, imgWidth );
  
  showOrientation( destinationImage, imgHeight, imgWidth, imgOffset );
}

void computeHOGFeatures( BYTE* sourceImage
                       , BYTE* destinationImage
                       , int imgHeight
                       , int imgWidth
                       , int imgOffset
                       , int binsNr
                       , int cellHeight
                       , int cellWidth ) {

  int* orientations = new int[binsNr];
  int heightCells = imgHeight / cellHeight;  
  int widthCells = imgWidth / cellWidth;
  init2DMatrix( HOGFeatures, heightCells, widthCells );
  
  //go through each region of the image
  for ( int i = 0; i < heightCells; i++ ) {
    for ( int j = 0 ; j < widthCells; j++ ) {
      
      //clear orientations
      for (int x =0 ; x < 9; x++)
        orientations[x] = 0;

      int feature = getOrientationForRegion( i, j, binsNr, cellHeight, cellWidth, orientations );
      showRegion( destinationImage, i, j, cellHeight, cellWidth, imgOffset, feature );
      
      HOGFeatures[i][j] = feature;
    }
  }
  
  delete orientations;
}


void init2DMatrix( intMatrix2D& matrix, int imgHeight, int imgWidth ) {
  matrix.resize(imgHeight);
  
  for (int i = 0; i < imgHeight; i++)
    matrix[i].resize(imgWidth);
}

void computeDerivatives( BYTE* sourceImage, int imgHeight, int imgWidth, int imgOffset, intMatrix2D& xDerivative, intMatrix2D& yDerivative ) {
  for ( int i = 1; i < imgHeight-1; i++ ) {
		for ( int j = 1; j < imgWidth-1; j++ ) {
      xDerivative[i][j] = sourceImage[i * imgOffset + j+1] - sourceImage[i * imgOffset + j-1];     
      yDerivative[i][j] = sourceImage[(i+1) * imgOffset + j] - sourceImage[(i-1) * imgOffset + j];
		}
	}
}

void computeMagnitude( intMatrix2D& xDerivative, intMatrix2D& yDerivative, int imgHeight, int imgWidth ) {
  for ( int i = 1; i < imgHeight-1; i++ ) {
		for ( int j = 1; j < imgWidth-1; j++ ) {
      magnitude[i][j] = sqrt( (double) xDerivative[i][j] * xDerivative[i][j] +
                              yDerivative[i][j] * yDerivative[i][j] );
    }
	}
}

void computeOrientation( intMatrix2D& xDerivative, intMatrix2D& yDerivative, int imgHeight, int imgWidth ) {
  for ( int i = 1; i < imgHeight-1; i++ ) {
		for ( int j = 1; j < imgWidth-1; j++ ) {
      float alpha = atan2(yDerivative[i][j], xDerivative[i][j]) * 180.0/PI;
      orientation[i][j] = ( alpha >= 0 ) ? alpha : alpha + 360;
    }
	}
}

void showMagnitude( BYTE* destinationImage, int imgHeight, int imgWidth, int imgOffset) {
  for ( int i = 1; i < imgHeight-1; i++ ) {
		for ( int j = 1; j < imgWidth-1; j++ ) {
      destinationImage[i * imgOffset + j] = magnitude[i][j];
		}
	}
}

void showOrientation( BYTE* destinationImage, int imgHeight, int imgWidth, int imgOffset) {
  for ( int i = 1; i < imgHeight-1; i++ ) {
		for ( int j = 1; j < imgWidth-1; j++ ) {
      destinationImage[i * imgOffset + j] = orientation[i][j] / 40;
		}
	}
}

int getMaxOrientation( int* orientations, int binsNr ) {
  int max = 0, value = 0;
  for ( int k = 0 ; k < binsNr; k++) {
    if (orientations[k] > max) {
      max = orientations[k];
      value = k;
    }
  }

  return value;
}

int getOrientationForRegion( int i, int j, int binsNr, int cellHeight, int cellWidth, int* orientations ) {
  // get value of each orientation
  for ( int ii = 0; ii < cellHeight; ii++ ) {
    for ( int jj = 0; jj < cellWidth; jj++ ) {
      int localI = ii + i*cellHeight;
      int localJ = jj + j*cellWidth;
      int beanValue = orientation[localI][localJ] / 40; 
      orientations[beanValue]++;
    }
  }

  return getMaxOrientation( orientations, binsNr );
}

void showRegion( BYTE* destinationImage, int i, int j, int cellHeight, int cellWidth, int imgOffset, int feature ) {
  for ( int ii = 0; ii < cellHeight; ii++ ) {
    for ( int jj = 0; jj < cellWidth; jj++ ) {
      int localI = ii + i*cellHeight;
      int localJ = jj + j*cellWidth;
      destinationImage[ localI * imgOffset + localJ ] = feature; 
    }
  }
}
