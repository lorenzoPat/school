#ifndef _LAB5_H_
#define _LAB5_H_

#include "common.h"

void computeGradientMagnitude( BYTE* sourceImage
                             , BYTE* destinationImage
                             , int imgHeight
                             , int imgWidth
                             , int imgOffset );

void computeGradientOrientation( BYTE* sourceImage
                               , BYTE* destinationImage
                               , int imgHeight
                               , int imgWidth
                               , int imgOffset );

void computeHOGFeatures( BYTE* sourceImage
                       , BYTE* destinationImage
                       , int imgHeight
                       , int imgWidth
                       , int imgOffset
                       , int binsNr
                       , int cellHeight
                       , int cellWidth );

#endif