#include "stdafx.h"
#include "lab6.h"
#include <fstream>

BYTE** lab6Images;
int imgHeight;
int imgWidth;
int imgW;
int imgNr;
float** covarianceMatrix;
float** corelationMatrix;


void initializeImagesVector() {
  lab6Images = new BYTE*[400];
  for (int i = 0; i < 400; i++)
    lab6Images[i] = new BYTE;
}

void initCovarianceMatrix() {
  covarianceMatrix = new float*[imgHeight * imgWidth];
  for ( int i = 0; i < imgHeight * imgWidth; i++ )
    covarianceMatrix[i] = new float[imgHeight * imgWidth];
}

void initCorelationMatrix() {
  corelationMatrix = new float*[imgHeight * imgWidth];
  for ( int i = 0; i < imgHeight * imgWidth; i++ )
    corelationMatrix[i] = new float[imgHeight * imgWidth];
}

void computeMeanValue(float* meanValue) {
  float sum = 0;
  int offset = imgWidth;
  
  for ( int i = 0; i < imgHeight; i++ ) {
    for ( int j = 0 ; j < imgWidth; j++ ) {
      sum = 0;
      
      for ( int img = 0; img < imgNr; img++ )
        sum += lab6Images[img][i * imgW + j];
      
      float mean = sum /(float)imgNr;
      meanValue[i * offset + j] = mean;
    }
  }
}

void computeCovarianceMatrix( float* meanValue ) {
  float cov = 0;
  
  for ( int i = 0; i < imgHeight * imgWidth; i++ ) {
    for ( int j = 0; j < imgHeight * imgWidth; j++ ) {

      //compute the sum of feature between every 2 images
      int sum = 0;
      for ( int img = 0; img < imgNr; img++ ) {
        float firstTerm = lab6Images[img][i] - meanValue[i];
        float secondTerm = lab6Images[img][j] - meanValue[j];
        float prod = firstTerm * secondTerm;
        sum += prod;
      }
      
      covarianceMatrix[i][j] = sum / (float)imgNr;
    }
  }
}

void printCovarianceMatrix() {
  std::ofstream covariance("covariance.txt"); 

  for ( int i = 0; i < imgHeight * imgWidth; i++ ) {
    covariance << std::endl;
    for ( int j = 0; j < imgHeight * imgWidth; j++ ) {
      covariance << covarianceMatrix[i][j] << " ";
    }
  }

  covariance.close();
}

void computeCorelationMatrix() {
  float corel = 0;
  float thetaI = 0, thetaJ = 0;

  for ( int i = 0; i < imgHeight * imgWidth; i++ ) {
    for ( int j = 0; j < imgHeight * imgWidth; j++ ) {
      thetaI = sqrt(covarianceMatrix[i][i]);
      thetaJ = sqrt(covarianceMatrix[j][j]);
      corel = covarianceMatrix[i][j] / (thetaI * thetaJ);
      corelationMatrix[i][j] = corel;
    }
  }
}

void printCorelationMatrix() {
  std::ofstream corelation("corelation.txt");
  
  for ( int i = 0; i < imgHeight * imgWidth; i++ ) {
    corelation << std::endl;
    for ( int j = 0; j < imgHeight * imgWidth; j++ ) {
      corelation << corelationMatrix[i][j] << " ";
    }
  }

  corelation.close();
}





void computeCovariance() {
  float* meanValue = new float[imgHeight * imgWidth];
  computeMeanValue( meanValue );
  
  initCovarianceMatrix();
  computeCovarianceMatrix(meanValue);
  printCovarianceMatrix();
}

void computeCorelation() {
  initCorelationMatrix();
  computeCorelationMatrix();
  printCorelationMatrix();
}

float computeCorelationCoefficientBetweenPoints( POINT p1
                                              , POINT p2
                                              , BYTE* destinationImage
                                              , int destinationHeight
                                              , int destinationWidth
                                              , int destinationOffset) {

  int realW = imgW-1; // strange offset for small imag
  float ret = corelationMatrix[p1.x * realW + p1.y][p2.x * realW + p2.y];

  for ( int i = 0; i < 400; i++ ) {
    int val1 = lab6Images[i][p1.x * realW + p1.y];
    int val2 = lab6Images[i][p2.x * realW + p2.y];
    destinationImage[val1 * destinationOffset + val2] = 0;
  }

  return ret;
}
