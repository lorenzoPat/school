#ifndef _LAB6_H_
#define _LAB6_H_

#include "common.h"

void initializeImagesVector();
void computeCovariance();
void computeCorelation();
float computeCorelationCoefficientBetweenPoints( POINT p1
                                              , POINT p2
                                              , BYTE* destinationImage
                                              , int destinationHeight
                                              , int destinationWidth
                                              , int destinationOffset);
                                              


#endif