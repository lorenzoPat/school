#include "stdafx.h"
#include "lab7.h"
#include <fstream>
#include <string>
#include <sstream>

int pointsNr;
floatingPoints* points;
integerPoints* intPoints;
float stdDev1, stdDev2;
float meanX1, meanX2;
float* normalDistribution1;
float* normalDistribution2;
float* hist1;
float* hist2;
float* probabilitiesBVD;

void    getIntegerPoints();
float   getNormalDistribution(float value, float stdDev, float meanVal);
void    getHistogram(int* pts, float* hist);
float   getNormalizationRaport(float* matrix1, float* matrix2, int limit, int imgHeight);
float   getNormalizationRaport(float* matrix, int limit, int max);
void    scaleMatrix(float* matrix, float normalizationRaport, int limit);
float   computeCovariance();
void    computeProbabilities(float corelationCoeff);

void readCoordinatesFromFile() {
  std::ifstream pointsFile("points.txt");
 
  pointsFile >> pointsNr;
  
  points = new floatingPoints[pointsNr];
  for (int i = 0; i < pointsNr; i++) {
    pointsFile >> points[i].x1;
    pointsFile >> points[i].x2;
  }

  pointsFile.close();
}

void drawPoints( BYTE* destinationImage, int imgOffset) {
  getIntegerPoints();

  for (int i = 0; i < pointsNr; i++) 
    destinationImage[intPoints[i].x2 * imgOffset + intPoints[i].x1] = 0;
}

void computeMeanValueOfPoints() {
  for (int i = 0; i < pointsNr; i++) {
    meanX1 += points[i].x1;
    meanX2 += points[i].x2;
  }

  meanX1 /= pointsNr;
  meanX2 /= pointsNr;
}

void computeStandardDeviationOfPoints() {
  for (int i = 0; i < pointsNr; i++) {
    stdDev1 = stdDev1 + (points[i].x1 - meanX1) * (points[i].x1 - meanX1);
    stdDev2 = stdDev2 + (points[i].x2 - meanX2) * (points[i].x2 - meanX2);
  }

  stdDev1 /= pointsNr;
  stdDev2 /= pointsNr;

  stdDev1 = sqrt(stdDev1);
  stdDev2 = sqrt(stdDev2);
}

void showResultMessageBox() {
  std::stringstream stream;
  stream << "Mean for x1: " << meanX1 << "\n";
  stream << "Mean for x2: " << meanX2 << "\n";
  stream << "StdDev for x1: " << stdDev1 << "\n";
  stream << "StdDev for x2: " << stdDev2 << "\n";
  
  std::string message = stream.str();

  ::MessageBox(NULL, message.c_str(), "Result", 0);
}

void computeNormalDistribution(int imgHeight) {
  normalDistribution1 = new float[pointsNr];
  normalDistribution2 = new float[pointsNr];
  
  for (int i = 0; i < pointsNr; i++) {
    normalDistribution1[i] = getNormalDistribution(points[i].x1, stdDev1, meanX1);
    normalDistribution2[i] = getNormalDistribution(points[i].x2, stdDev2, meanX2);
  }

  
  float normalizationRaport = getNormalizationRaport(normalDistribution1, normalDistribution2, pointsNr, imgHeight);
  
  scaleMatrix(normalDistribution1, normalizationRaport, pointsNr);
  scaleMatrix(normalDistribution2, normalizationRaport, pointsNr);
}

void showNormalDistribution( BYTE* destinationImage
                           , int imgOffset) {
 
  POINT* pts1 = new POINT[pointsNr];
  POINT* pts2 = new POINT[pointsNr];
  
  for (int i = 0; i < pointsNr; i++) {
    pts1[i].x = points[i].x1;
    pts1[i].y = normalDistribution1[i];
    pts2[i].x = points[i].x2;
    pts2[i].y = normalDistribution2[i];
  }

  for (int i = 0; i < pointsNr; i++) {
    destinationImage[pts1[i].y * imgOffset + pts1[i].x] = 0;
    destinationImage[pts2[i].y * imgOffset + pts2[i].x] = 0;
  }
  
  delete pts1;
  delete pts2;
}

void computeHistogram( int imgHeight ) {
  hist1 = new float[imgHeight];
  hist2 = new float[imgHeight];
  
  for (int i = 0; i < imgHeight; i++) {
    hist1[i] = 0;
    hist2[i] = 0;
  }

  int* intPts1 = new int[pointsNr];
  int* intPts2 = new int[pointsNr];

  //get the integer part of the points
  for (int i = 0; i < pointsNr; i++) {
    intPts1[i] = intPoints[i].x1;
    intPts2[i] = intPoints[i].x2;
  }
  
  getHistogram(intPts1, hist1);
  getHistogram(intPts2, hist2);
  
  
  float normalizationRaport = getNormalizationRaport( hist1, hist2, imgHeight, imgHeight );
  
  scaleMatrix(hist1, normalizationRaport, imgHeight);
  scaleMatrix(hist2, normalizationRaport, imgHeight);
}

void showHistogram( BYTE* destinationImage
                           , int imgHeight
                           , int imgWidth
                           , int imgOffset) {

  for (int i = 0; i < imgHeight; i++) {
    destinationImage[(int)hist1[i] * imgOffset + i] = 0;
    destinationImage[(int)hist2[i] * imgOffset + i] = 0;
  }
}

void computeBivariateDistribution() {
  float covariance = computeCovariance();
  float corelationCoeff = covariance/(stdDev1*stdDev2);

  probabilitiesBVD = new float[pointsNr];
  computeProbabilities(corelationCoeff);
  float scale = getNormalizationRaport(probabilitiesBVD, pointsNr, 255); // pass limit of rgb 
  scaleMatrix(probabilitiesBVD, scale, pointsNr);
}

void showResultOfBivariateDistribution( BYTE* destinationImage
                                      , int imgHeight
                                      , int imgWidth
                                      , int imgOffset) {
  //init image with a background colog
  for (int i = 0; i < imgHeight; i++) {
    for (int j = 0 ; j < imgWidth; j++) {	
      destinationImage[i * imgOffset + j]= 255;
    }
  }
  
  POINT* intPoints = new POINT[pointsNr];

  for (int i = 0; i < pointsNr; i++) {
    intPoints[i].x = (int)points[i].x1;
    intPoints[i].y = (int)points[i].x2;
  }
  
  for (int i = 0; i < pointsNr; i++) {
    destinationImage[intPoints[i].y * imgOffset + intPoints[i].x] = 255 - (int)probabilitiesBVD[i];
  }

}


void getIntegerPoints() {
  intPoints = new integerPoints[pointsNr];
  for (int i = 0; i < pointsNr; i++) {
    intPoints[i].x1 = points[i].x1;
    intPoints[i].x2 = points[i].x2;
  }
}

float getNormalDistribution(float value, float stdDev, float meanVal) {
  float result = 0;
  
  float firstTerm = sqrt(2 * PI) * stdDev;
  float square = (value - meanVal) * (value - meanVal);
  float secondTerm = exp( -square / (2 * stdDev * stdDev) );
  result = secondTerm / firstTerm;

  return result;
}

void getHistogram(int* pts, float* hist) {
  for (int i = 0; i < pointsNr; i++)
    hist[pts[i]]++;
}

float getNormalizationRaport(float* matrix1, float* matrix2, int limit, int imgHeight) {
  float max = 0;

  for (int i = 0; i < limit; i++) {
    max = (matrix1[i] > max) ? matrix1[i] : max;
    max = (matrix2[i] > max) ? matrix2[i] : max;
  }

  return imgHeight / max;
}

float getNormalizationRaport(float* matrix, int limit, int max) {
  float maxProb = 0;
  for (int i = 0; i < limit; i++) {
    if (matrix[i] > maxProb) maxProb = matrix[i];
  }

  return max / maxProb;
}

void scaleMatrix(float* matrix, float normalizationRaport, int limit) {
  for (int i = 0; i < limit; i++)
    matrix[i] *= normalizationRaport;
}

float computeCovariance() {
  float sum = 0;
  for (int i = 0; i < pointsNr; i++) {
    sum += (points[i].x1 - meanX1) * (points[i].x2 - meanX2);
  }

  return sum / pointsNr;
}

void computeProbabilities(float corelationCoeff) {
  for (int i = 0; i < pointsNr; i++) {
    float firstTerm = 1/ (2 * PI * stdDev1 * stdDev2 * sqrt(1 - corelationCoeff * corelationCoeff));

    float secondTerm;
    float x1 = (points[i].x1 - meanX1) * (points[i].x1 - meanX1) / (stdDev1 * stdDev1);
    float x2 = (points[i].x2 - meanX2) * (points[i].x2 - meanX2) / (stdDev2 * stdDev2);
    float x3 = 2 * corelationCoeff * (points[i].x1 - meanX1) * (points[i].x2 - meanX2) / (stdDev1 * stdDev2);
    secondTerm = exp( -(x1 + x2 - x3)/(2 * (1- corelationCoeff * corelationCoeff)) );
    
    probabilitiesBVD[i] = firstTerm * secondTerm;
  }
}