#ifndef _LAB7_H_
#define _LAB7_H_

#include "common.h"

typedef struct _floatingPoints {
  float x1;
  float x2;
} floatingPoints;

typedef struct _integerPoints {
  int x1;
  int x2;
} integerPoints;

void readCoordinatesFromFile();
void drawPoints( BYTE* destinationImage, int imgOffset);
void computeMeanValueOfPoints();
void computeStandardDeviationOfPoints();
void showResultMessageBox();
void computeNormalDistribution( int imgHeight );
void showNormalDistribution( BYTE* destinationImage
                           , int imgOffset);
void computeHistogram( int imgHeight );
void showHistogram( BYTE* destinationImage
                           , int imgHeight
                           , int imgWidth
                           , int imgOffset);
void computeBivariateDistribution();
void showResultOfBivariateDistribution( BYTE* destinationImage
                                      , int imgHeight
                                      , int imgWidth
                                      , int imgOffset);

#endif
