#include "stdafx.h"
#include "lab8.h"
#include <string>
#include <sstream>

BYTE** lab8Category1;
BYTE** lab8Category2;
BYTE** lab8Category3;
int sampleImgHeight;
int sampleImgWidth;
int sampleImgW;
int imgNr_Cat1;
int imgNr_Cat2;
int imgNr_Cat3;

int getDistanceBetween( BYTE* img1, BYTE* img2 );

float getProbabilityOfCategory( int category ) {
  float total = imgNr_Cat1 + imgNr_Cat2;
  
  switch (category) {
  case 1:
    return imgNr_Cat1 / total;
  case 2:
    return imgNr_Cat2 / total;
  }
}

float getProbabilityOfSampleInCategoryOne(BYTE* sample) {
  int thresholdDistance = 35;
  float imagesNearSample = 0;

  for ( int i = 0; i < imgNr_Cat1; i++ ) {
    int dist;
    dist = getDistanceBetween( sample, lab8Category1[i] );
    imagesNearSample += ( dist < thresholdDistance ) ? 1 : 0;
  }

  return imagesNearSample / imgNr_Cat1;
}

float getProbabilityOfSampleInCategoryTwo(BYTE* sample) {
  int thresholdDistance = 35;
  float imagesNearSample = 0;

  for ( int i = 0; i < imgNr_Cat2; i++ ) {
    int dist;
    dist = getDistanceBetween( sample, lab8Category2[i] );
    imagesNearSample += ( dist < thresholdDistance ) ? 1 : 0;
  }

  return imagesNearSample / imgNr_Cat2;
}

float getProbabilityOfSampleInCategoryThree(BYTE* sample) {
  int thresholdDistance = 35;
  float imagesNearSample = 0;

  for ( int i = 0; i < imgNr_Cat3; i++ ) {
    int dist;
    dist = getDistanceBetween( sample, lab8Category3[i] );
    imagesNearSample += ( dist < thresholdDistance ) ? 1 : 0;
  }

  return imagesNearSample / imgNr_Cat3;
}

float getPostProbabilityOfCategory( BYTE* sample, int category ) {
  float probOfSampleInCategory;

  switch(category) {
  case 1:
    probOfSampleInCategory = getProbabilityOfSampleInCategoryOne(sample);
    break;
  case 2:
    probOfSampleInCategory = getProbabilityOfSampleInCategoryTwo(sample);
    break;
  }

  return probOfSampleInCategory * getProbabilityOfCategory( category );
}

void showResultMessageBoxForBayes( float probabilityOfOne
                                 , float probabilityOfTwo
                                 , float probabilityOfSampleInOne
                                 , float probabilityOfSampleInTwo
                                 , float posteriorProbabilityOne
                                 , float posteriorProbabilityTwo
                                 , int result) {
  std::stringstream stream;
  stream << "P(one): " << probabilityOfOne << "\n";
  stream << "P(two): " << probabilityOfTwo << "\n";
  stream << "P(T|one): " << probabilityOfSampleInOne << "\n";
  stream << "P(T|two): " << probabilityOfSampleInTwo << "\n";
  stream << "P(one|T): " << posteriorProbabilityOne << "\n";
  stream << "P(two|T): " << posteriorProbabilityTwo << "\n";
  stream << "Result: " << result << "\n";
  
  std::string message = stream.str();

  ::MessageBox(NULL, message.c_str(), "Result", 0);
}


float getProbabilityOfCategoryEx( int category ) {
  float total = imgNr_Cat1 + imgNr_Cat2 + imgNr_Cat3;
  
  switch (category) {
  case 1:
    return imgNr_Cat1 / total;
  case 2:
    return imgNr_Cat2 / total;
  case 3:
    return imgNr_Cat3 / total;
  }
}

float getPostProbabilityOfCategoryEx( BYTE* sample, int category ) {
  float probOfSampleInCategory;

  switch(category) {
  case 1:
    probOfSampleInCategory = getProbabilityOfSampleInCategoryOne(sample);
    break;
  case 2:
    probOfSampleInCategory = getProbabilityOfSampleInCategoryTwo(sample);
    break;
  case 3:
    probOfSampleInCategory = getProbabilityOfSampleInCategoryThree(sample);
    break;
  }
  
  return getProbabilityOfCategoryEx( category ) * probOfSampleInCategory;
}

void showResultMessageBoxForBayesEx( float probabilityOfOne
                                   , float probabilityOfTwo
                                   , float probabilityOfThree
                                   , float probabilityOfSampleInOne
                                   , float probabilityOfSampleInTwo
                                   , float probabilityOfSampleInThree
                                   , float posteriorProbabilityOne
                                   , float posteriorProbabilityTwo
                                   , float posteriorProbabilityThree
                                   , int result) {
  std::stringstream stream;
  stream << "P(one): " << probabilityOfOne << "\n";
  stream << "P(two): " << probabilityOfTwo << "\n";
  stream << "P(three): " << probabilityOfThree << "\n";
  stream << "P(T|one): " << probabilityOfSampleInOne << "\n";
  stream << "P(T|two): " << probabilityOfSampleInTwo << "\n";
  stream << "P(T|three): " << probabilityOfSampleInThree << "\n";
  stream << "P(one|T): " << posteriorProbabilityOne << "\n";
  stream << "P(two|T): " << posteriorProbabilityTwo << "\n";
  stream << "P(three|T): " << posteriorProbabilityThree << "\n";
  stream << "Result: " << result << "\n";

  
  std::string message = stream.str();

  ::MessageBox(NULL, message.c_str(), "Result", 0);
}


int getDistanceBetween( BYTE* img1, BYTE* img2 ) {
  int sum = 0;
  for ( int i = 0; i < sampleImgHeight; i++ ) {
    for ( int j = 0; j < sampleImgWidth; j++ ) {
      sum += (int)abs(img1[i * sampleImgW + j] - img2[i * sampleImgW + j]);
    }
  }

  return sum/(sampleImgHeight * sampleImgWidth);
}
