#ifndef _LAB8_H_
#define _LAB8_H_

#include "common.h"

float getProbabilityOfCategory( int category );
float getProbabilityOfSampleInCategoryOne(BYTE* sample);
float getProbabilityOfSampleInCategoryTwo(BYTE* sample);
float getProbabilityOfSampleInCategoryThree(BYTE* sample);
float getPostProbabilityOfCategory( BYTE* sample, int category );
float getProbabilityOfCategoryEx( int category );
float getPostProbabilityOfCategoryEx( BYTE* sample, int category );

void showResultMessageBoxForBayes( float probabilityOfOne
                                 , float probabilityOfTwo
                                 , float probabilityOfSampleInOne
                                 , float probabilityOfSampleInTwo
                                 , float posteriorProbabilityOne
                                 , float posteriorProbabilityTwo
                                 , int result);
void showResultMessageBoxForBayesEx( float probabilityOfOne
                                   , float probabilityOfTwo
                                   , float probabilityOfThree
                                   , float probabilityOfSampleInOne
                                   , float probabilityOfSampleInTwo
                                   , float probabilityOfSampleInThree
                                   , float posteriorProbabilityOne
                                   , float posteriorProbabilityTwo
                                   , float posteriorProbabilityThree
                                   , int result);

                                              


#endif