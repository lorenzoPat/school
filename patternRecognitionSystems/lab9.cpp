#include "stdafx.h"
#include "lab9.h"
#include <math.h>


float getVectorNorm(float x[]) {
  return sqrt(x[0]*x[0] + x[1]*x[1] + x[2]*x[2]);
}

float getY(LinearClassifierData data, float* a) {
  float x1 = data.label * a[0];
  float x2 = data.x * a[1];
  float x3 = data.y * a[2];

  return x1 + x2 + x3;
}

void updateVec(float* vec, float n, LinearClassifierData data ) {
  vec[0] = vec[0] + data.label * n;
  vec[1] = vec[1] + data.x * n;
  vec[2] = vec[2] + data.y * n;
}

int getTotalPointsNr(BYTE* sourceImage
               , int imgHeight
               , int imgWidth
               , int imgOffset) {
  int pointsNr = 0;

  for (int i = 0; i < imgHeight; i++) {
		for (int j = 0; j < imgWidth; j++) {	
      if (sourceImage[i * imgOffset + j] == 2 || sourceImage[i * imgOffset + j] == 1  ) 
        pointsNr++; 
	  }
  }

  return pointsNr;
}

void getPoints(float* weightVec, int imgWidth, POINT& p1, POINT& p2) {
  p1.x = 0;
  p1.y = weightVec[0] / (-weightVec[2]);
  
  p2.x = imgWidth;
  p2.y = (weightVec[0] + weightVec[1]*imgWidth) / (-weightVec[2]);
}

void constructSample(BYTE* sourceImage, int imgHeight, int imgWidth, int imgOffset, LinearClassifierData* sample) {
  int sampleIndex = 0;
  for (int i = 0; i < imgHeight; i++) {
		for (int j = 0; j < imgWidth; j++) {	
      if (sourceImage[i * imgOffset + j] == 2) {
        sample[sampleIndex].label = -1;
        sample[sampleIndex].x = -j;
        sample[sampleIndex].y = -i;
        sampleIndex++;
      }
      
      if (sourceImage[i * imgOffset + j] == 1) {
        sample[sampleIndex].label = 1;
        sample[sampleIndex].x = j;
        sample[sampleIndex].y = i;
        sampleIndex++;
      }
	  }
  }
}


void getLinearClassifier(BYTE* sourceImage
                         , int imgHeight
                         , int imgWidth
                         , int imgOffset
                         , POINT& p1
                         , POINT& p2) {

  int pointsNr = getTotalPointsNr(sourceImage, imgHeight, imgWidth, imgOffset);

  int k = 0; int iter = 0;
  float n = 1;
  float theta = 0.0001;
  float a[3] = {0.1, 0.1, 0.1};
  float b[3] = {0,0,0};

  LinearClassifierData* sample = new LinearClassifierData[pointsNr];
  constructSample(sourceImage, imgHeight, imgWidth, imgOffset, sample);
  
  do {
    b[0] = 0; b[1] = 0; b[2] = 0;    

    do {
      float testY = getY(sample[k], a);

      if ( testY < 0 ) {
        updateVec(a, n, sample[k]);
        updateVec(b, n, sample[k]);
      }

      k++;
    } while ( k < pointsNr);

    k = 0;  
  } while(getVectorNorm(b) > theta);
  
  getPoints(a, imgWidth, p1, p2);
}