#ifndef _LAB9_H_
#define _LAB9_H_

typedef struct _LinearClassifierData {
  int label;
  int x;
  int y;
} LinearClassifierData;

// gets the input image and returns two points that describe the linear classifier
void getLinearClassifier(BYTE* sourceImage
                         , int imgHeight
                         , int imgWidth
                         , int imgOffset
                         , POINT& p1
                         , POINT& p2);

#endif